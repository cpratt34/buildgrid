# Copyright (C) 2022 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import subprocess
import time
from unittest.mock import patch

import pytest


@pytest.fixture(scope="session")
def moto_server(request):
    """
    This fixture spawns "moto_server", which is a test server for S3.
    """
    moto_server_proc = subprocess.Popen(["moto_server"])
    # Wait for server to start
    time.sleep(2)

    # Instruct moto to use the spawned moto_server
    with patch.dict(os.environ, {"TEST_SERVER_MODE": "true"}):
        yield moto_server_proc

    # Terminate moto_server at the end of the test session
    moto_server_proc.terminate()
