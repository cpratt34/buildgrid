# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License' is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
from base64 import b64decode
from collections import namedtuple
import io
import os
import shutil
import tarfile
import tempfile

from aiohttp_middlewares.cors import ACCESS_CONTROL_ALLOW_ORIGIN
from google.protobuf import empty_pb2
import grpc
import pytest

from buildgrid._app.cli import Context
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, ActionResult, Digest, Directory, FileNode, GetActionResultRequest, RequestMetadata, ToolDetails
from buildgrid._protos.google.longrunning.operations_pb2 import CancelOperationRequest, GetOperationRequest, ListOperationsRequest, ListOperationsResponse, Operation
from buildgrid._protos.google.bytestream.bytestream_pb2 import ReadRequest, ReadResponse
from buildgrid.browser import utils
from buildgrid.browser.app import create_app
from buildgrid.settings import HASH, REQUEST_METADATA_HEADER_NAME, REQUEST_METADATA_TOOL_NAME, REQUEST_METADATA_TOOL_VERSION

from tests.utils.utils import run_coroutine_in_subprocess


BAD_FILTER = 'bad-filter-string'
TEST_ACTION = Action(
    do_not_cache=True,
    salt=b'example-salt'
)
TEST_ACTION_DIGEST = Digest(
    hash='test-digest',
    size_bytes=123
)
TEST_EMPTY_BLOB_DIGEST = Digest(
    hash='empty-blob-digest',
    size_bytes=0
)
TEST_RESOURCE_NAME = f'blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}'
TEST_EMPTY_BLOB_RESOURCE_NAME = f'blobs/{TEST_EMPTY_BLOB_DIGEST.hash}/{TEST_EMPTY_BLOB_DIGEST.size_bytes}'

TEST_LOGSTREAM_NAME = 'logStream/test'
TEST_LOG_LINES = [
    b'logstream line 1',
    b'logstream line 2',
    b'logstream line 3'
]
TEST_ACTION_RESULT = ActionResult(stdout_raw=b'test', exit_code=0)
TEST_TOOL_DETAILS = ToolDetails(
    tool_name=REQUEST_METADATA_TOOL_NAME,
    tool_version=REQUEST_METADATA_TOOL_VERSION
)
TEST_REQUEST_METADATA = RequestMetadata(
    tool_details=TEST_TOOL_DETAILS,
    tool_invocation_id='test-invocation',
    correlated_invocations_id='test-correlation',
    action_id=f'{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}'
)
MetadataTuple = namedtuple('MetadataTuple', ['key', 'value'])
TEST_TRAILING_METADATA = (
    MetadataTuple(key=REQUEST_METADATA_HEADER_NAME, value=TEST_REQUEST_METADATA.SerializeToString()),
)
TEST_OPERATION = Operation(name='test-operation')
LIST_OPERATIONS_RESPONSE = ListOperationsResponse(
    operations=[TEST_OPERATION]
)
INDEX_CONTENT = '<html><body>Hello world</body></html>'
JS_CONTENT = 'function main() { console.log("hello world"); }; main();'
TEST_INSTANCE_NAME = "dev"
TEST_REQUEST_HEADERS = {
    'Origin': 'testsuite'
}
TEST_TARBALL_DIRECTORY_NAME = f'{utils.TARBALL_DIRECTORY_PREFIX}test'

# The following constants define a Directory message used to test tarball generation
FILE_CONTENT = b'hello world'
FILE_DIGEST = Digest(
    hash=HASH(FILE_CONTENT).hexdigest(),
    size_bytes=len(FILE_CONTENT)
)
FILE_RESOURCE_NAME = f'blobs/{FILE_DIGEST.hash}/{FILE_DIGEST.size_bytes}'
FILE_NODE = FileNode(
    name="test.txt",
    digest=FILE_DIGEST,
    is_executable=True
)

DIRECTORY = Directory(
    files=[FILE_NODE]
)
serialized_directory = DIRECTORY.SerializeToString()
DIRECTORY_DIGEST = Digest(
    hash=HASH(serialized_directory).hexdigest(),
    size_bytes=len(serialized_directory)
)
DIRECTORY_RESOURCE_NAME = f'blobs/{DIRECTORY_DIGEST.hash}/{DIRECTORY_DIGEST.size_bytes}'


class FakeCall:

    def trailing_metadata(self):
        return TEST_TRAILING_METADATA


class FakeUnaryCall:

    def __init__(self, loop, raise_error, instance_name, request, response_delay):
        self._loop = loop
        self._raise_error = raise_error
        self._instance_name = instance_name
        self._request = request
        self._response_delay = response_delay

    def _parse_operation_name(self, name):
        names = name.split('/')
        return names[-1] if len(names) > 1 else name

    def _get_operation_handler(self):
        if self._parse_operation_name(self._request.name) == TEST_OPERATION.name:
            return TEST_OPERATION
        raise grpc.aio.AioRpcError(grpc.StatusCode.INVALID_ARGUMENT, None, None)

    def _cancel_operation_handler(self):
        if self._parse_operation_name(self._request.name) == TEST_OPERATION.name:
            return empty_pb2.Empty()  # `CancelOperation()` returns an empty proto
        raise grpc.aio.AioRpcError(grpc.StatusCode.INVALID_ARGUMENT, None, None)

    def _list_operations_handler(self):
        if self._request.filter == BAD_FILTER:
            raise grpc.aio.AioRpcError(grpc.StatusCode.INVALID_ARGUMENT, None, None)
        return LIST_OPERATIONS_RESPONSE

    def _action_result_handler(self):
        if self._request.action_digest == TEST_ACTION_DIGEST:
            if self._instance_name and self._request.instance_name != self._instance_name:
                raise grpc.aio.AioRpcError(grpc.StatusCode.INVALID_ARGUMENT, None, None)
            return TEST_ACTION_RESULT
        raise grpc.aio.AioRpcError(grpc.StatusCode.NOT_FOUND, None, None)

    async def _get_response(self):
        if self._response_delay:
            await asyncio.sleep(self._response_delay)

        if self._raise_error:
            raise grpc.aio.AioRpcError(grpc.StatusCode.INTERNAL, None, None)

        if isinstance(self._request, ListOperationsRequest):
            return self._list_operations_handler()
        elif isinstance(self._request, GetOperationRequest):
            return self._get_operation_handler()
        elif isinstance(self._request, GetActionResultRequest):
            return self._action_result_handler()
        elif isinstance(self._request, CancelOperationRequest):
            return self._cancel_operation_handler()

    async def trailing_metadata(self):
        return TEST_TRAILING_METADATA

    def __await__(self):
        response = yield from self._loop.create_task(self._get_response())
        return response


class FakeStreamCall:

    def __init__(self, loop, raise_error, instance_name, request):
        self._loop = loop
        self._raise_error = raise_error
        self._instance_name = instance_name
        self._request = request

    async def _get_response(self):
        if self._raise_error:
            raise grpc.aio.AioRpcError(grpc.StatusCode.INTERNAL, None, None)

        if self._request.resource_name == f'{self._instance_name}/{TEST_RESOURCE_NAME}':
            serialized = TEST_ACTION.SerializeToString()
            read_offset = self._request.read_offset
            read_limit = self._request.read_limit
            if read_offset > 0 and read_limit > 0:
                yield ReadResponse(data=serialized[read_offset:read_offset+read_limit])
            else:
                length = len(serialized) // 2
                yield ReadResponse(data=serialized[:length])
                yield ReadResponse(data=serialized[length:])

        elif self._request.resource_name == f'{self._instance_name}/{TEST_EMPTY_BLOB_RESOURCE_NAME}':
            yield ReadResponse(data=b'')

        elif self._request.resource_name == f'{self._instance_name}/{DIRECTORY_RESOURCE_NAME}':
            yield ReadResponse(data=DIRECTORY.SerializeToString())

        elif self._request.resource_name == f'{self._instance_name}/{FILE_RESOURCE_NAME}':
            yield ReadResponse(data=FILE_CONTENT)

        elif self._request.resource_name == f'{self._instance_name}/{TEST_LOGSTREAM_NAME}':
            yield ReadResponse(data=b'logstream line 1')
            yield ReadResponse(data=b'logstream line 2')
            yield ReadResponse(data=b'logstream line 3')

        else:
            raise grpc.aio.AioRpcError(grpc.StatusCode.NOT_FOUND, None, None)

    def __aiter__(self):
        return self._get_response()


class FakeUnaryMultiCallable:

    _callable = None

    def __init__(self, loop, raise_error, instance_name, request_log, response_delay):
        self._loop = loop
        self._raise_error = raise_error
        self._instance_name = instance_name
        self._request_log = request_log
        self._response_delay = response_delay

    def _log_request(self, request):
        if isinstance(request, ListOperationsRequest):
            self._request_log['ListOperations'].append(request)
        elif isinstance(request, GetOperationRequest):
            self._request_log['GetOperation'].append(request)
        elif isinstance(request, GetActionResultRequest):
            self._request_log['GetActionResult'].append(request)
        elif isinstance(request, CancelOperationRequest):
            self._request_log['CancelOperation'].append(request)

    def __call__(self, request):
        self._log_request(request)
        return FakeUnaryCall(self._loop, self._raise_error, self._instance_name, request, self._response_delay)

    @classmethod
    async def with_call(cls, request):
        if cls._callable is None:
            raise Exception(
                'Error in usage of the fake async gRPC calls. Using '
                'FakeUnaryMultiCallable.with_call requires you first '
                'instantiate a FakeUnaryMultiCallable object and then '
                'store it in `FakeUnaryMultiCallable._callable`'
            )
        result = await cls.__call__(cls._callable, request)
        return result, FakeCall()


class FakeStreamMultiCallable:

    def __init__(self, loop, raise_error, instance_name, request_log):
        self._loop = loop
        self._raise_error = raise_error
        self._instance_name = instance_name
        self._request_log = request_log

    def _log_request(self, request):
        if isinstance(request, ReadRequest):
            self._request_log['Read'].append(request)

    def __call__(self, request):
        self._log_request(request)
        return FakeStreamCall(self._loop, self._raise_error, self._instance_name, request)


class FakeChannel:

    def __init__(self, loop, raise_error=False, instance_name=None):
        self._loop = loop
        self._raise_error = raise_error
        self._instance_name = instance_name
        self.unary_stream_call_count = 0
        self.request_log = {
            'CancelOperation': [],
            'GetActionResult': [],
            'GetOperation': [],
            'ListOperations': [],
            'Read': []
        }
        self.response_delay = 0

    def unary_unary(self, name, *, request_serializer, response_deserializer):
        # NOTE: This ugly-looking thing allows us to have the unary multi-callable
        # object implement the `with_call` classmethod whilst still letting us
        # pass in the event loop and raise_error boolean.
        multi_callable = FakeUnaryMultiCallable(
            self._loop,
            self._raise_error,
            self._instance_name,
            self.request_log,
            self.response_delay
        )
        FakeUnaryMultiCallable._callable = multi_callable
        return multi_callable

    def unary_stream(self, name, *, request_serializer, response_deserializer):
        self.unary_stream_call_count += 1
        return FakeStreamMultiCallable(self._loop, self._raise_error, self._instance_name, self.request_log)

    def stream_unary(self, name, *, request_serializer, response_deserializer):
        return FakeStreamMultiCallable(self._loop, self._raise_error, self._instance_name, self.request_log)


@pytest.fixture
def static_directory():
    with tempfile.TemporaryDirectory() as directory:
        with open(os.path.join(directory, 'index.html'), 'w') as index:
            index.write(INDEX_CONTENT)
        with open(os.path.join(directory, 'app.js'), 'w') as js:
            js.write(JS_CONTENT)
        yield directory


@pytest.fixture
def context():
    context = Context()
    context.channel = FakeChannel(asyncio.get_event_loop())
    context.cas_channel = context.channel
    context.operations_channel = context.channel
    context.cache_channel = context.channel
    context.logstream_channel = context.channel
    return context


@pytest.fixture
def response_cache():
    return utils.ResponseCache()


@pytest.fixture(params=[TEST_INSTANCE_NAME, ''])
def app(static_directory, context, request, response_cache):
    context.channel._instance_name = request.param
    context.instance_name = request.param
    return create_app(context, response_cache, [], static_directory, allow_cancelling_operations=True)


@pytest.fixture
def error_app(static_directory, context, response_cache):
    context.channel._raise_error = True
    return create_app(context, response_cache, [], static_directory)


@pytest.fixture(params=[TEST_INSTANCE_NAME, ''])
def app_without_cancellations(static_directory, request):
    context = Context()
    context.channel = FakeChannel(asyncio.get_event_loop(), instance_name=request.param)
    context.cas_channel = context.channel
    context.operations_channel = context.channel
    context.cache_channel = context.channel
    context.logstream_channel = context.channel
    context.instance_name = request.param
    return create_app(context, [], static_directory, allow_cancelling_operations=False)


def test_list_operations(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get('/api/v1/operations', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers
        serialized_response = await resp.read()
        assert serialized_response == LIST_OPERATIONS_RESPONSE.SerializeToString()

    assert run_coroutine_in_subprocess(_test)


def test_list_operations_populates_cache(aiohttp_client, app, context, response_cache):
    async def _test():
        client = await aiohttp_client(app)

        # Send a request for all Operations. This should populate the Operation cache.
        resp = await client.get('/api/v1/operations', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200

        # Send a request for the test Operation. This should be a cache hit.
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200

        # Check that we sent a ListOperations request, but not a GetOperation request.
        assert len(context.channel.request_log['ListOperations']) == 1
        assert len(context.channel.request_log['GetOperation']) == 0

    assert run_coroutine_in_subprocess(_test)


def test_list_operations_bad_filter(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/operations?q={BAD_FILTER}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 400
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_list_operations_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        resp = await error_client.get('/api/v1/operations', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 500
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_operation(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers
        serialized_response = await resp.read()
        assert serialized_response == TEST_OPERATION.SerializeToString()

    assert run_coroutine_in_subprocess(_test)


def test_get_operation_cache(aiohttp_client, app, context):
    async def _test():
        client = await aiohttp_client(app)

        # Send a request to get an Operation. This should populate the cache.
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200

        # Send a request to get the same Operation. This should be a cache hit.
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200

        # Check that only one of these requests resulted in a gRPC call.
        assert len(context.channel.request_log['GetOperation']) == 1

    assert run_coroutine_in_subprocess(_test)


def test_get_operation_metadata(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}/request_metadata', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers
        serialized_response = await resp.read()
        assert serialized_response == TEST_REQUEST_METADATA.SerializeToString()

    assert run_coroutine_in_subprocess(_test)


def test_get_unknown_operation(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get('/api/v1/operations/not-an-operation', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_operation_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        resp = await error_client.get('/api/v1/operations/test-operation', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 500
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_operation_request_metadata(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/operations/{TEST_OPERATION.name}/request_metadata', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers
        serialized_response = await resp.read()
        assert serialized_response == TEST_REQUEST_METADATA.SerializeToString()

    assert run_coroutine_in_subprocess(_test)


def test_get_unknown_operation_request_metadata(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get('/api/v1/operations/not-an-operation.request_metadata', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_operation_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        resp = await error_client.get(f'/api/v1/operations/{TEST_OPERATION.name}/request_metadata', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 500
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_cancel_operation(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.delete(f'/api/v1/operations/{TEST_OPERATION.name}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_cancel_operation_not_allowed(aiohttp_client, app_without_cancellations):
    async def _test():
        client_without_cancellations = await aiohttp_client(app_without_cancellations)
        # The DELETE route is non-existent in this case.
        resp = await client_without_cancellations.delete(f'/api/v1/operations/{TEST_OPERATION.name}')
        assert resp.status == 405

    assert run_coroutine_in_subprocess(_test)


def test_cancel_unknown_operation(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.delete(f'/api/v1/operations/this-operation-does-not-exist-123', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_action_result(aiohttp_client, app, context):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers
        serialized_response = await resp.read()
        assert serialized_response == TEST_ACTION_RESULT.SerializeToString()

        # Fetch the same ActionResult again and make sure we don't hit the
        # actual ActionCache this time.
        assert len(context.channel.request_log['GetActionResult']) == 1
        resp = await client.get(f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        serialized_response = await resp.read()
        assert serialized_response == TEST_ACTION_RESULT.SerializeToString()
        assert len(context.channel.request_log['GetActionResult']) == 1

    assert run_coroutine_in_subprocess(_test)


def test_get_action_result_deduplication(aiohttp_client, app, context):
    async def _test():
        client = await aiohttp_client(app)
        # Add some delay to the ActionCache response, to give time to actually deduplicate
        context.channel.response_delay = 2

        tasks = [
            client.get(
                f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
                headers=TEST_REQUEST_HEADERS
            ),
            client.get(
                f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
                headers=TEST_REQUEST_HEADERS
            )
        ]
        resp1, resp2 = await asyncio.gather(*tasks)
        assert resp1.status == 200
        assert resp2.status == 200

        # Make sure that we only sent one GetActionResult request
        assert len(context.channel.request_log['GetActionResult']) == 1

        # Ensure that the response is as expected
        serialized1 = await resp1.read()
        serialized2 = await resp2.read()
        assert serialized1 == serialized2 == TEST_ACTION_RESULT.SerializeToString()

    assert run_coroutine_in_subprocess(_test)


def test_action_result_caching(aiohttp_client, app, context):
    async def _test():
        client = await aiohttp_client(app)

        # Request an ActionResult from the browser-backend. This should cache
        # the result for future requests.
        resp1 = await client.get(
                f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
                headers=TEST_REQUEST_HEADERS
        )
        assert resp1.status == 200

        # Request the same ActionResult again. This should be a cache hit.
        resp2 = await client.get(
                f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
                headers=TEST_REQUEST_HEADERS
        )
        assert resp2.status == 200

        # Make sure that only one of these requests resulted in a gRPC call.
        assert len(context.channel.request_log['GetActionResult']) == 1

    assert run_coroutine_in_subprocess(_test)


def test_get_action_result_cache_miss(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/action_results/missing-action/321', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_action_result_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        resp = await error_client.get(f'/api/v1/action_results/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 500
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_blob(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

        serialized = TEST_ACTION.SerializeToString()
        length = len(serialized) // 2

        content = b''
        data, _ = await resp.content.readchunk()
        assert data == serialized[:length]
        content += data

        data, _ = await resp.content.readchunk()
        assert data == serialized[length:]
        content += data
        assert content == TEST_ACTION.SerializeToString()

        assert resp.headers['Content-Disposition'] == f'Attachment;filename={TEST_ACTION_DIGEST.hash}_{TEST_ACTION_DIGEST.size_bytes}'

    assert run_coroutine_in_subprocess(_test)


def test_get_blob_cache(aiohttp_client, app, context):
    async def _fetch_blob(client):
        resp = await client.get(
            f'/api/v1/blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
            headers=TEST_REQUEST_HEADERS
        )
        buffer = b''
        async for data, _ in resp.content.iter_chunks():
            buffer += data
        return buffer

    async def _test():
        client = await aiohttp_client(app)

        # Send a request for a blob to the browser backend. This should populate the cache.
        response = await _fetch_blob(client)
        assert response == TEST_ACTION.SerializeToString()

        # Send a second request for the same blob. This should be a cache hit.
        response = await _fetch_blob(client)
        assert response == TEST_ACTION.SerializeToString()

        # Make sure that only one of these requests resulted in a gRPC call.
        assert len(context.channel.request_log['Read']) == 1

    assert run_coroutine_in_subprocess(_test)


@pytest.mark.parametrize('read_raw_blob', [False, True])
def test_get_partial_blob(aiohttp_client, app, read_raw_blob):
    async def _test():
        client = await aiohttp_client(app)
        offset = 4
        limit = 10

        params = {'offset': offset, 'limit': limit}
        if read_raw_blob:
            params['raw'] = 1

        resp = await client.get(f'/api/v1/blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}',
                                params=params,
                                headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

        data, _ = await resp.content.readchunk()
        assert len(data) == limit

        serialized = TEST_ACTION.SerializeToString()
        assert data == serialized[offset:offset+limit]

        if read_raw_blob:
            assert resp.headers['Content-type'] == 'text/plain; charset=utf-8'
        else:
            assert resp.headers['Content-Disposition'] == f'Attachment;filename={TEST_ACTION_DIGEST.hash}_{TEST_ACTION_DIGEST.size_bytes}_chunk_{offset}-{offset+limit}'
    
    assert run_coroutine_in_subprocess(_test)


def test_get_raw_blob(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}?raw=1', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

        serialized = TEST_ACTION.SerializeToString()
        length = len(serialized) // 2

        content = b''
        data, _ = await resp.content.readchunk()
        assert data == serialized[:length]
        content += data

        data, _ = await resp.content.readchunk()
        assert data == serialized[length:]
        content += data
        assert content == TEST_ACTION.SerializeToString()

        assert 'Content-Disposition' not in resp.headers
        assert resp.headers['Content-type'] == 'text/plain; charset=utf-8'

    assert run_coroutine_in_subprocess(_test)


def test_get_empty_blob(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/blobs/{TEST_EMPTY_BLOB_DIGEST.hash}/{TEST_EMPTY_BLOB_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 200
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

        data, _ = await resp.content.readchunk()
        assert not data

    assert run_coroutine_in_subprocess(_test)


def test_get_tarball(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get(f'/api/v1/tarballs/{DIRECTORY_DIGEST.hash}_{DIRECTORY_DIGEST.size_bytes}.tar.gz')
        assert resp.status == 200

        chunk, done = await resp.content.readchunk()
        tarball_content = io.BytesIO()
        tarball_content.write(chunk)
        while not done:
            chunk, done = await resp.content.readchunk()
            tarball_content.write(chunk)

        tarball_content.seek(0)
        tarball = tarfile.open(fileobj=tarball_content, mode="r:gz")
        file_entry = tarball.getmember(FILE_NODE.name)
        assert file_entry.isfile()
        assert file_entry.size == len(FILE_CONTENT)

    assert run_coroutine_in_subprocess(_test)


def test_get_tarball_deduplication(aiohttp_client, app, context):
    async def _test():
        client = await aiohttp_client(app)
        tasks = [
            client.get(f'/api/v1/tarballs/{DIRECTORY_DIGEST.hash}_{DIRECTORY_DIGEST.size_bytes}.tar.gz'),
            client.get(f'/api/v1/tarballs/{DIRECTORY_DIGEST.hash}_{DIRECTORY_DIGEST.size_bytes}.tar.gz')
        ]
        resp1, resp2 = await asyncio.gather(*tasks)
        assert resp1.status == 200
        assert resp2.status == 200

        # If deduplication is working, we should only have made 2 unary_stream
        # requests (i.e. ByteStream Reads) in the above requests. 1 request to
        # fetch the directory message and another request to fetch the file
        # referenced in the directory message.
        assert context.channel.unary_stream_call_count == 2

        # Verify that both responses have the expected content
        chunk, done = await resp1.content.readchunk()
        tarball_content_1 = io.BytesIO()
        tarball_content_1.write(chunk)
        while not done:
            chunk, done = await resp1.content.readchunk()
            tarball_content_1.write(chunk)

        tarball_content_1.seek(0)
        tarball_1 = tarfile.open(fileobj=tarball_content_1, mode="r:gz")
        file_entry = tarball_1.getmember(FILE_NODE.name)
        assert file_entry.isfile()
        assert file_entry.size == len(FILE_CONTENT)

        chunk, done = await resp2.content.readchunk()
        tarball_content_2 = io.BytesIO()
        tarball_content_2.write(chunk)
        while not done:
            chunk, done = await resp2.content.readchunk()
            tarball_content_2.write(chunk)

        tarball_content_2.seek(0)
        tarball_2 = tarfile.open(fileobj=tarball_content_2, mode="r:gz")
        file_entry = tarball_2.getmember(FILE_NODE.name)
        assert file_entry.isfile()
        assert file_entry.size == len(FILE_CONTENT)

    assert run_coroutine_in_subprocess(_test)


@pytest.mark.asyncio
async def test_tarball_cleanup(context, static_directory):
    # Create a directory layout as if we forcibly stopped mid-tarball construction
    tarball_directory = tempfile.mkdtemp()
    leftover_tarball_path = os.path.join(tarball_directory, TEST_TARBALL_DIRECTORY_NAME)
    os.mkdir(leftover_tarball_path)

    # Test that the mock leftover tarball directory gets cleaned up
    create_app(context, utils.ResponseCache(), [], static_path=static_directory, tarball_dir=tarball_directory)
    assert not os.path.exists(leftover_tarball_path)

    # Clean up after ourselves
    shutil.rmtree(tarball_directory)


def test_get_missing_blob(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get('/api/v1/blobs/missing-blob/123', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

        resp2 = await client.get('/api/v1/blobs/missing-blob/0', headers=TEST_REQUEST_HEADERS)
        assert resp2.status == 404
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


def test_get_blob_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        resp = await error_client.get(f'/api/v1/blobs/{TEST_ACTION_DIGEST.hash}/{TEST_ACTION_DIGEST.size_bytes}', headers=TEST_REQUEST_HEADERS)
        assert resp.status == 500
        assert ACCESS_CONTROL_ALLOW_ORIGIN in resp.headers

    assert run_coroutine_in_subprocess(_test)


# Kind of a hack to expose the same instance name to both the client fixture and this test
# in the 'instance_name' parameter to verify the returned resource name includes the instance name
@pytest.mark.parametrize('app, instance_name', [(TEST_INSTANCE_NAME, TEST_INSTANCE_NAME), ('', '')], indirect=['app'])
def test_logstream(aiohttp_client, app, instance_name):
    async def _test():
        client = await aiohttp_client(app)
        ws_connection = await client.ws_connect('/ws/logstream')
        request = ReadRequest(resource_name=TEST_LOGSTREAM_NAME)
        await ws_connection.send_bytes(request.SerializeToString())

        full_resource_name = f'{instance_name}/{TEST_LOGSTREAM_NAME}'
        for line in TEST_LOG_LINES:
            message = await ws_connection.receive_json()
            assert not message['complete']
            assert message['resource_name'] == full_resource_name
            response = ReadResponse.FromString(b64decode(message['data']))
            assert response.data == line

        message = await ws_connection.receive_json()
        assert message['complete']
        assert message['resource_name'] == full_resource_name
        assert not message['data']

    assert run_coroutine_in_subprocess(_test)


# Kind of a hack to expose the same instance name to both the client fixture and this test
# in the 'instance_name' parameter to verify the returned resource name includes the instance name
@pytest.mark.parametrize('app, instance_name', [(TEST_INSTANCE_NAME, TEST_INSTANCE_NAME), ('', '')], indirect=['app'])
def test_logstream_missing(aiohttp_client, app, instance_name):
    async def _test():
        client = await aiohttp_client(app)
        ws_connection = await client.ws_connect('/ws/logstream')
        request = ReadRequest(resource_name='imaginary-logstream')
        await ws_connection.send_bytes(request.SerializeToString())
        message = await ws_connection.receive_json()

        assert message['complete']
        assert message['resource_name'] == f'{instance_name}/imaginary-logstream'
        assert message['data'] == "NOT_FOUND"

    assert run_coroutine_in_subprocess(_test)


def test_logstream_internal_error(aiohttp_client, error_app):
    async def _test():
        error_client = await aiohttp_client(error_app)
        ws_connection = await error_client.ws_connect('/ws/logstream')
        request = ReadRequest(resource_name=TEST_LOGSTREAM_NAME)
        full_resource_name = f'/{TEST_LOGSTREAM_NAME}'
        await ws_connection.send_bytes(request.SerializeToString())
        message = await ws_connection.receive_json()

        assert message['complete']
        assert message['resource_name'] == full_resource_name
        assert message['data'] == "INTERNAL"

    assert run_coroutine_in_subprocess(_test)


def test_serve_static_files(aiohttp_client, app):
    async def _test():
        client = await aiohttp_client(app)
        resp = await client.get('/')
        assert resp.status == 200
        index = await resp.text()
        assert index == INDEX_CONTENT

        # check other file
        resp = await client.get('/app.js')
        assert resp.status == 200
        js = await resp.text()
        assert js == JS_CONTENT

        # check 404 falls back to index
        resp = await client.get('/action/foo/bar')
        assert resp.status == 200
        content = await resp.text()
        assert content == INDEX_CONTENT

    assert run_coroutine_in_subprocess(_test)
