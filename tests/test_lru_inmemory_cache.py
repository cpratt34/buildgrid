# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from buildgrid.server.utils.lru_inmemory_cache import LruInMemoryCache


def test_empty():
    c = LruInMemoryCache(capacity=10)
    assert c.capacity == 10
    assert c.size == 0
    assert c.get('key') is None


def test_update_and_get():
    c = LruInMemoryCache(capacity=10)
    c.update('k', 'value')
    assert c.size == 1
    assert c.get('k') == 'value'


def test_expiry():
    c = LruInMemoryCache(capacity=2)

    entries = [('k1', 'v1'), ('k2', 'v2'), ('k3', 'v3')]
    for k, v in entries:
        c.update(k, v)

    assert c.size == 2
    assert c.get('k1') is None
    assert c.get('k2') is 'v2'
    assert c.get('k3') is 'v3'


def test_update():
    c = LruInMemoryCache(capacity=2)
    c.update('k1', 'v1')
    c.update('k2', 'v2')

    # Update changes the value:
    assert c.get('k1') == 'v1'
    c.update('k1', 'v1.1')
    assert c.get('k1') == 'v1.1'

    # And updates the last-access time:
    c.update('k3', 'v3')
    assert c.get('k1') == 'v1.1'
    assert c.get('k2') is None
    assert c.get('k3') == 'v3'


def test_get_updates_last_usage():
    c = LruInMemoryCache(capacity=2)
    c.update('k1', 'v1')
    c.update('k2', 'v2')
    # Expiry would delete k1 right now.

    c.get('k1')
    # Now adding a new entry should expire k2:
    c.update('k3', 'v3')
    assert c.get('k2') is None
    assert c.get('k3') == 'v3'
