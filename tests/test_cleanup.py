# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name

from buildgrid.cleanup.cleanup import CASCleanUp

import pytest
from unittest.mock import patch, call
import asyncio


class mock_storage:
    def is_cleanup_enabled(self):
        return True


def get_cleanup():
    storages = {'test': mock_storage()}
    indexes = {'test': mock_storage()}
    return CASCleanUp(False, 100, 0, 5, 10, 0, storages, indexes, False)


def test_cleanup_start():
    """ Test cleanup.start. """
    new_loop = asyncio.new_event_loop()

    def get_new_loop():
        return new_loop

    def mock_cleanup_batch():
        pass

    exceptions = []

    def mock_logger(self, e):
        nonlocal exceptions
        exceptions.append(e)

    async def mock_cleanupWorker(self, instance_name):
        await new_loop.run_in_executor(None, mock_cleanup_batch)

    with patch('buildgrid.cleanup.cleanup.CASCleanUp._cleanupWorker', mock_cleanupWorker):
        with patch('logging.Logger.exception', mock_logger):
            with patch('time.sleep') as mock_sleep:
                with patch('asyncio.get_event_loop', get_new_loop):
                    cleanup = get_cleanup()
                    cleanup.start()
                    assert mock_sleep.call_args_list == []
                    assert exceptions == []


def test_cleanup_start_exception():
    """ Test that _cleanupWorker exceptions are propogated up. """
    new_loop = asyncio.new_event_loop()

    def get_new_loop():
        return new_loop

    def mock_cleanup_batch(state=[0]):
        excep_map = {1: RuntimeError(), 2: KeyError(), 3: MemoryError()}
        state[0] += 1
        if state[0] in excep_map:
            raise excep_map[state[0]]

    exceptions = []
    expected = [RuntimeError, KeyError, MemoryError]

    def mock_logger(self, e):
        nonlocal exceptions
        exceptions.append(e)

    async def mock_cleanupWorker(self, instance_name):
        await new_loop.run_in_executor(None, mock_cleanup_batch)

    with patch('buildgrid.cleanup.cleanup.CASCleanUp._cleanupWorker', mock_cleanupWorker):
        with patch('logging.Logger.exception', mock_logger):
            with patch('time.sleep') as mock_sleep:
                with patch('asyncio.get_event_loop', get_new_loop):
                    cleanup = get_cleanup()
                    cleanup.start()
                    assert mock_sleep.call_args_list == [call(1), call(1.6), call(1.6 ** 2)]
                    for x, ex in zip(exceptions, expected): assert type(x) == ex
