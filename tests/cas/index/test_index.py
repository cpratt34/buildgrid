# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name

from concurrent.futures import ThreadPoolExecutor, wait
from datetime import datetime, timedelta
import random
import tempfile
import time
import sqlite3
import pytest
from unittest import mock

from sqlalchemy import select
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound, StaleDataError
import testing.postgresql

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.with_cache import WithCacheStorage
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.metrics_names import (
    CAS_INDEX_BLOB_TIMESTAMP_UPDATE_TIME_METRIC_NAME,
    CAS_INDEX_BLOB_TIMESTAMP_UPDATE_TIME_METRIC_NAME,
    CAS_INDEX_BULK_SELECT_DIGEST_TIME_METRIC_NAME,
    CAS_INDEX_BULK_TIMESTAMP_UPDATE_TIME_METRIC_NAME,
    CAS_INDEX_GET_BLOB_TIME_METRIC_NAME,
    CAS_INDEX_SAVE_DIGESTS_TIME_METRIC_NAME,
    CAS_INDEX_SIZE_CALCULATION_TIME_METRIC_NAME
)
from buildgrid.server.monitoring import _MonitoringBus
from buildgrid.server.persistence.sql.models import IndexEntry
from buildgrid.settings import HASH, MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES
from tests.utils.metrics import mock_create_timer_record

# Skip certain tests if sqlite3 driver is less than 3.25
sql_version_skip = bool((int(sqlite3.sqlite_version_info[0]) == 3 and int(sqlite3.sqlite_version_info[1]) < 25) or
                        int(sqlite3.sqlite_version[0]) < 3)
sql_version_skip_message = "Skipped test due to sqlite3 version being less than 3.25."
check_sqlite3_version = pytest.mark.skipif(sql_version_skip, reason=sql_version_skip_message)

try:
    Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)
except RuntimeError:
    print(f"skipping all POSTGRES tests")
    POSTGRES_INSTALLED = False
else:
    POSTGRES_INSTALLED = True

postgres_skip_message = "Skipped test due to Postgres not being installed"
check_for_postgres = pytest.mark.skipif(not POSTGRES_INSTALLED, reason=postgres_skip_message)

BLOBS = [b'abc', b'defg', b'hijk', b'']
DIGESTS = [remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(),
                                       size_bytes=len(blob)) for blob in BLOBS]
EXTRA_BLOBS = [b'lmno', b'pqr']
EXTRA_DIGESTS = [remote_execution_pb2.Digest(
    hash=HASH(blob).hexdigest(),
    size_bytes=len(blob)) for blob in EXTRA_BLOBS]


@pytest.fixture()
def blobs_digests():
    return list(zip(BLOBS, DIGESTS))


@pytest.fixture()
def extra_blobs_digests():
    return list(zip(EXTRA_BLOBS, EXTRA_DIGESTS))


@pytest.fixture(params=['sqlite', 'postgresql', 'postgresql-with-cache-fallback-writes-only', 'postgresql-inlined'])
def any_index(request):
    if request.param == 'sqlite':
        storage = LRUMemoryCache(256)
        with tempfile.NamedTemporaryFile() as db:
            index = SQLIndex(
                storage=storage,
                connection_string=f"sqlite:///{db.name}",
                automigrate=True)
            index.set_instance_name('')
            yield index

    elif request.param == 'postgresql':
        storage = LRUMemoryCache(256)
        with Postgresql() as postgresql:
            index = SQLIndex(
                storage=storage,
                connection_string=postgresql.url(),
                automigrate=True)
            index.set_instance_name('')
            yield index

    elif request.param == 'postgresql-with-cache-fallback-writes-only':
        cache_storage = LRUMemoryCache(256)
        fallback_storage = LRUMemoryCache(256)
        storage = WithCacheStorage(cache_storage, fallback_storage)
        with mock.patch('buildgrid.server.cas.storage.with_cache.WithCacheStorage.begin_write', new=fallback_storage.begin_write):
            with mock.patch('buildgrid.server.cas.storage.with_cache.WithCacheStorage.commit_write', new=fallback_storage.commit_write):
                with Postgresql() as postgresql:
                    index = SQLIndex(
                        storage=storage,
                        connection_string=postgresql.url(),
                        automigrate=True)
                    index.set_instance_name('')
                    yield index

    elif request.param == 'postgresql-inlined':
        storage = LRUMemoryCache(256)
        with Postgresql() as postgresql:
            index = SQLIndex(
                storage=storage,
                connection_string=postgresql.url(),
                automigrate=True,
                max_inline_blob_size=256)
            index.set_instance_name('')
            yield index


def _write(storage, digest, blob):
    session = storage.begin_write(digest)
    session.write(blob)
    storage.commit_write(digest, session)


@check_for_postgres
def test_session_exceptions_to_not_rollback_on(any_index):
    """ Test that pass_exceptions flag in session works as expected. """
    exceptions = [StaleDataError, NoResultFound]

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with any_index.session(exceptions_to_not_rollback_on=exceptions):
            raise StaleDataError
        rollback.assert_not_called()

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with any_index.session(exceptions_to_not_rollback_on=exceptions):
            raise NoResultFound
        rollback.assert_not_called()

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with pytest.raises(MultipleResultsFound):
            with any_index.session(exceptions_to_not_rollback_on=exceptions):
                raise MultipleResultsFound
        rollback.assert_called_once()

    # Verify default is to always not pass on exceptions
    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with pytest.raises(MultipleResultsFound):
            with any_index.session():
                raise MultipleResultsFound
        rollback.assert_called_once()


def _get_disposal_exceptions(db):
    dialect = db._engine.dialect.name
    assert dialect != ''

    exceptions = db._sql_pool_dispose_helper._dispose_pool_on_exceptions
    if dialect == 'postgresql':
        assert len(exceptions) > 0

    return exceptions or ()


@check_for_postgres
def test_pool_disposal_exceptions(any_index):
    disposal_exceptions = _get_disposal_exceptions(any_index)
    any_index._engine.dispose = mock.MagicMock()
    any_index._sql_pool_dispose_helper._cooldown_time_in_secs = 1

    # Throw some random exceptions and ensure we don't dispose the pool
    for e in (ValueError, KeyError):
        with pytest.raises(Exception):
            try:
                with any_index.session() as session:
                    raise Exception("Forced exception") from e()
            except Exception:
                # Should only call this once in a short period of time,
                # regardless of how many exceptions we have
                raise
        any_index._engine.dispose.assert_not_called()

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        with pytest.raises(Exception):
            try:
                with any_index.session() as session:
                    raise Exception("Forced exception") from e()
            except Exception:
                # Should only call this once in a short period of time,
                # regardless of how many exceptions we have
                raise
        any_index._engine.dispose.assert_called_once()


@check_for_postgres
def test_pool_disposal_limit(any_index):
    disposal_exceptions = _get_disposal_exceptions(any_index)
    any_index._engine.dispose = mock.MagicMock()
    any_index._sql_pool_dispose_helper._cooldown_time_in_secs = 1

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        with pytest.raises(Exception):
            try:
                with any_index.session() as session:
                    raise Exception("Forced exception") from e()
            except Exception:
                # Should only call this once in a short period of time,
                # regardless of how many exceptions we have
                raise
        any_index._engine.dispose.assert_called_once()

    # Now manually override the last disposal time and see if that triggers another disposal (once more)
    timestamp_disposed_a_while_back = datetime.utcnow() - 2 * timedelta(minutes=MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES)
    any_index._sql_pool_dispose_helper._last_pool_dispose_time = timestamp_disposed_a_while_back

    # Throw the exceptions we want to dispose the pool on
    # Ensure we dispose the pool again, but only once more
    for e in disposal_exceptions:
        with pytest.raises(Exception):
            try:
                with any_index.session() as session:
                    raise Exception("Forced exception") from e()
            except Exception:
                # Should only call this once in a short period of time,
                # regardless of how many exceptions we have
                raise
        assert any_index._engine.dispose.call_count == 2


@check_for_postgres
def test_exceptions_to_not_raise_on(any_index):
    exceptions = [StaleDataError, NoResultFound]

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with any_index.session(exceptions_to_not_raise_on=exceptions):
            raise StaleDataError
        rollback.assert_called_once()

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with any_index.session(exceptions_to_not_raise_on=exceptions):
            raise NoResultFound
        rollback.assert_called_once()

    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with pytest.raises(MultipleResultsFound):
            with any_index.session(exceptions_to_not_raise_on=exceptions):
                raise MultipleResultsFound
        rollback.assert_called_once()

    # Verify default is to always not pass on exceptions
    with mock.patch('sqlalchemy.orm.session.Session.rollback') as rollback:
        with pytest.raises(MultipleResultsFound):
            with any_index.session():
                raise MultipleResultsFound
        rollback.assert_called_once()


@check_for_postgres
def test_has_blob(any_index, blobs_digests, extra_blobs_digests):
    """ The index should accurately reflect its contents with has_blob. """
    for _, digest in blobs_digests:
        assert any_index.has_blob(digest) is False
    for _, digest in extra_blobs_digests:
        assert any_index.has_blob(digest) is False

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    for _, digest in blobs_digests:
        assert any_index.has_blob(digest) is True
    for _, digest in extra_blobs_digests:
        assert any_index.has_blob(digest) is False


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_get_blob(any_index, blobs_digests, extra_blobs_digests):
    """ The index should properly return contents under get_blob. """
    _MonitoringBus._instance = mock.Mock()
    for _, digest in blobs_digests:
        assert any_index.get_blob(digest) is None
    for _, digest in extra_blobs_digests:
        assert any_index.get_blob(digest) is None

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    for blob, digest in blobs_digests:
        assert any_index.get_blob(digest).read() == blob
    for _, digest in extra_blobs_digests:
        assert any_index.get_blob(digest) is None
    mock_get_blob_time = mock_create_timer_record(
        name=CAS_INDEX_GET_BLOB_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_get_blob_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@check_for_postgres
def test_timestamp_updated_by_get(any_index, blobs_digests):
    """ When a blob is accessed, the timestamp should be updated. """
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    any_index.get_blob(second_digest)

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should have updated the timestamp
    assert second_digest == updated_order[-1]


@check_sqlite3_version
@check_for_postgres
def test_timestamp_not_updated_by_get(any_index, blobs_digests):
    """ When a blob is accessed, the timestamp should be updated. """
    REFRESH_THRESHOLD_IN_SECONDS = 1

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    # Test 1: Write order is the LRU order.
    lru_order = [d for d in any_index.least_recent_digests()]
    assert second_digest == lru_order[1]

    # Test 2: When a refresh threshold is configured, there's no timestamp update 
    # if the digest is read "too soon", such as immediately
    any_index.refresh_accesstime_older_than = REFRESH_THRESHOLD_IN_SECONDS
    any_index.get_blob(second_digest)
    lru_order = [d for d in any_index.least_recent_digests()]
    assert second_digest == lru_order[1]

    # Test 3: Performing the same read after the refresh threshold has elapsed
    # will update the timestamp.
    # Mock datetime.utcnow() so that it posts time from after the threshold has elapsed
    threshold_seconds_later = datetime.utcnow() + timedelta(seconds=REFRESH_THRESHOLD_IN_SECONDS)
    with mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime') as mock_date_pstgr, \
         mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime') as mock_date_sqlite, \
         mock.patch('buildgrid.server.cas.storage.index.sql.datetime') as mock_date_generic:
        mock_date_pstgr.utcnow.return_value = threshold_seconds_later
        mock_date_sqlite.utcnow.return_value = threshold_seconds_later
        mock_date_generic.utcnow.return_value = threshold_seconds_later
        # Perform the read
        any_index.get_blob(second_digest)
    lru_order = [d for d in any_index.least_recent_digests()]
    assert second_digest == lru_order[-1]

@check_sqlite3_version
@check_for_postgres
def test_timestamp_updated_by_write(any_index, blobs_digests):
    """ Writes should also update the timestamp. """
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    _write(any_index, second_digest, second_blob)

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should have updated the timestamp
    assert second_digest == updated_order[-1]


def _digestified_range(max):
    """ Generator for digests for bytestring representations of all numbers in
    the range [0, max)
    """
    for i in range(max):
        blob = bytes(i)
        yield remote_execution_pb2.Digest(
            hash=HASH(blob).hexdigest(),
            size_bytes=len(blob)
        )


@check_sqlite3_version
@check_for_postgres
def test_timestamp_updated_by_random_gets_and_writes(any_index):
    """ Test throwing a random arrangement of gets and writes against the
    index to ensure that it updates the blobs in the correct order.

    With an index primed with a modest number of blobs, we call get_blob
    on a third of them chosen randomly, _write on another third, and leave
    the last third alone. The index should be left in the proper state
    at the end."""
    digest_map = {}
    for i, digest in enumerate(_digestified_range(100)):
        _write(any_index, digest, bytes(i))
        digest_map[digest.hash] = i

    old_order = []
    for i, digest in enumerate(any_index.least_recent_digests()):
        assert digest_map[digest.hash] == i
        old_order.append(digest)

    untouched_digests = []
    updated_digests = []

    gets = ['get'] * (len(old_order) // 3)
    writes = ['write'] * (len(old_order) // 3)
    do_nothings = ['do nothing'] * (len(old_order) - len(gets) - len(writes))
    actions = gets + writes + do_nothings
    random.shuffle(actions)

    for i, (action, digest) in enumerate(zip(old_order, actions)):

        if action == 'get':
            any_index.get_blob(digest)
            updated_digests.append(digest)
        elif action == 'write':
            _write(any_index, digest, bytes(i))
            updated_digests.append(digest)
        elif action == 'do nothing':
            untouched_digests.append(digest)

    # The proper order should be every blob that wasn't updated (in relative
    # order), followed by every blob that was updated (in relative order)
    new_order = untouched_digests + updated_digests

    for actual_digest, expected_digest in zip(any_index.least_recent_digests(), new_order):
        assert actual_digest == expected_digest


@check_for_postgres
def test_bulk_read_on_missing_blobs(any_index, blobs_digests, extra_blobs_digests):
    """ Check that attempting to do a bulk read on blobs that are missing
    returns the blobs for blobs found and None for all others.

    'extra_blobs_digests' will be our missing blobs in this test."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    results = any_index.bulk_read_blobs(
        [digest for (blob, digest) in blobs_digests + extra_blobs_digests])
    for blob, digest in blobs_digests:
        assert results.get(digest.hash, None) is not None
    for blob, digest in extra_blobs_digests:
        assert results.get(digest.hash, None) is None


@check_for_postgres
def test_get_blob_backfills_inline_blobs(any_index, blobs_digests):
    # Skip testing any indexes that are already doing inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 0

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    empty_digest_hash = HASH(b'').hexdigest()

    digest_list = [digest for (blob, digest) in blobs_digests]

    # Blobs should not be inlined
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        if entry.digest_hash != empty_digest_hash:
            assert entry.inline_blob is None

    # Enable inlining after writing blobs
    any_index._max_inline_blob_size = 10000

    # Rerun get_blob to backfill
    for (blob, digest) in blobs_digests:
        any_index.get_blob(digest)

    # Blobs should be visible in the index entries
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        assert entry.inline_blob is not None


@check_for_postgres
def test_bulk_read_backfills_inline_blobs(any_index, blobs_digests):
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 0

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    digest_list = [digest for (blob, digest) in blobs_digests]
    any_index.bulk_read_blobs(digest_list)
    empty_digest_hash = HASH(b'').hexdigest()

    # Blobs should not be inlined
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        if entry.digest_hash != empty_digest_hash:
            assert entry.inline_blob is None

    # Enable inlining after writing blobs
    any_index._max_inline_blob_size = 10000

    # Rerun bulk_read_blobs to backfill
    any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])

    # Blobs should be visible in the index entries
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        assert entry.inline_blob is not None


@check_for_postgres
def test_get_blob_adjusted_inlining(any_index, blobs_digests):
    """ Ensure that get_blob can fetch blobs even when the inlining size is adjusted
    to be smaller or larger. This won't happen in practice, but it simulates an
    environment with multiple BuildGrids configured with different inlining sizes
    (which can happen when deploying a configuration change to the inlining size). """
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 3

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # '' and 'abc' will have been inlined, but 'defg' and 'hijk' won't

    # Now adjust the size up and verify we can get each blob
    any_index._max_inline_blob_size = 10000
    for (blob, digest) in blobs_digests:
        assert any_index.get_blob(digest).read() == blob

    # Now adjust the size DOWN and verify we can get each blob
    any_index._max_inline_blob_size = 0
    for (blob, digest) in blobs_digests:
        assert any_index.get_blob(digest).read() == blob


@check_for_postgres
def test_bulk_read_blobs_adjusted_inlining(any_index, blobs_digests):
    """ Ensure that bulk_read_blobs can fetch blobs even when the inlining size is adjusted
    to be smaller or larger. This won't happen in practice, but it simulates an
    environment with multiple BuildGrids configured with different inlining sizes
    (which can happen when deploying a configuration change to the inlining size). """
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 3

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # '' and 'abc' will have been inlined, but 'defg' and 'hijk' won't

    # Now adjust the size up and verify we can get each blob
    any_index._max_inline_blob_size = 10000
    results = any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])
    for (blob, digest) in blobs_digests:
        assert results[digest.hash].read() == blob 

    # Now adjust the size DOWN and verify we can get each blob
    any_index._max_inline_blob_size = 0
    results = any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])
    for (blob, digest) in blobs_digests:
        assert results[digest.hash].read() == blob 


def _first_batch_seen_before_second(index, first_batch, second_batch):
    """ Returns true if the first batch of digests is located entirely before
    the second group in the index's LRU order.
    """
    first_batch_hashes = set([digest.hash for digest in first_batch])
    second_batch_hashes = set([digest.hash for digest in second_batch])

    # The sets must be disjoint
    assert(first_batch_hashes.isdisjoint(second_batch_hashes))

    for digest in index.least_recent_digests():
        # Delete the element if it's in the first set
        first_batch_hashes.discard(digest.hash)
        # If we see anything in the second set, we should have seen everything
        # in the first set
        if digest.hash in second_batch_hashes:
            return not first_batch_hashes
    return False


@check_sqlite3_version
@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_timestamp_updated_by_bulk_update(any_index, blobs_digests, extra_blobs_digests):
    """ Test that timestamps are updated by bulk writes. """
    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    first_batch_digests = [digest for (blob, digest) in blobs_digests]
    second_batch_digests = [digest for (blob, digest) in extra_blobs_digests]

    # At the start, the blobs in the first batch have an earlier timestamp
    # than the blobs in the second
    assert _first_batch_seen_before_second(
        any_index, first_batch_digests, second_batch_digests)

    any_index.bulk_update_blobs([(digest, blob)
                                 for (blob, digest) in blobs_digests])

    # After rewriting the first batch, the first batch appears after the second
    assert _first_batch_seen_before_second(
        any_index, second_batch_digests, first_batch_digests)

    mock_save_digests_time = mock_create_timer_record(
        name=CAS_INDEX_SAVE_DIGESTS_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_save_digests_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_timestamp_updated_by_bulk_read(any_index, blobs_digests, extra_blobs_digests):
    """ Test that timestamps are updated by bulk reads. """
    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    first_batch_digests = [digest for (blob, digest) in blobs_digests]
    second_batch_digests = [digest for (blob, digest) in extra_blobs_digests]

    # At the start, the blobs in the first batch have an earlier timestamp
    # than the blobs in the second
    assert _first_batch_seen_before_second(
        any_index, first_batch_digests, second_batch_digests)

    any_index.bulk_read_blobs(first_batch_digests)

    # After reading the first batch, the first batch appears after the second
    assert _first_batch_seen_before_second(
        any_index, second_batch_digests, first_batch_digests)

    mock_bulk_select_time = mock_create_timer_record(
        name=CAS_INDEX_BULK_SELECT_DIGEST_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_bulk_select_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_timestamp_updated_by_missing_blobs(any_index, blobs_digests):
    """ FindMissingBlobs should also update the timestamp. """
    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    any_index.missing_blobs([second_digest])

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should have updated the timestamp
    assert second_digest == updated_order[-1]

    mock_bulk_update_time = mock_create_timer_record(
        name=CAS_INDEX_BULK_TIMESTAMP_UPDATE_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_bulk_update_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_timestamp_not_updated_by_bulk_read(any_index, blobs_digests, extra_blobs_digests):
    """ Test that timestamps are not updated by bulk writes when any_index is
        configured with a refresh threshold time.
        (Its purpose is to prevent a burst of timestamp updates on a blob
        during heavy read that causes database performance to degrade). """
    REFRESH_THRESHOLD_IN_SECONDS = 1

    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    first_batch_digests = [digest for (blob, digest) in blobs_digests]
    second_batch_digests = [digest for (blob, digest) in extra_blobs_digests]

    # Test 1: The first written batch have an earlier timestamp than the second batch
    assert _first_batch_seen_before_second(
        any_index, first_batch_digests, second_batch_digests)

    # Test 2: When a refresh threshold is configured, there's no timestamp update 
    # if the digests are read "too soon", such as immediately
    any_index.refresh_accesstime_older_than = REFRESH_THRESHOLD_IN_SECONDS
    any_index.bulk_read_blobs(first_batch_digests)
    assert _first_batch_seen_before_second(
        any_index, first_batch_digests, second_batch_digests)

    # Test 3: Performing the same read after the refresh threshold has elapsed
    # will update the timestamp.
    # Mock datetime.utcnow() so that it posts time from after the threshold has elapsed
    threshold_seconds_later = datetime.utcnow() + timedelta(seconds=REFRESH_THRESHOLD_IN_SECONDS)
    with mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime') as mock_date_pstgr, \
         mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime') as mock_date_sqlite, \
         mock.patch('buildgrid.server.cas.storage.index.sql.datetime') as mock_date_generic:
        mock_date_pstgr.utcnow.return_value = threshold_seconds_later
        mock_date_sqlite.utcnow.return_value = threshold_seconds_later
        mock_date_generic.utcnow.return_value = threshold_seconds_later
        # Perform the read
        any_index.bulk_read_blobs(first_batch_digests)
    assert _first_batch_seen_before_second(
        any_index, second_batch_digests, first_batch_digests)

    mock_bulk_select_time = mock_create_timer_record(
        name=CAS_INDEX_BULK_SELECT_DIGEST_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_bulk_select_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_timestamp_not_updated_by_missing_blobs(any_index, blobs_digests):
    """ FindMissingBlobs should also not update the timestamp
        when the refresh threshold is configured. """
    REFRESH_THRESHOLD_IN_SECONDS = 1

    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    
    # Test 1: The written order is the same as the LRU query
    second_blob, second_digest = blobs_digests[1]
    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)
    assert second_digest == old_order[1]

    # Test 2: When a refresh threshold is configured, there's no timestamp update
    # if the find missing blob is performed too soon, such as immediately
    any_index.refresh_accesstime_older_than = REFRESH_THRESHOLD_IN_SECONDS
    any_index.missing_blobs([second_digest])
    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)
    assert second_digest != updated_order[-1]

    # Test 3: After the refresh threshold has elapsed, timestamp updates occur
    # Mock datetime.utcnow() so that it posts time from after the threshold has elapsed
    threshold_seconds_later = datetime.utcnow() + timedelta(seconds=REFRESH_THRESHOLD_IN_SECONDS)
    with mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime') as mock_date_pstgr, \
         mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime') as mock_date_sqlite, \
         mock.patch('buildgrid.server.cas.storage.index.sql.datetime') as mock_date_generic:
        mock_date_pstgr.utcnow.return_value = threshold_seconds_later
        mock_date_sqlite.utcnow.return_value = threshold_seconds_later
        mock_date_generic.utcnow.return_value = threshold_seconds_later
        # Perform the underlying read
        any_index.missing_blobs([second_digest])
    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)
    assert second_digest == updated_order[-1]

    mock_bulk_update_time = mock_create_timer_record(
        name=CAS_INDEX_BULK_TIMESTAMP_UPDATE_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_bulk_update_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_large_missing_blobs(any_index):
    """ Ensure that a large missing_blobs query can be handled by the index
    implementation. We'll just use an empty index for this test since
    we don't really care about its state.

    SQLite can only handle 999 bind variables, so we'll test a search for
    a much larger number."""
    large_input = [digest for digest in _digestified_range(50000)]
    expected_hashes = {digest.hash for digest in large_input}
    for digest in any_index.missing_blobs(large_input):
        assert digest.hash in expected_hashes
        expected_hashes.remove(digest.hash)
    assert not expected_hashes


@check_for_postgres
def test_large_missing_blobs_with_some_present(any_index):
    """ Same as above, but let's add some blobs to the index. """
    digests_blobs = [(digest, bytes(i)) for i, digest in enumerate(_digestified_range(1000))]
    any_index.bulk_update_blobs(digests_blobs)
    not_missing = set(digest.hash for digest, _ in digests_blobs)

    large_input = [digest for digest in _digestified_range(50000)]
    expected_hashes = {digest.hash for digest in large_input
                       if digest.hash not in not_missing}
    for digest in any_index.missing_blobs(large_input):
        assert digest.hash in expected_hashes
        expected_hashes.remove(digest.hash)
    assert not expected_hashes


@check_for_postgres
def test_delete_blob(any_index, blobs_digests):
    """ Deleting a blob should make has_blob return False. """
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    assert any_index.has_blob(second_digest) is True

    any_index.delete_blob(second_digest)

    assert any_index.has_blob(second_digest) is False


@check_for_postgres
def test_get_blob_fallback(any_index, blobs_digests):
    """ If fallback_on_get is enabled, blobs not present in the index should
    get pulled in from the backend storage if they're not in the index when
    get_blob is called. """

    for blob, digest in blobs_digests:
        # Write the blobs to the storage directly but not through the index
        _write(any_index._storage, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    # First let's show what happens when fallback is disabled. The
    # digest isn't pulled into the index.
    any_index._fallback_on_get = False
    assert any_index.has_blob(second_digest) is False
    assert any_index.get_blob(second_digest) is None
    assert any_index.has_blob(second_digest) is False

    # Now let's enable fallback to demonstrate that the index will
    # contain the element after fetching
    any_index._fallback_on_get = True
    assert any_index.get_blob(second_digest).read() == second_blob
    assert any_index.has_blob(second_digest) is True

    # Now let's check that if you pass in a blob that doesn't exist in the
    # backend that it doesn't try and write into the index
    missing_blob = remote_execution_pb2.Digest(hash="deadbeef", size_bytes=666)
    assert any_index.has_blob(missing_blob) is False

    assert any_index.get_blob(missing_blob) is None
    assert any_index.bulk_read_blobs([missing_blob]) == {}
    result = any_index.bulk_read_blobs([missing_blob, second_digest])

    assert second_digest.hash in result


@check_for_postgres
def test_duplicate_writes_ok(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
        _write(any_index, digest, blob)
    for blob, digest in blobs_digests:
        assert any_index.has_blob(digest) is True
        assert any_index.get_blob(digest).read() == blob


@check_for_postgres
def test_bulk_read_blobs_fallback(any_index, blobs_digests):
    """ Similar to the above, BatchUpdateBlobs should retrieve blobs from
    storage when fallback is enabled. """
    for blob, digest in blobs_digests:
        # Write the blobs to the storage directly but not through the index
        _write(any_index._storage, digest, blob)

    # To make things slightly more interesting we'll add the first blob
    # to the index
    first_blob, first_digest = blobs_digests[0]
    _write(any_index, first_digest, first_blob)

    digests = [digest for (blob, digest) in blobs_digests]

    # We'll first show what happens when fallback is disabled
    any_index._fallback_on_get = False
    for digest in digests:
        assert any_index.has_blob(digest) is (digest == first_digest)
    any_index.bulk_read_blobs(digests)
    for digest in digests:
        assert any_index.has_blob(digest) is (digest == first_digest)

    # Now with fallback. All of the blobs should be present after a read.
    any_index._fallback_on_get = True
    any_index.bulk_read_blobs(digests)
    for digest in digests:
        assert any_index.has_blob(digest) is True


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_get_total_size(any_index, blobs_digests):
    """ Test if summing total size is accurate """
    _MonitoringBus._instance = mock.Mock()
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    assert any_index.get_total_size() == 11

    mock_size_calc_time = mock_create_timer_record(name=CAS_INDEX_SIZE_CALCULATION_TIME_METRIC_NAME)
    call_list = [mock.call(mock_size_calc_time)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_mark_n_bytes_as_deleted(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    delete_size = 7

    # Check that the entries are correctly marked as deleted
    digests = any_index.mark_n_bytes_as_deleted(delete_size)
    expected = set(digest.hash for digest in digests)
    with any_index.session() as session:
        marked_entries = session.query(IndexEntry).filter_by(deleted=True).all()
        marked = set(entry.digest_hash for entry in marked_entries)

        unmarked_entries = session.query(IndexEntry).filter_by(deleted=False).all()
        undeleted = set(entry.digest_hash for entry in unmarked_entries)

    assert expected == marked
    assert undeleted

    # Check that we got at least the requested number of bytes marked
    # for deletion
    marked_size = sum(digest.size_bytes for digest in digests)
    assert marked_size >= delete_size

    # Should never mark more than the requested amount + the largest item
    max_size = max(digest.size_bytes for _, digest in blobs_digests)
    assert marked_size <= delete_size + max_size


@check_for_postgres
def test_mark_n_bytes_as_deleted_dry_run(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    delete_size = 7
    # Check that "dry run" functionality doesn't mark anything as deleted,
    # but still returns the correct number of digests
    digests = any_index.mark_n_bytes_as_deleted(delete_size, True)
    with any_index.session() as session:
        marked_entries = session.query(IndexEntry).filter_by(deleted=True).all()
        marked = set(entry.digest_hash for entry in marked_entries)
    assert not marked

    # Check that we got at least the requested number of bytes marked
    # for deletion
    returned_size = sum(digest.size_bytes for digest in digests)
    assert returned_size >= delete_size

    # Should never mark more than the requested amount + the largest item
    max_size = max(digest.size_bytes for _, digest in blobs_digests)
    assert returned_size <= delete_size + max_size


@check_for_postgres
def test_mark_n_bytes_as_deleted_protect_blobs(any_index, blobs_digests, extra_blobs_digests):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    almost_two_days_ago = datetime.utcnow() - timedelta(days=2) - timedelta(seconds=30)
    one_day_ago = datetime.utcnow() - timedelta(days=1)

    # Mock datetime.utcnow() so that it posts time from two days ago
    # Blobs in blobs_digests will appear as if they were last accessed
    # 2 days ago within the index
    with mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime') as mock_date_pstgr, \
         mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime') as mock_date_sqlite, \
         mock.patch('buildgrid.server.cas.storage.index.sql.datetime') as mock_date_generic:
        mock_date_pstgr.utcnow.return_value = almost_two_days_ago
        mock_date_sqlite.utcnow.return_value = almost_two_days_ago
        mock_date_generic.utcnow.return_value = almost_two_days_ago
        for (blob, digest) in blobs_digests:
            _write(any_index, digest, blob)

    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    delete_size = 20

    digests = any_index.mark_n_bytes_as_deleted(delete_size, protect_blobs_after=one_day_ago)
    assert len(digests) == len(blobs_digests)

    digests = any_index.mark_n_bytes_as_deleted(delete_size, protect_blobs_after=two_days_ago)
    assert len(digests) == len(blobs_digests)

    digests = any_index.mark_n_bytes_as_deleted(delete_size)
    assert len(digests) == len(blobs_digests) + len(extra_blobs_digests)


@check_for_postgres
def test_mark_n_bytes_as_deleted_verify_order(any_index, blobs_digests, extra_blobs_digests):
    two_days_ago = datetime.utcnow() - timedelta(days=2)

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # Mock datetime.utcnow() so that it posts time from two days ago
    # Blobs in blobs_digests will appear as if they were last accessed
    # 2 days ago within the index
    with mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime') as mock_date_pstgr, \
         mock.patch('buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime') as mock_date_sqlite, \
         mock.patch('buildgrid.server.cas.storage.index.sql.datetime') as mock_date_generic:
        mock_date_pstgr.utcnow.return_value = two_days_ago
        mock_date_sqlite.utcnow.return_value = two_days_ago
        mock_date_generic.utcnow.return_value = two_days_ago
        for (blob, digest) in extra_blobs_digests:
            _write(any_index, digest, blob)

    delete_size = 7
    digests = any_index.mark_n_bytes_as_deleted(delete_size)
    assert digests == [x[1] for x in extra_blobs_digests]

    delete_size_all = 20
    digests_all = any_index.mark_n_bytes_as_deleted(delete_size_all)
    assert digests_all == [x[1] for x in (extra_blobs_digests + blobs_digests)]


@check_for_postgres
def test_mark_n_bytes_as_deleted_cleans_up(any_index, blobs_digests, extra_blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    # Mark some entries as deleted
    request_size = 7
    digests = any_index.mark_n_bytes_as_deleted(request_size)

    # Get the same amount of entries marked as deleted
    # This should be the exact same digests, simulating what happens if
    # something breaks before the deletion can be finalised
    same_digests = any_index.mark_n_bytes_as_deleted(request_size)

    def getHash(x):
        return x.hash

    assert same_digests.sort(key=getHash) == digests.sort(key=getHash)

    # Check that asking for more bytes returns a strict superset of the
    # earlier (still undeleted but marked) digests
    request_size = 10
    new_digests = any_index.mark_n_bytes_as_deleted(request_size)
    new_set = set(digest.hash for digest in new_digests)
    old_set = set(digest.hash for digest in digests)
    assert old_set < new_set

    all_digests = blobs_digests + extra_blobs_digests
    max_size = max(digest.size_bytes for _, digest in all_digests)
    marked_size = sum(digest.size_bytes for digest in new_digests)
    # Should never mark more than the requested amount + the largest item
    assert marked_size <= request_size + max_size


@check_for_postgres
def test_mark_n_bytes_as_deleted_renew_windows(any_index, blobs_digests, extra_blobs_digests):
    for blob, digest in blobs_digests + extra_blobs_digests:
        _write(any_index, digest, blob)

    # Test 1: At start, and without doing cleanup, there should be no LRU windows.
    # A terminology note: "any_index._queue_of_whereclauses" will be referred as "windows"
    assert len(any_index._queue_of_whereclauses) == 0

    # Test 2: Given window_size==2, number of digests==5, there should
    # be 3 windows (of whereclauses) constructed upon invoking cleanup
    any_index._all_blobs_window_size = 2
    request_size = 3
    digests = any_index.mark_n_bytes_as_deleted(request_size)
    assert len(any_index._queue_of_whereclauses) == 3
    assert digests[0].size_bytes == 3

    # Test 3: Deleting 7 bytes should exhaust the first window (two digests), but not
    # discard the window (not stepping into next window), hence 3 windows.
    request_size = 7
    digests = any_index.mark_n_bytes_as_deleted(request_size, renew_windows=False)
    assert len(digests) == 2
    assert digests[0].size_bytes == 3  # b'abc'
    assert digests[1].size_bytes == 4  # b'defg'
    assert len(any_index._queue_of_whereclauses) == 3

    # Test 4: Deleting 8 bytes should exhaust the first whereclause (two digests), discard it, and
    # stepping into the 2nd whereclause (to grab one digest), hence 2 windows.
    request_size = 8
    digests = any_index.mark_n_bytes_as_deleted(request_size, renew_windows=False)
    assert len(digests) == 3
    assert digests[0].size_bytes == 3  # b'abc'
    assert digests[1].size_bytes == 4  # b'defg'
    assert digests[2].size_bytes == 4  # b'hijk
    assert len(any_index._queue_of_whereclauses) == 2

    # Test 5: A window_size of 10 should contain all 7 blobs, hence 1 windows upon renewal.
    # The whereclause renewal also won't happen unless request_size > premarked_size=11.
    any_index._all_blobs_window_size = 10
    request_size = 12
    digests = any_index.mark_n_bytes_as_deleted(request_size, renew_windows=True)
    assert len(digests) == 5
    assert digests[0].size_bytes == 3  # b'abc'
    assert digests[1].size_bytes == 4  # b'defg'
    assert digests[2].size_bytes == 4  # b'hijk
    assert digests[3].size_bytes == 0  # b''
    assert digests[4].size_bytes == 4  # b'lmnr'
    assert len(any_index._queue_of_whereclauses) == 1


def _query_for_entry_by_digest(digest):
    return select(IndexEntry).where(
        IndexEntry.digest_hash == digest.hash
    )

@check_for_postgres
def test_bulk_delete(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # Mark some entries as deleted
    old_digests = [digest for blob, digest in blobs_digests]
    deleted_digests = any_index.mark_n_bytes_as_deleted(7)
    deleted_hashes = set([x.hash for x in deleted_digests])
    any_index.bulk_delete(deleted_digests)

    for digest in deleted_digests:
        statement1 = _query_for_entry_by_digest(digest)
    # Check that only the right things were deleted
    diff = [digest for digest in old_digests if digest.hash not in deleted_hashes]
    for digest in diff:
        statement2 = _query_for_entry_by_digest(digest)

    entry1 = []
    entry2 = []
    with any_index.session() as session:
        entry1 = session.execute(statement1).scalars().all()
        entry2 = session.execute(statement2).scalars().first()

    assert not entry1
    assert entry2


@check_for_postgres
def test_bulk_delete_test_inclause(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # make inclause smaller than length of blob_digests
    any_index._inclause_limit = 2

    # Mark some entries as deleted
    old_digests = [digest for blob, digest in blobs_digests]
    deleted_digests = any_index.mark_n_bytes_as_deleted(7)
    deleted_hashes = set([x.hash for x in deleted_digests])
    any_index.bulk_delete(deleted_digests)

    assert len(blobs_digests) > any_index._inclause_limit

    for digest in deleted_digests:
        statement1 = _query_for_entry_by_digest(digest)
    # Check that only the right things were deleted
    diff = [digest for digest in old_digests if digest.hash not in deleted_hashes]
    for digest in diff:
        statement2 = _query_for_entry_by_digest(digest)

    entry1 = []
    entry2 = []
    with any_index.session() as session:
        entry1 = session.execute(statement1).scalars().all()
        entry2 = session.execute(statement2).scalars().first()

    assert not entry1
    assert entry2
