# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import math
import random
import tempfile
import time
from unittest import mock

import boto3
import botocore
from botocore.exceptions import ClientError
from buildgrid.server.s3 import s3utils
import grpc
import errno
from moto import mock_s3
import psycopg2
import pytest
import fakeredis
import itertools
from unittest.mock import patch
from random import shuffle
from sqlalchemy.exc import DBAPIError

from buildgrid._exceptions import StorageFullError
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.rpc import code_pb2
from buildgrid.server.cas.storage.storage_abc import StorageABC
from buildgrid.server.cas.storage.remote import RemoteStorage
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.disk import DiskStorage
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.with_cache import WithCacheStorage
from buildgrid.server.cas.storage.size_differentiated import SizeDifferentiatedStorage
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.settings import HASH, MAX_IN_MEMORY_BLOB_SIZE_BYTES, S3_MAX_UPLOAD_SIZE, S3_MULTIPART_PART_SIZE

from ..utils.cas import serve_cas
from ..utils.fixtures import moto_server
from ..utils.utils import run_in_subprocess


BLOBS = [(b'abc', b'defg', b'hijk', b'', b'abcdefghijk')]
BLOBS_DIGESTS = [tuple([remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(),
                                                    size_bytes=len(blob)) for blob in blobs])
                 for blobs in BLOBS]
STORAGE_TYPES = [
    'lru',
    'disk',
    's3',
    'lru_disk',
    'lru_disk_deferred',
    'lru_disk_size_differentiated',
    'disk_s3',
    'remote',
    'redis',
    'sql_index',
    'sql_index_inlined',
    'tiny_lru_cache_s3',
    's3_in_mem_lru_index',
    'disk_with_unavailable_cache'
]


@pytest.fixture(params=STORAGE_TYPES)
def any_storage(request, moto_server):
    if request.param == 'lru':
        yield LRUMemoryCache(8129)

    elif request.param == 'disk':
        with tempfile.TemporaryDirectory() as path:
            yield DiskStorage(path)

    elif request.param == 's3':
        nums = [str(i) for i in range(10)]
        lets = [chr(i) for i in range(ord('a'), ord('g'))]
        hexvals = nums + lets
        with mock_s3():
            auth_args = {"aws_access_key_id": "access_key",
                         "aws_secret_access_key": "secret_key"}
            for i in range(len(hexvals)):
                for j in range(len(hexvals)):
                    bucket_name = 'testing--' + hexvals[i] + hexvals[j]
                    boto3.resource('s3', **auth_args).create_bucket(Bucket=bucket_name)
            # limit page size to test functionality
            yield S3Storage('testing--{digest[0]}{digest[1]}', page_size=3)

    elif request.param == 'lru_disk':
        # LRU cache with a uselessly small limit, so requests always fall back
        with tempfile.TemporaryDirectory() as path:
            yield WithCacheStorage(LRUMemoryCache(1), DiskStorage(path))

    elif request.param == 'lru_disk_size_differentiated':
        with tempfile.TemporaryDirectory() as path:
            lru = LRUMemoryCache(10)
            disk = DiskStorage(path)
            yield SizeDifferentiatedStorage([{'storage': lru, 'max_size': 3}], disk)

    elif request.param == 'lru_disk_deferred':
        # LRU cache with a disk fallback, with writes to the fallback deferred
        # to a background thread to increase write request performance.
        with tempfile.TemporaryDirectory() as path:
            deferred_storage = WithCacheStorage(LRUMemoryCache(2048),
                                                DiskStorage(path),
                                                defer_fallback_writes=True)
            yield deferred_storage
            # Call __del__ explicitly to ensure that the thread pool
            # is stopped before the temporary directory is cleaned up
            #
            # ThreadPoolExecutor.shutdown() is idempotent, so it should
            # be ok to call __del__ multiple times
            deferred_storage.__del__()


    elif request.param == 'disk_s3':
        # Disk-based cache of S3, but we don't delete files, so requests
        # are always handled by the cache
        with tempfile.TemporaryDirectory() as path:
            with mock_s3():
                auth_args = {"aws_access_key_id": "access_key",
                             "aws_secret_access_key": "secret_key"}
                boto3.resource('s3', **auth_args).create_bucket(Bucket='testing')
                yield WithCacheStorage(DiskStorage(path), S3Storage('testing'))

    elif request.param == 'remote':
        with serve_cas(['testing']) as server:
            # Defer remote storage initialization to avoid use of gRPC
            # in the main process.
            yield lambda: _prepare_remote_storage(server.remote)

    elif request.param == 'remote_without_bytestream':
        # CAS server without a Bytestream service to verify use of
        # batch requests. Not enabled by default.
        with serve_cas(['testing'], bytestream=False) as server:
            # Defer remote storage initialization to avoid use of gRPC
            # in the main process.
            yield lambda: _prepare_remote_storage(server.remote)

    elif request.param == 'redis':
        with patch('buildgrid.server.cas.storage.redis.redis.Redis', fakeredis.FakeRedis):
            from buildgrid.server.cas.storage.redis import RedisStorage

            input_dict = {
                'host': "localhost",
                'port': 8000,
                'db': 0
            }
            yield RedisStorage(**input_dict)

    elif request.param == 'sql_index':
        storage = LRUMemoryCache(8129)
        with tempfile.NamedTemporaryFile() as db:
            yield SQLIndex(
                storage=storage,
                connection_string=f"sqlite:///{db.name}",
                automigrate=True)

    elif request.param == 'sql_index_inlined':
        storage = LRUMemoryCache(8129)
        with tempfile.NamedTemporaryFile() as db:
            yield SQLIndex(
                storage=storage,
                connection_string=f"sqlite:///{db.name}",
                automigrate=True,
                max_inline_blob_size=256)

    elif request.param == 'tiny_lru_cache_s3':
        # LRU in-memory cache of S3, but the LRU size is set to 1 so all
        # requests are handled by the fallback
        with mock_s3():
            auth_args = {"aws_access_key_id": "access_key",
                         "aws_secret_access_key": "secret_key"}
            boto3.resource('s3', **auth_args).create_bucket(Bucket='testing')
            yield WithCacheStorage(LRUMemoryCache(1), S3Storage('testing'))

    elif request.param == 's3_in_mem_lru_index':
        # index pointing to s3+in_mem_lru cache storage, with the lru sized to handle
        # some blobs but not others
        with mock_s3():
            auth_args = {"aws_access_key_id": "access_key",
                         "aws_secret_access_key": "secret_key"}
            boto3.resource('s3', **auth_args).create_bucket(Bucket='testing')
            storage = WithCacheStorage(LRUMemoryCache(8), S3Storage('testing'))
            with tempfile.NamedTemporaryFile() as db:
                yield SQLIndex(
                    storage=storage,
                    connection_string=f"sqlite:///{db.name}",
                    automigrate=True)

    elif request.param == 'disk_with_unavailable_cache':
        with tempfile.TemporaryDirectory() as path:
            def prepare_storage():
                # Domains in the `invalid` TLD can be assumed to not exist (RFC 6761)
                remote = RemoteStorage('http://unavailable.invalid', 'testing')
                remote.setup_grpc()
                return WithCacheStorage(remote, DiskStorage(path))
            yield prepare_storage


def write(storage, digest, blob):
    session = storage.begin_write(digest)
    session.write(blob)
    storage.commit_write(digest, session)


# Helper function for setting up remote storage:
def _prepare_remote_storage(remote, *, request_timeout=None):
    remote_storage = RemoteStorage(remote, 'testing', request_timeout=request_timeout)
    remote_storage.setup_grpc()
    return remote_storage


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_initially_empty(any_storage, blobs_digests):
    _, digests = blobs_digests

    # Actual test function, failing on assertions:
    def __test_initially_empty(any_storage, digests):
        if callable(any_storage):
            any_storage = any_storage()
        for digest in digests:
            assert not any_storage.has_blob(digest)
        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_initially_empty, any_storage, digests)
    else:
        __test_initially_empty(any_storage, digests)


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_basic_write_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    # Actual test function, failing on assertions:
    def __test_basic_write_read(any_storage, blobs, digests):
        if callable(any_storage):
            any_storage = any_storage()
        for blob, digest in zip(blobs, digests):
            assert not any_storage.has_blob(digest)
            write(any_storage, digest, blob)
            assert any_storage.has_blob(digest)
            assert any_storage.get_blob(digest).read() == blob

            # Try seeking on that blob
            read_head = any_storage.get_blob(digest)
            read_head.seek(1)
            assert read_head.read() == blob[1:]

            # Try writing the same digest again (since it's valid to do that)
            write(any_storage, digest, blob)
            assert any_storage.has_blob(digest)
            assert any_storage.get_blob(digest).read() == blob
        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_basic_write_read,
                                 any_storage, blobs, digests)
    else:
        __test_basic_write_read(any_storage, blobs, digests)


@pytest.mark.parametrize('any_storage', ['s3', 'remote'], indirect=['any_storage'])
@pytest.mark.xdist_group('mock-s3-client')
def test_basic_write_read_large_blob(any_storage):
    # Some storages use an intermediary temporary file when reading files
    # larger than `MAX_IN_MEMORY_BLOB_SIZE_BYTES`; exercising that code path.
    large_blob = b'large blob' + b'.' * MAX_IN_MEMORY_BLOB_SIZE_BYTES
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(hash=large_blob_hash,
                                                    size_bytes=len(large_blob))

    # Actual test function, failing on assertions:
    def __test_basic_write_read(any_storage):
        if callable(any_storage):
            any_storage = any_storage()
        assert not any_storage.has_blob(large_blob_digest)
        write(any_storage, large_blob_digest, large_blob)
        assert any_storage.has_blob(large_blob_digest)
        assert any_storage.get_blob(large_blob_digest).read() == large_blob
        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_basic_write_read, any_storage)
    else:
        __test_basic_write_read(any_storage)


@pytest.mark.parametrize('any_storage', ['s3'], indirect=['any_storage'])
@pytest.mark.parametrize('blob_size', [2 * S3_MULTIPART_PART_SIZE, 2 * S3_MULTIPART_PART_SIZE + 10])
@pytest.mark.xdist_group('mock-s3-client')
def test_multipart_upload(any_storage, blob_size):
    large_blob = random.getrandbits(blob_size * 8).to_bytes(blob_size, 'little')
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(
        hash=large_blob_hash,
        size_bytes=len(large_blob)
    )

    # Wrap the multipart upload code path so we can check the call
    call_count = 0
    orig = s3utils.start_multipart_upload
    def _wrapped_multipart_upload(*args, **kwargs):
        nonlocal call_count
        call_count += 1
        return orig(*args, **kwargs)

    with mock.patch('buildgrid.server.cas.storage.s3.s3utils.start_multipart_upload', new=_wrapped_multipart_upload):
        assert not any_storage.has_blob(large_blob_digest)
        write(any_storage, large_blob_digest, large_blob)
        assert any_storage.has_blob(large_blob_digest)

    # Check that we actually performed a multipart upload
    assert call_count == 1

    # Check that the blob was uploaded correctly
    response = any_storage._s3.get_object(
        Bucket=any_storage._get_bucket_name(large_blob_digest.hash),
        Key=any_storage._construct_key(large_blob_digest)
    )
    assert response['Body'].read() == large_blob

    # Check that the blob was uploaded in the expected number of parts
    response = any_storage._s3.head_object(
        Bucket=any_storage._get_bucket_name(large_blob_digest.hash),
        Key=any_storage._construct_key(large_blob_digest),
        PartNumber=1
    )
    assert response['PartsCount'] == math.ceil(len(large_blob) / S3_MULTIPART_PART_SIZE)


@pytest.mark.parametrize('any_storage', ['s3'], indirect=['any_storage'])
@pytest.mark.xdist_group('mock-s3-client')
def test_multipart_abort(any_storage):
    large_blob = b'large blob' + b'.' * S3_MAX_UPLOAD_SIZE
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(
        hash=large_blob_hash,
        size_bytes=len(large_blob)
    )

    abort_call_count = 0
    orig_abort = s3utils.abort_multipart_upload
    def _wrapped_abort_upload(*args, **kwargs):
        nonlocal abort_call_count
        abort_call_count += 1
        return orig_abort(*args, **kwargs)

    with mock.patch('buildgrid.server.cas.storage.s3.s3utils.abort_multipart_upload', new=_wrapped_abort_upload):
        with mock.patch('buildgrid.server.cas.storage.s3.s3utils.upload_parts', side_effect=Exception('Bad upload')):
            assert not any_storage.has_blob(large_blob_digest)
            with pytest.raises(Exception) as e:
                write(any_storage, large_blob_digest, large_blob)

    assert abort_call_count == 1


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_write_if_storage_is_full(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    def __test_basic_write(any_storage, blobs, digests):
        with pytest.raises(StorageFullError):
            for blob, digest in zip(blobs, digests):
                write(any_storage, digest, blob)

    if isinstance(any_storage, S3Storage):
        with patch('buildgrid.server.s3.s3utils.put_objects') as m:
            response = {'Error': {'Code': 'QuotaExceededException'}}
            err = ClientError(response, "Error")
            def mock_put_objects(s3, objects):
                for s3object in objects:
                    s3object.error = err
            m.side_effect = mock_put_objects
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, SQLIndex):
        with patch('buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index') as m:
            orig = psycopg2.OperationalError()
            orig.__setstate__({"pgerror": psycopg2.errors.DiskFull})
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

        with patch('buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index') as m:
            orig = psycopg2.OperationalError()
            orig.__setstate__({"pgerror": psycopg2.errors.OutOfMemory})
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, DiskStorage):
        with patch('os.makedirs') as m:
            error = OSError()
            error.errno = errno.ENOSPC
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

        with patch('os.link') as m:
            error = OSError()
            error.errno = errno.EFBIG
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_write_if_storage_errors_not_related_to_full(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    def __test_basic_write(any_storage, blobs, digests):
        try:
            for blob, digest in zip(blobs, digests):
                write(any_storage, digest, blob)
        except StorageFullError:
            pytest.fail("StorageFullError should not have been raised")
        except Exception:
            pass
        else:
            pytest.fail("Exception expected")

    if isinstance(any_storage, S3Storage):
        with patch('buildgrid.server.s3.s3utils.put_objects') as m:
            response = {'Error': {'Code': 'LimitExceededException'}}
            err = ClientError(response, "Error")
            def mock_put_objects(s3, objects):
                for s3object in objects:
                    s3object.error = err
            m.side_effect = mock_put_objects
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, SQLIndex):
        with patch('buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index') as m:
            orig = psycopg2.OperationalError()
            # Invalid SQL Statement Name
            orig.__setstate__({"pgerror": psycopg2.errors.InvalidSqlStatementName})
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, DiskStorage):
        with patch('os.makedirs') as m:
            error = OSError()
            # Permission Denied Error
            error.errno = errno.EACCES
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_deletes(any_storage, blobs_digests):
    """ Test the functionality of deletes.

    Deleting a blob should cause has_blob to return False and
    get_blob to return None.
    """
    blobs, digests = blobs_digests
    # any_storage returns a string for remote storage. Since deletes
    # are not supported with remote storage, we ignore those
    if isinstance(any_storage, StorageABC):
        for blob, digest in zip(blobs, digests):
            write(any_storage, digest, blob)

        for blob, digest in zip(blobs, digests):
            assert any_storage.has_blob(digest)
            assert any_storage.get_blob(digest).read() == blob

        first_digest, *_ = digests

        any_storage.delete_blob(first_digest)

        for blob, digest in zip(blobs, digests):
            if digest != first_digest:
                assert any_storage.has_blob(digest)
                assert any_storage.get_blob(digest).read() == blob
            else:
                assert not any_storage.has_blob(digest)
                assert any_storage.get_blob(digest) is None

        # There shouldn't be any issue with deleting a blob that isn't there
        missing_digest = remote_execution_pb2.Digest(hash=HASH(b'missing_blob').hexdigest(),
                                                     size_bytes=len(b'missing_blob'))
        assert not any_storage.has_blob(missing_digest)
        assert any_storage.get_blob(missing_digest) is None
        any_storage.delete_blob(missing_digest)
        assert not any_storage.has_blob(missing_digest)
        assert any_storage.get_blob(missing_digest) is None


@pytest.mark.xdist_group('mock-s3-client')
def test_bulk_deletes(any_storage):
    """ Test the functionality of deletes.

    Deleting a blob should cause has_blob to return False and
    get_blob to return None.
    """
    permutations = list(itertools.permutations([b'a', b'b', b'c', b'd', b'e', b'f']))
    blobs = [b''.join(x) for x in permutations]
    digests = [remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(),
                                           size_bytes=len(blob)) for blob in blobs]

    # any_storage returns a string for remote storage. Since deletes
    # are not supported with remote storage, we ignore those
    if isinstance(any_storage, StorageABC):
        results = any_storage.bulk_update_blobs(list(zip(digests, blobs)))
        assert all(status.code == code_pb2.OK for status in results)

        # Test all S3 bulk_delete code paths by deleting
        # digests with different corresponding bucket sizes
        deleted_blobs = [b'abecfd',  # bucket 4f
                         b'abcdfe', b'bcadfe',  # bucket 98
                         b'abcedf', b'adcfeb', b'defcba',  # bucket 99
                         b'abcdef', b'bdecfa', b'cdfbea',  # bucket be (also contains b'ebcdaf')
                         b'abfced', b'bdeacf', b'cbedfa', b'facdeb', b'febcda',  # bucket 3d
                         b'abdfce', b'adefcb', b'becadf', b'bfdaec', b'dbeacf', b'dfaebc',  # bucket d9
                         b'acdbef', b'bedacf', b'befacd', b'defbac', b'ecbdaf', b'fadceb', b'fdecab']  # bucket ac

        # Shuffle list to catch any bugs related to ordering
        shuffle(deleted_blobs)

        # Get digests to delete
        deleted_hashes = set([HASH(x).hexdigest() for x in deleted_blobs])
        deleted_digests = [digest for digest in digests if digest.hash in deleted_hashes]

        # digests not in storage
        missing_blobs = [b'wxyz', b'zyxw']
        missing_digests = [remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(),
                                                       size_bytes=len(blob)) for blob in missing_blobs]
        for missing_digest in missing_digests:
            assert not any_storage.has_blob(missing_digest)

        deleted_digests += missing_digests
        failed = any_storage.bulk_delete(deleted_digests)

        # bulk_delete returns a list of digests that failed to
        # be deleted. This list should only include actual failures
        # rather than blobs that were missing. We don't expect any
        # real failures, despite having missing blobs in the
        # delete request
        assert len(failed) == 0

        # Check that digests in storage were deleted
        for digest in digests:
            if digest.hash not in deleted_hashes:
                assert any_storage.has_blob(digest)
            else:
                assert not any_storage.has_blob(digest)

        # Verify the missing blobs are still missing
        for missing_digest in missing_digests:
            assert not any_storage.has_blob(missing_digest)


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_bulk_write_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    # Actual test function, failing on assertions:
    def __test_bulk_write_read(any_storage, blobs, digests):
        if callable(any_storage):
            any_storage = any_storage()
        missing_digests = any_storage.missing_blobs(digests)
        assert len(missing_digests) == len(digests)
        for digest in digests:
            assert digest in missing_digests

        faulty_blobs = list(blobs)
        faulty_blobs[-1] = b'this-is-not-matching'

        results = any_storage.bulk_update_blobs(list(zip(digests, faulty_blobs)))
        assert len(results) == len(digests)
        for result, blob, digest in zip(results[:-1], faulty_blobs[:-1], digests[:-1]):
            assert result.code == 0
            assert any_storage.get_blob(digest).read() == blob
        assert results[-1].code != 0

        missing_digests = any_storage.missing_blobs(digests)
        assert len(missing_digests) == 1
        assert missing_digests[0] == digests[-1]

        blobmap = any_storage.bulk_read_blobs(digests)
        for blob, digest in zip(blobs[:-1], digests[:-1]):
            assert blobmap[digest.hash].read() == blob
        assert digests[-1].hash not in blobmap

        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_bulk_write_read,
                                 any_storage, blobs, digests)
    else:
        __test_bulk_write_read(any_storage, blobs, digests)


@pytest.mark.parametrize('any_storage', STORAGE_TYPES + ['remote_without_bytestream'], indirect=['any_storage'])
@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_duplicate_bulk_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    # Duplicate the blobs and digests
    duplicate_blobs = blobs + blobs
    duplicate_digests = digests + digests

    # Actual test function, failing on assertions:
    def __test_duplicate_bulk_read(any_storage, blobs, digests):
        if callable(any_storage):
            any_storage = any_storage()
        any_storage.bulk_update_blobs(list(zip(digests, blobs)))

        blobmap = any_storage.bulk_read_blobs(duplicate_digests)
        assert len(blobmap) == len(blobs)
        for blob, digest in zip(blobs, digests):
            assert blobmap[digest.hash].read() == blob
        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_duplicate_bulk_read,
                                 any_storage, blobs, digests)
    else:
        __test_duplicate_bulk_read(any_storage, blobs, digests)


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.xdist_group('mock-s3-client')
def test_nonexistent_read(any_storage, blobs_digests):
    _, digests = blobs_digests

    # Actual test function, failing on assertions:
    def __test_nonexistent_read(any_storage, digests):
        if callable(any_storage):
            any_storage = any_storage()
        for digest in digests:
            assert any_storage.get_blob(digest) is None

        # Testing a blob that exceeds the limit to be kept completely in memory:
        large_blob_digest = remote_execution_pb2.Digest(hash=HASH(b'').hexdigest(),
                                                        size_bytes = MAX_IN_MEMORY_BLOB_SIZE_BYTES * 2)
        assert any_storage.get_blob(large_blob_digest) is None

        return True

    if callable(any_storage):
        assert run_in_subprocess(__test_nonexistent_read, any_storage, digests)
    else:
        __test_nonexistent_read(any_storage, digests)


@pytest.mark.parametrize('blobs_digests', [(BLOBS[0], BLOBS_DIGESTS[0])])
@pytest.mark.xdist_group('mock-s3-client')
def test_lru_eviction(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    lru = LRUMemoryCache(8)
    write(lru, digest1, blob1)
    write(lru, digest2, blob2)
    assert lru.has_blob(digest1)
    assert lru.has_blob(digest2)

    write(lru, digest3, blob3)
    # Check that the LRU evicted blob1 (it was written first)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    assert lru.get_blob(digest2).read() == blob2
    write(lru, digest1, blob1)
    # Check that the LRU evicted blob3 (since we just read blob2)
    assert lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert not lru.has_blob(digest3)

    write(lru, digest3, blob3)
    # Check that the LRU evicted blob1 (since we just checked blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    # Write the older blob (blob3) into the cache again,
    # and verify nothing is evicted.
    write(lru, digest3, blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    # deleting a blob and readding it shouldn't trigger eviction
    lru.delete_blob(digest3)
    write(lru, digest3, blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)


@pytest.mark.parametrize('blobs_digests', [(BLOBS[0], BLOBS_DIGESTS[0])])
@pytest.mark.xdist_group('mock-s3-client')
def test_with_cache(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)
    fallback = LRUMemoryCache(1024)
    with_cache_storage = WithCacheStorage(cache, fallback)

    assert not with_cache_storage.has_blob(digest1)
    write(with_cache_storage, digest1, blob1)
    assert cache.has_blob(digest1)
    assert fallback.has_blob(digest1)
    assert with_cache_storage.get_blob(digest1).read() == blob1

    # Even if a blob is in cache, we still need to check if the fallback
    # has it.
    write(cache, digest2, blob2)
    assert not with_cache_storage.has_blob(digest2)
    write(fallback, digest2, blob2)
    assert with_cache_storage.has_blob(digest2)

    # When a blob is in the fallback but not the cache, reading it should
    # put it into the cache.
    write(fallback, digest3, blob3)
    assert with_cache_storage.get_blob(digest3).read() == blob3
    assert cache.has_blob(digest3)
    assert cache.get_blob(digest3).read() == blob3

@pytest.mark.parametrize('blobs_digests', [(BLOBS[0], BLOBS_DIGESTS[0])])
@pytest.mark.xdist_group('mock-s3-client')
def test_bulk_read_with_cache(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)
    fallback = LRUMemoryCache(1024)
    with_cache_storage = WithCacheStorage(cache, fallback)

    write(with_cache_storage, digest1, blob1)
    write(cache, digest2, blob2)

    # When a blob is in the fallback but not the cache, bulk reading blobs
    # should put it into the cache.
    write(fallback, digest3, blob3)
    assert with_cache_storage.bulk_read_blobs(digests)
    assert cache.has_blob(digest3)
    assert cache.get_blob(digest3).read() == blob3

@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.parametrize('fallback', ['disk', 's3'])
@pytest.mark.xdist_group('mock-s3-client')
def test_with_cache_deferred_writes(blobs_digests, fallback, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)

    def _test_deferred_writes(fallback_storage):
        with_cache_storage = WithCacheStorage(cache, fallback_storage, defer_fallback_writes=True)

        assert not with_cache_storage.has_blob(digest1)
        write(with_cache_storage, digest1, blob1)

        assert cache.has_blob(digest1)
        assert with_cache_storage.get_blob(digest1).read() == blob1

        # Check that the fallback eventually has the blob
        attempts = 0
        while attempts <= 10:
            attempts += 1
            try:
                assert fallback_storage.has_blob(digest1)
            except:
                if attempts < 10:
                    time.sleep(5)
                    continue
                raise

        # With deferred writes, `has_blob` should return True if the blob is in
        # the cache layer without checking the fallback
        write(cache, digest2, blob2)
        assert with_cache_storage.has_blob(digest2)
        write(fallback_storage, digest2, blob2)
        assert with_cache_storage.has_blob(digest2)

        # When a blob is in the fallback but not the cache, reading it should
        # put it into the cache.
        write(fallback_storage, digest3, blob3)
        assert with_cache_storage.get_blob(digest3).read() == blob3
        assert cache.has_blob(digest3)
        assert cache.get_blob(digest3).read() == blob3

    if fallback == 'disk':
        with tempfile.TemporaryDirectory() as path:
            _test_deferred_writes(DiskStorage(path))

    elif fallback == 's3':
        with mock_s3():
            auth_args = {"aws_access_key_id": "access_key",
                            "aws_secret_access_key": "secret_key"}
            boto3.resource('s3', **auth_args).create_bucket(Bucket='testing')
            _test_deferred_writes(S3Storage('testing'))


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.parametrize('fallback', ['disk'])
def test_with_cache_deferred_writes_shuts_down_pool(blobs_digests, fallback):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)

    def _test_deferred_writes(fallback_storage):
        with_cache_storage = WithCacheStorage(cache, fallback_storage, defer_fallback_writes=True)

        assert not with_cache_storage.has_blob(digest1)
        write(with_cache_storage, digest1, blob1)

        assert cache.has_blob(digest1)
        assert with_cache_storage.get_blob(digest1).read() == blob1

        # With deferred writes, `has_blob` should return True if the blob is in
        # the cache layer without checking the fallback
        write(cache, digest2, blob2)
        assert with_cache_storage.has_blob(digest2)
        write(fallback_storage, digest2, blob2)
        assert with_cache_storage.has_blob(digest2)

        # When a blob is in the fallback but not the cache, reading it should
        # put it into the cache.
        write(fallback_storage, digest3, blob3)
        assert with_cache_storage.get_blob(digest3).read() == blob3
        assert cache.has_blob(digest3)
        assert cache.get_blob(digest3).read() == blob3

    # DON'T ACTUALLY WRITE THE BLOBS to avoid flakiness
    # with the thread writing blob after the contextmanager exits
    with patch('buildgrid.server.cas.storage.with_cache.ThreadPoolExecutor.shutdown') as shutdown:
        with patch('buildgrid.server.cas.storage.with_cache.ThreadPoolExecutor.submit') as submit:
            with tempfile.TemporaryDirectory() as path:
                _test_deferred_writes(DiskStorage(path))
                # This should've been called once to write the blob
                assert submit.called_once()
        assert shutdown.called_once()


@pytest.mark.parametrize('blobs_digests', zip(BLOBS, BLOBS_DIGESTS))
def test_remote_timeout(blobs_digests):
    _, digests = blobs_digests

    # Actual test function, failing on assertions:
    def __test_remote_timeout(remote):
        storage = _prepare_remote_storage(remote, request_timeout=0.5)

        storage.get_blob(digests[0])

    with serve_cas(['testing'], delay=5) as server:
        with pytest.raises(TimeoutError):
            run_in_subprocess(__test_remote_timeout, server.remote)
