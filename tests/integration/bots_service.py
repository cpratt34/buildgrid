# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import asyncio
from collections import Counter
from contextlib import contextmanager
from datetime import datetime, timedelta
import hashlib
import itertools
import json
import random
import tempfile
import time
from unittest import mock

from google.protobuf import any_pb2
import grpc
from grpc._server import _Context
import pytest

from buildgrid._enums import LeaseState, BotStatus
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import LogStream
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.rpc import code_pb2, status_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.bots import service
from buildgrid.server.bots.service import BotsService
from buildgrid.server.bots.instance import BotsInterface
from buildgrid.server.metrics_names import (
    BOTS_ASSIGN_JOB_LEASES_TIME_METRIC_NAME,
    BOTS_CREATE_BOT_SESSION_TIME_METRIC_NAME,
    BOTS_UPDATE_BOT_SESSION_TIME_METRIC_NAME,
    SCHEDULER_UPDATE_LEASE_TIME_METRIC_NAME
)
from buildgrid.server.monitoring import _MonitoringBus
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore
from tests.utils.metrics import mock_create_timer_record


server = mock.create_autospec(grpc.server)


# GRPC context
@pytest.fixture
def context():
    context_mock = mock.MagicMock(spec=_Context)
    context_mock.time_remaining.return_value = 1
    yield context_mock

# Context with a longer mock time_remaining, allows for the JobAssigner to
# hand off leases reliably
@pytest.fixture
def long_context():
    context_mock = mock.MagicMock(spec=_Context)
    context_mock.time_remaining.return_value = 3
    yield context_mock


@pytest.fixture
def bot_session():
    bot = bots_pb2.BotSession()
    bot.bot_id = 'ana'
    bot.status = BotStatus.OK.value
    yield bot


BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS = [None, 1, 2]
DATA_STORE_IMPLS = ["sql", "mem"]

PARAMS = [(impl, timeout)
          for timeout in BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS
          for impl in DATA_STORE_IMPLS]


def mock_logstream_client(suffix=''):
    @contextmanager
    def _mock_logstream_client(channel, instance_name):
        class LogStreamClient:

            def create(self, parent: str) -> LogStream:
                return LogStream(
                    name=f'{parent}/mock-logstream{suffix}',
                    write_resource_name=f'{parent}/mock-logstream{suffix}/write'
                )

            def close(self):
                pass

        yield LogStreamClient()

    return _mock_logstream_client


def setup_function():
    # We need to explicitly set an event loop here, since pytest-aiohttp
    # seems to close the default/implicit loop we rely on in our asyncio
    # code. Without explicitly creating a new loop, this leads to all tests
    # that touch asyncio-related code and run in the same test process as
    # the bgd-browser backend tests failing due to the lack of a running
    # event loop in the current thread.
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)


@pytest.fixture(params=PARAMS)
def bots_interface(request):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    impl, timeout = request.param
    instance = None
    try:
        if impl == "sql":
            db = tempfile.NamedTemporaryFile().name
            data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
        elif impl == "mem":
            data_store = MemoryDataStore(storage)
        instance = BotsInterface(data_store, bot_session_keepalive_timeout=timeout)
        instance.start()
        yield instance

    finally:
        if instance is not None:
            instance.stop()


@pytest.fixture(params=[timeout for timeout in [None, 1]])
def bots_interface_permissive(request):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    timeout = request.param

    # Permissive BotSession only works with shared state, so only test with SQL DataStore
    db = tempfile.NamedTemporaryFile().name
    data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)

    try:
        instance = BotsInterface(
            data_store,
            bot_session_keepalive_timeout=timeout,
            permissive_bot_session=True
        )
        instance.start()
        yield instance
    finally:
        data_store.watcher_keep_running = False


# Instance to test
@pytest.fixture
def bots_service(bots_interface):
    instances = {"": bots_interface}
    with mock.patch.object(service, 'bots_pb2_grpc'):
        bots_service = BotsService(server)
        for k, v in instances.items():
            bots_service.add_instance(k, v)
        yield bots_service


# Instance to test with monitoring enabled
@pytest.fixture
def bots_service_monitored(bots_interface):
    instances = {"": bots_interface}
    with mock.patch.object(service, 'bots_pb2_grpc'):
        bots_service = BotsService(server, monitor=True)
        for k, v in instances.items():
            bots_service.add_instance(k, v)
        yield bots_service


# Instances to test with permissive mode enabled
@pytest.fixture
def bots_service_permissive_botsession_1(bots_interface_permissive):
    instances = {"": bots_interface_permissive}
    with mock.patch.object(service, 'bots_pb2_grpc'):
        bots_service = BotsService(server)
        for k, v in instances.items():
            bots_service.add_instance(k, v)
        yield bots_service


# Same as #1, but individually constructed
bots_service_permissive_botsession_2 = bots_service_permissive_botsession_1


def _inject_work(scheduler, action=None, action_digest=None,
                 platform_requirements=None):
    if not action:
        action = remote_execution_pb2.Action()

    if not action_digest:
        action_digest = remote_execution_pb2.Digest()

    scheduler.queue_job_action(action, action_digest, platform_requirements,
                               skip_cache_lookup=True)


BOT_COUNT = [2]
HISTORY_LENGTH = [3]

BOT_SESSION_HISTORY = [(bot_count, history_len)
                       for bot_count in BOT_COUNT
                       for history_len in HISTORY_LENGTH]


@pytest.fixture(params=BOT_SESSION_HISTORY)
def update_bot_session_history(request):
    yield request.param


def check_bot_session_request_response_and_assigned_expiry(bots_service, request, response, request_time):
    assert isinstance(response, bots_pb2.BotSession)

    bot_session_keepalive_timeout = bots_service._instances[""]._bot_session_keepalive_timeout

    assert request.bot_id == response.bot_id

    if bot_session_keepalive_timeout:
        # See if expiry time was set (when enabled)
        assert response.expire_time.IsInitialized()
        # See if it was set to an expected time
        earliest_expire_time = request_time + timedelta(seconds=bot_session_keepalive_timeout)
        # We don't want to do strict comparisons because we are not looking for
        # exact time precision (e.g. in ms/ns)
        response_expire_time = response.expire_time.ToDatetime()
        assert response_expire_time >= earliest_expire_time


@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_create_bot_session(bot_session, context, bots_service):
    _MonitoringBus._instance = mock.Mock()
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    # Asserting that metrics were collected:
    mock_timing_record = mock_create_timer_record(name=BOTS_CREATE_BOT_SESSION_TIME_METRIC_NAME)
    mock_instanced_timing_record = mock_create_timer_record(
        name=BOTS_CREATE_BOT_SESSION_TIME_METRIC_NAME,
        metadata={'instance-name': ""}
    )

    call_list = [mock.call(mock_timing_record), mock.call(mock_instanced_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_lease_assignment_creates_logstream(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    bots_interface._logstream_channel = mock.MagicMock()
    bots_interface._logstream_instance_name = 'testing'

    _inject_work(bots_interface._scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch('buildgrid.server.bots.instance.logstream_client', new=mock_logstream_client()):
        response = bots_service.CreateBotSession(request, long_context)

        # Make sure a lease was assigned
        assert len(response.leases) == 1

        lease = response.leases[0]
        job = bots_interface._data_store.get_job_by_name(lease.id)
        assert job is not None

        metadata = job.get_metadata()
        assert metadata.stdout_stream_name
        assert metadata.stdout_stream_name.endswith('stdout/mock-logstream')
        assert metadata.stderr_stream_name
        assert metadata.stderr_stream_name.endswith('stderr/mock-logstream')


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_lease_retry_creates_new_logstream(bot_session, long_context, bots_service):
    bots_interface: BotsInterface = bots_service._get_instance("")
    bots_interface._logstream_channel = mock.MagicMock()
    bots_interface._logstream_instance_name = 'testing'

    _inject_work(bots_interface._scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch('buildgrid.server.bots.instance.logstream_client', new=mock_logstream_client()):
        response = bots_service.CreateBotSession(request, long_context)

        # Make sure a lease was assigned
        assert len(response.leases) == 1

        lease = response.leases[0]
        job = bots_interface._data_store.get_job_by_name(lease.id)
        assert job is not None

        metadata = job.get_metadata()
        assert metadata.stdout_stream_name
        assert metadata.stdout_stream_name.endswith('stdout/mock-logstream')
        assert metadata.stderr_stream_name
        assert metadata.stderr_stream_name.endswith('stderr/mock-logstream')

        bots_interface._close_bot_session(response.name, reason='forcing lease retry for test')

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch('buildgrid.server.bots.instance.logstream_client', new=mock_logstream_client(suffix='-second')):
        response = bots_service.CreateBotSession(request, long_context)

        # Make sure a lease was assigned
        assert len(response.leases) == 1

        lease = response.leases[0]
        job = bots_interface._data_store.get_job_by_name(lease.id)
        assert job is not None

        metadata = job.get_metadata()
        assert metadata.stdout_stream_name
        assert metadata.stdout_stream_name.endswith('stdout/mock-logstream-second')
        assert metadata.stderr_stream_name
        assert metadata.stderr_stream_name.endswith('stderr/mock-logstream-second')


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_retryable_not_ok_lease_status(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # The lease should have been rescheduled, and since
    # this is the only bot connected it'll get the lease again
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1
    assert bot.leases[0].state == LeaseState.PENDING.value


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_non_retryable_lease_status(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1
    leaseId = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.FAILED_PRECONDITION))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # The lease should not have been rescheduled due to the status
    # not being retryable
    assert len(bot.leases) == 0
    assert len(bots_interface._assigned_leases[bot.name]) == 0

    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease is None


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_non_ok_retries_exhausted(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler
    scheduler.MAX_N_TRIES = 2

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1
    leaseId = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('ACTIVE')
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)
    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease.state == bots_pb2.LeaseState.Value('ACTIVE')

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # The lease should have been rescheduled, and since
    # this is the only bot connected it'll get the lease again
    assert len(bot.leases) == 1
    assert bot.leases[0].id == leaseId
    assert len(bots_interface._assigned_leases[bot.name]) == 1
    assert bot.leases[0].state == LeaseState.PENDING.value

    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease.state == bots_pb2.LeaseState.Value('PENDING')

    bot.leases[0].state = bots_pb2.LeaseState.Value('ACTIVE')
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)
    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease.state == bots_pb2.LeaseState.Value('ACTIVE')

    # Send a non-OK lease status again, and verify it doesn't get
    # rescheduled
    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request no matter what
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    assert len(bot.leases) == 0
    assert len(bots_interface._assigned_leases[bot.name]) == 0

    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease is None


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_complete_lease(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1
    leaseId = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('ACTIVE')
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)
    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease.state == bots_pb2.LeaseState.Value('ACTIVE')

    # Make the bot call UpdateBotSession with a COMPLETED lease with
    # a dummy ActionResult in the payload
    action_result = remote_execution_pb2.ActionResult()
    action_result_in_any_proto = any_pb2.Any()
    action_result_in_any_proto.Pack(action_result)

    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    bot.leases[0].payload.CopyFrom(action_result_in_any_proto)
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.OK))

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # Verify that there are no other leases assigned to the bot
    # and that the existing lease has been removed
    assert len(bot.leases) == 0
    assert len(bots_interface._assigned_leases[bot.name]) == 0

    internal_lease = scheduler.get_job_lease(leaseId)
    assert internal_lease is None


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_job_assigner_queue_empty_after_create_update_bot_session_no_work(bot_session, long_context, bots_service):
    """ This test verifies that a worker which calls CreateBotSession or UpdateBotSession
        with no work available doesn't leave any entries in the job_assigner
    """
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler
    job_assigner = bots_interface._job_assigner

    # Create BotSession and expect no lease to be assigned as there's no work
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)
    assert len(bot.leases) == 0
    assert len(bots_interface._assigned_leases[bot.name]) == 0
    
    # Verify that the JobAssigner has no entry for this worker
    assert bot.name not in job_assigner._worker_map

    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    # The bucket may have been cleaned up already, but it not existing is okay
    if len(job_assigner._buckets) > 0:
        assert len(job_assigner._buckets) == 1
        assert capability_hash in job_assigner._buckets
        assert len(job_assigner._buckets[capability_hash]) == 0

    # Make the bot call UpdateBotSession and also verify there's no entries
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # Verify that the JobAssigner has no entry for this worker
    assert bot.name not in job_assigner._worker_map

    # The bucket may have been cleaned up already, but it not existing is okay
    if len(job_assigner._buckets) > 0:
        assert len(job_assigner._buckets) == 1
        assert capability_hash in job_assigner._buckets
        assert len(job_assigner._buckets[capability_hash]) == 0
    
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_create_bot_session_bot_id_fail(context, bots_service):
    bot = bots_pb2.BotSession()

    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot)

    bots_service.CreateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_recreate_bot_session(bot_session, context, bots_service):
    # This test simulates bots re-connecting due to e.g. restarting suddenly
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    bots_interface = bots_service._get_instance("")
    with mock.patch.object(bots_interface, '_close_bot_session', autospec=True) as close_botsession_fn:
        request_time = datetime.utcnow()
        request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

        response = bots_service.CreateBotSession(request, context)
        new_botsession_bot_id = response.bot_id
        new_botsession_name = response.name

        # New BotSession should be for the same BotID
        assert initial_botsession_bot_id == new_botsession_bot_id
        # New BotSession should have a new name!
        assert initial_botsession_name != new_botsession_name

        check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

        # The previous botsession for this bot must have gotten closed
        assert close_botsession_fn.call_count == 1


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_recreate_bot_session_as_initial_bot_session_expires(bot_session, context, bots_service):
    # This tests edge cases in which a botsession is re-created as it expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    bots_interface = bots_service._get_instance("")

    # Simulate simultaneous expiry of earlier botsession
    # and recreation of a new BotSession with a `CreateBotSession` request

    # Re-create botsession (e.g. bot crashed)
    # Causes a call to `_close_bot_session`
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # Simultaneously, the reaper calls tries to close the botsession
    # Force expiry
    next_botsession_expiry_override = itertools.chain(
        [(initial_botsession_name, request_time)], itertools.repeat((None, None)))
    with mock.patch.object(bots_interface, '_get_next_botsession_expiry',
                           side_effect=next_botsession_expiry_override):
        bots_interface._reap_expired_sessions()

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_recreate_bot_session_after_expiry(bot_session, context, bots_service):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    bots_interface = bots_service._get_instance("")

    # Force expiry
    next_botsession_expiry_override = itertools.chain(
        [(initial_botsession_name, request_time)], itertools.repeat((None, None)))
    with mock.patch.object(bots_interface, '_get_next_botsession_expiry', side_effect=next_botsession_expiry_override):
        bots_interface._reap_expired_sessions()

    # Bot returns and creates a new session
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_recreated_bot_session_after_update_expiry_receives_leases(bot_session, context, long_context, bots_service):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession = response
    initial_botsession_bot_id = initial_botsession.bot_id
    initial_botsession_name = initial_botsession.name

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    bots_interface = bots_service._get_instance("")

    # Send an UpdateBotSession
    request = bots_pb2.UpdateBotSessionRequest(name=initial_botsession_name, bot_session=initial_botsession)
    response = bots_service.UpdateBotSession(request, context)

    # Force expiry
    next_botsession_expiry_override = itertools.chain(
        [(initial_botsession_name, request_time)], itertools.repeat((None, None)))
    with mock.patch.object(bots_interface, '_get_next_botsession_expiry', side_effect=next_botsession_expiry_override):
        bots_interface._reap_expired_sessions()

    # Add a job to be picked up by the reconnecting bot
    _inject_work(bots_interface._scheduler)
    
    # Bot returns and creates a new session
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, long_context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name
    # Assigned a job under the new botsession name
    assert len(response.leases) == 1
    assert len(bots_interface._assigned_leases[new_botsession_name]) == 1


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_zombie(bot_session, context, bots_service):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)
    # Update server with incorrect UUID by rotating it
    bot.name = bot.name[len(bot.name): 0]

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    bots_service.UpdateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_bot_id_fail(bot_session, context, bots_service):
    request = bots_pb2.UpdateBotSessionRequest(bot_session=bot_session)

    bots_service.UpdateBotSession(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_after_expiry(bot_session, context, bots_service):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession = response
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    bots_interface = bots_service._get_instance("")

    # Close bot session (e.g. expiry)
    bots_interface._close_bot_session(initial_botsession_name, reason='expired')

    # Bot now sends an `UpdateBotSession` request
    request = bots_pb2.UpdateBotSessionRequest(name=initial_botsession_name, bot_session=initial_botsession)
    response = bots_service.UpdateBotSession(request, context)

    # The response should contain the relevant error code
    context.set_code.assert_called_once_with(grpc.StatusCode.DATA_LOSS)
    # The botsession name of the response shouldn't match the initial name
    assert response.name != initial_botsession_name
    # The botsession name of the response should be None
    assert response.name == ''


@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session(bot_session, long_context, bots_service):
    _MonitoringBus._instance = mock.Mock()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    response = bots_service.UpdateBotSession(request, long_context)

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    # Asserting that metrics were collected:
    mock_update_record = mock_create_timer_record(name=BOTS_UPDATE_BOT_SESSION_TIME_METRIC_NAME)
    mock_instanced_timing_record = mock_create_timer_record(
        name=BOTS_UPDATE_BOT_SESSION_TIME_METRIC_NAME,
        metadata={'instance-name': ""}
    )
    mock_assignment_record = mock_create_timer_record(
        name=BOTS_ASSIGN_JOB_LEASES_TIME_METRIC_NAME,
        metadata={'instance-name': ""}
    )
    call_list = [
        mock.call(mock_update_record),
        mock.call(mock_assignment_record),
        mock.call(mock_instanced_timing_record)
    ]

    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_completed_no_request_leases(bot_session, long_context, bots_service):
    """When a Lease is completed, we shouldn't ask for new work in the same request.

    This behaviour avoids an issue where the Bots service going down whilst waiting
    for new work after removing a completed Lease leads to the worker attempting to
    update the completed Lease in future requests.

    """
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1

    # Have the bot complete the job
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value('COMPLETED')
    bots_interface._request_leases = mock.MagicMock()

    # We provide a deadline of 10s, but the deadline used is None
    bots_interface.update_bot_session(bot.name, bot, long_context, deadline=10)

    bots_interface._request_leases.assert_not_called()


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_bot_not_reporting_lease(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1

    # Mark the job as completed in the data store
    # so that retrying it will become a no-op
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value('COMPLETED')
    scheduler.update_job_lease_state(lease.id, lease)

    # Make the bot call UpdateBotSession without the lease
    # Expect the BotsInterface to retry the lease and unassign
    del bot.leases[0]
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    with mock.patch.object(scheduler, 'retry_job_lease', autospec=True) as scheduler_retry_fn:
        # UpdateBotSession and report no leases to BotsInterface
        bot = bots_service.UpdateBotSession(request, long_context)
        # BotsInterface should have called scheduler.retry once
        assert scheduler_retry_fn.call_count == 1
        # The bot received 0 leases (job was marked completed above, so retry was no-op)
        # and the BotsInterface has the same records
        assert len(bot.leases) == 0
        assert len(bots_interface._assigned_leases[bot.name]) == 0

        # Make sure this remains the case even after 2 UpdateBotSessions!
        bot = bots_service.UpdateBotSession(request, long_context)
        assert scheduler_retry_fn.call_count == 1  # still only called the first time, above
        assert len(bot.leases) == 0
        assert len(bots_interface._assigned_leases[bot.name]) == 0


@pytest.mark.parametrize("sleep_duration", [1, 2])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_and_wait_for_expiry(bot_session, context, bots_service, sleep_duration):
    bots_interface = bots_service._get_instance("")
    bot_session_keepalive_timeout = bots_interface._bot_session_keepalive_timeout

    with mock.patch.object(bots_interface, '_close_bot_session', autospec=True) as close_botsession_fn:
        def close_botsession_side_effect(name, *, reason):
            bots_interface._untrack_deadline_for_botsession(name)

        close_botsession_fn.side_effect = close_botsession_side_effect
        request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
        bot = bots_service.CreateBotSession(request, context)

        request_time = datetime.utcnow()
        request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                                   bot_session=bot)

        response = bots_service.UpdateBotSession(request, context)

        check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

        if bot_session_keepalive_timeout:
            assert bots_interface._next_expire_time_occurs_in() >= 0

        time.sleep(sleep_duration)
        # Call this manually since the asyncio event loop isn't running
        bots_interface._reap_expired_sessions()

        if bot_session_keepalive_timeout and bot_session_keepalive_timeout <= sleep_duration:
            # the BotSession should have expired after sleeping `sleep_duration`
            assert close_botsession_fn.call_count == 1
            # Should have no tracked expiries now
        else:
            # no timeout, or timeout > 1, shouldn't see any expiries yet.
            assert close_botsession_fn.call_count == 0


@pytest.mark.parametrize("bot_session_keepalive_timeout", BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS)
def test_bots_instance_sets_up_reaper_loop(bot_session_keepalive_timeout):
    main_loop = asyncio.get_event_loop()
    with mock.patch.object(main_loop, 'create_task', autospec=True) as loop_create_task_fn:
        with mock.patch.object(BotsInterface, '_reap_expired_sessions_loop', autospec=True):
            # Just instantiate and see whether the __init__ sets up the reaper loop when needed
            my_instance = BotsInterface(
                None, bot_session_keepalive_timeout=bot_session_keepalive_timeout)

            # If the timeout was set, the reaper task should have been created, otherwise it shouldn't
            if bot_session_keepalive_timeout:
                assert loop_create_task_fn.call_count == 1
            else:
                assert loop_create_task_fn.call_count == 0


@pytest.mark.parametrize("sleep_duration", [1, 2])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_botsession_expiry_cleanup(bot_session, context, bots_service, sleep_duration):
    bots_interface = bots_service._get_instance("")
    bot_session_keepalive_timeout = bots_interface._bot_session_keepalive_timeout

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)

    response = bots_service.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    if bot_session_keepalive_timeout:
        assert len(bots_interface._ordered_expire_times_by_botsession) == 1
        assert bots_interface._next_expire_time_occurs_in() >= 0

    time.sleep(sleep_duration)
    # Call this manually since the asyncio event loop isn't running
    bots_interface._reap_expired_sessions()

    if bot_session_keepalive_timeout and bot_session_keepalive_timeout <= sleep_duration:
        # Should have no tracked expiries now
        assert len(bots_interface._ordered_expire_times_by_botsession) == 0
        # Next expiry should be None
        assert bots_interface._next_expire_time_occurs_in() is None


@pytest.mark.parametrize("sleep_duration", [1, 2])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_botsession_expiry_cleanup_two_entries(bot_session, context, bots_service, sleep_duration):
    bots_interface = bots_service._get_instance("")
    bot_session_keepalive_timeout = bots_interface._bot_session_keepalive_timeout

    # Create two bots around the same time
    for bot_num in range(2):
        new_bot_session = bot_session
        new_bot_session.bot_id = f"{bot_session.bot_id}-{bot_num}"
        request = bots_pb2.CreateBotSessionRequest(bot_session=new_bot_session)
        bot = bots_service.CreateBotSession(request, context)

        request_time = datetime.utcnow()
        request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                                   bot_session=bot)

        response = bots_service.UpdateBotSession(request, context)

        check_bot_session_request_response_and_assigned_expiry(bots_service, bot_session, response, request_time)

    if bot_session_keepalive_timeout:
        assert len(bots_interface._ordered_expire_times_by_botsession) == 2
        assert bots_interface._next_expire_time_occurs_in() >= 0

    time.sleep(sleep_duration)
    # Call this manually since the asyncio event loop isn't running
    bots_interface._reap_expired_sessions()

    if bot_session_keepalive_timeout and bot_session_keepalive_timeout <= sleep_duration:
        # Verify both expired bot sessions were reaped with the single call
        assert len(bots_interface._ordered_expire_times_by_botsession) == 0
        # Next expiry should be None
        assert bots_interface._next_expire_time_occurs_in() is None


@pytest.mark.parametrize("number_of_jobs", [0, 1, 3, 500])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_number_of_leases(number_of_jobs, bot_session, long_context, bots_service):

    for _ in range(0, number_of_jobs):
        _inject_work(bots_service._instances[""]._scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, long_context)

    assert len(response.leases) == min(number_of_jobs, 1)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_createbotsession_leases_with_work(bot_session, long_context, bots_service):
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service._instances[""]._scheduler, action_digest=action_digest,
                 action=action)

    response = bots_service.CreateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    assert response.leases[0].state == LeaseState.PENDING.value

    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)
    assert response_action == action


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_updatebotsession_leases_with_work(bot_session, long_context, bots_service):
    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    response = bots_service.CreateBotSession(request, long_context)
    assert len(response.leases) == 0

    # Work queued on the server
    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service._instances[""]._scheduler, action_digest=action_digest,
                 action=action)

    bot_session = response  # the server has assigned a session name
    # UpdateBotSession should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    long_context.set_trailing_metadata.assert_called_once()

    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action


# Use only subset of the 'bots_interface' fixture combinations (only SQL, fewer timeouts)
@pytest.mark.parametrize('bots_interface', [('sql', None), ('sql', 1)], indirect=['bots_interface'])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_updatebotsession_permissive_mode_to_nonpermissive_server(
        bot_session, long_context, bots_service_permissive_botsession_1, bots_service):
    # Make the names used a bit shorter within this method
    bots_service_nonpermissive = bots_service
    bots_service_permissive_1 = bots_service_permissive_botsession_1

    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    response = bots_service_permissive_1.CreateBotSession(request, long_context)
    assert len(response.leases) == 0

    # Work queued on the server
    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service_permissive_1._instances[""]._scheduler, action_digest=action_digest,
                 action=action)

    bot_session = response  # the server has assigned a session name

    # UpdateBotSession with initial BotsInterface instance should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_permissive_1.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    bot_session = response  # the server has assigned a lease

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    # UpdateBotSession with different instance (with permissive BotSession mode disabled) — should fail
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_nonpermissive.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    long_context.set_code.assert_called_once_with(grpc.StatusCode.INVALID_ARGUMENT)
    assert len(response.leases) == 0
    response_action = remote_execution_pb2.Digest()


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_updatebotsession_permissive_mode_different_server(
        bot_session, long_context, bots_service_permissive_botsession_1,
        bots_service_permissive_botsession_2):
    # Make the names used a bit shorter within this method
    bots_service_permissive_1, bots_service_permissive_2 = bots_service_permissive_botsession_1, bots_service_permissive_botsession_2

    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    response = bots_service_permissive_1.CreateBotSession(request, long_context)
    assert len(response.leases) == 0

    # Work queued on the server
    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service_permissive_1._instances[""]._scheduler, action_digest=action_digest,
                 action=action)

    bot_session = response  # the server has assigned a session name

    # UpdateBotSession with initial BotsInterface instance should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_permissive_1.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    bot_session = response  # the server has assigned a lease

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    # UpdateBotSession with different instance (with permissive BotSession mode enabled) — should work
    # and not alter any of the initial leases
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_permissive_2.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action


# Use only subset of the 'bots_interface' fixture combinations (only SQL, fewer timeouts)
@pytest.mark.parametrize('bots_interface', [('sql', None), ('sql', 1)], indirect=['bots_interface'])
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_updatebotsession_nonpermissive_to_permissive_server(
        bot_session, long_context, bots_service_permissive_botsession_1, bots_service):
    # Make the names used a bit shorter within this method
    bots_service_nonpermissive = bots_service
    bots_service_permissive_1 = bots_service_permissive_botsession_1

    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    response = bots_service_nonpermissive.CreateBotSession(request, long_context)
    assert len(response.leases) == 0

    # Work queued on the server
    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service_nonpermissive._instances[""]._scheduler, action_digest=action_digest,
                 action=action)

    bot_session = response  # the server has assigned a session name

    # UpdateBotSession with initial BotsInterface instance should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_nonpermissive.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    bot_session = response  # the server has assigned a lease

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    # UpdateBotSession with different instance (with permissive BotSession mode enabled) — should work
    # and not alter any of the initial leases
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                               bot_session=bot_session)

    response = bots_service_permissive_1.UpdateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)
    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_lease_reassignment_after_botsession_close(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)

    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_interface._scheduler, action_digest=action_digest,
                 action=action)

    response = bots_service.CreateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    assert len(bots_interface._assigned_leases) == 1

    # Invoke `_close_bot_session`
    bots_interface._close_bot_session(response.name, reason='test')
    # Check whether the lease assignment has been cleaned up
    assert len(bots_interface._assigned_leases) == 0

    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)
    # Check whether a new botsession is able to pick up the re-queued lease
    response = bots_service.CreateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    assert len(bots_interface._assigned_leases) == 1


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_bot_disconnects_as_lease_is_being_assigned(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)

    # Bot connects
    response = bots_service.CreateBotSession(request, long_context)
    assert len(response.leases) == 0

    # Work is queued
    command_digest = remote_execution_pb2.Digest(hash='command-hash')
    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_interface._scheduler, action_digest=action_digest,
                 action=action)

    # Bot disconnects
    bots_interface._close_bot_session(response.name, reason='test')

    bot_session = response
    # Simultaneously, `UpdateBotSession` is waiting on leases
    # (patch checking functionality to simulate simultaneous events) and
    # waiting on leases from scheduler
    with mock.patch.object(bots_interface, '_check_bot_ids', autospec=True):
        with mock.patch.object(bots_interface, '_check_assigned_leases', autospec=True):
            with mock.patch.object(bots_interface, '_bot_ids', autospec=True):
                request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name,
                                                           bot_session=bot_session)
                # The BotSession no longer exists due to the `_close_bot_session`, this shouldn't
                # cause problems to buildgrid though, it should let it resume normally
                response = bots_service.UpdateBotSession(request, long_context)
                assert len(response.leases) == 0

    # Make sure the lease is now assigned to another bot!
    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot_session)
    # Check whether a new botsession is able to pick up the re-queued lease
    response = bots_service.CreateBotSession(request, long_context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    assert len(bots_interface._assigned_leases) == 1


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_post_bot_event_temp(context, bots_service):
    request = bots_pb2.PostBotEventTempRequest()
    bots_service.PostBotEventTemp(request, context)

    context.set_code.assert_called_once_with(grpc.StatusCode.UNIMPLEMENTED)


def test_unmet_platform_requirements(bot_session, long_context, bots_service):
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service._instances[""]._scheduler,
                 action_digest=action_digest,
                 platform_requirements={'OSFamily': set('wonderful-os')})

    response = bots_service.CreateBotSession(request, long_context)

    assert len(response.leases) == 0


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_unhealthy_bot(bot_session, long_context, bots_service):
    # set botstatus to unhealthy
    bot_session.status = BotStatus.UNHEALTHY.value
    request = bots_pb2.CreateBotSessionRequest(parent='',
                                               bot_session=bot_session)

    action_digest = remote_execution_pb2.Digest(hash='gaff')
    _inject_work(bots_service._instances[""]._scheduler, action_digest=action_digest)

    response = bots_service.CreateBotSession(request, long_context)

    # No leases should be given
    assert len(response.leases) == 0


def _create_bot_session(name, bots_service, context):
    bot = bots_pb2.BotSession()
    bot.status = BotStatus.OK.value
    bot.bot_id = name

    request = bots_pb2.CreateBotSessionRequest(parent='', bot_session=bot)
    new_bot_session = bots_service.CreateBotSession(request, context)

    return new_bot_session


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_state_reflected_in_metrics(update_bot_session_history,
                                                       context, bots_service_monitored):
    TRACKED_BOT_STATUSES = [botstatus for botstatus in BotStatus]

    n_bots, history_len = update_bot_session_history
    # Generate history of desired length
    bots_history = []
    for i in range(history_len):
        bot_statuses = [random.choice(TRACKED_BOT_STATUSES) for i in range(n_bots)]
        # Calculate # of bots in each status
        total_bots_by_status = Counter()
        total_bots_by_status.update(bot_statuses)
        bots_history.append((bot_statuses, total_bots_by_status))

    # Create all the bot sessions
    bot_sessions = []
    for i in range(n_bots):
        bot_id = f"bot_{i}"
        new_bot_session = _create_bot_session(bot_id, bots_service_monitored, context)
        print(f"bot id=[{bot_id}] has name=[{new_bot_session.name}]")
        bot_sessions.append(new_bot_session)

    process_order = [i for i in range(n_bots)]
    # Simulate UpdateBotSession calls from all bots and check metrics
    for iteration in range(history_len):
        print(f"Iteration: {iteration}")
        # Update all BotSessions (in random order for each iteraion)
        random.shuffle(process_order)  # Shuffle update order
        status_updates, total_bots_by_status = bots_history[iteration]
        for bot_index in process_order:
            current_bot_session = bot_sessions[bot_index]
            assert (f"bot_{bot_index}") == current_bot_session.bot_id
            # Update status
            current_bot_session.status = status_updates[bot_index].value
            request = bots_pb2.UpdateBotSessionRequest(name=current_bot_session.name,
                                                       bot_session=current_bot_session)
            update_bot_session_response = bots_service_monitored.UpdateBotSession(request, context)
            assert (f"bot_{bot_index}") == update_bot_session_response.bot_id

        print("Bot statuses:")
        print([bot.status for bot in bot_sessions])

        # Make sure the total number of bots matches expected number
        assert n_bots == bots_service_monitored.query_n_bots()

        # Make sure the numbers of the tracked states match up
        for status in TRACKED_BOT_STATUSES:
            # Print this so that it is visible in case of errors
            print(f"Asserting expected numbers for status: {status} (value={status.value})")
            assert total_bots_by_status[status] == bots_service_monitored.query_n_bots_for_status(status)

    # Simulate all bot sessions closing in a random order (e.g. due to expiry)
    process_order = [i for i in range(n_bots)]
    remaining_bots = n_bots
    # Start from the last iteration statuses and keep subtracting
    last_status_updates, last_total_bots_by_status = bots_history[-1]
    # We'll need to access the internal methods of BotsInterface for this
    bots_interface = bots_service_monitored._get_instance("")

    for bot_index in process_order:
        current_bot_session = bot_sessions[bot_index]
        assert (f"bot_{bot_index}") == current_bot_session.bot_id

        # Close bot session
        bots_interface._close_bot_session(current_bot_session.name, reason='test')

        # Remove this bot_session from the totals
        last_total_bots_by_status[last_status_updates[bot_index]] -= 1
        remaining_bots -= 1

        print("Bot statuses:")
        print([bot.status for bot in bot_sessions])

        # Make sure the total number of bots matches expected number
        assert remaining_bots == bots_service_monitored.query_n_bots()

        # Make sure the numbers of the tracked states match up
        for status in TRACKED_BOT_STATUSES:
            # Print this so that it is visible in case of errors
            print(f"Asserting expected numbers for status: {status} (value={status.value})")
            assert total_bots_by_status[status] == bots_service_monitored.query_n_bots_for_status(status)


@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_complete_already_completed_lease(bot_session, long_context, bots_service):
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1

    # Mark the job as completed in the data store
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value('COMPLETED')
    scheduler.update_job_lease_state(lease.id, lease)

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value('COMPLETED')
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # The lease should be removed by the server
    assert len(bot.leases) == 0


@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
@mock.patch('buildgrid.server.bots.instance.NETWORK_TIMEOUT', 1)
def test_update_bot_session_updating_already_completed_lease(bot_session, long_context, bots_service):
    _MonitoringBus._instance = mock.Mock()
    bots_interface = bots_service._get_instance("")
    scheduler = bots_interface._scheduler

    # Create BotSession and expect a lease to be assigned
    _inject_work(scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, long_context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    assert len(bots_interface._assigned_leases[bot.name]) == 1

    # Mark the job as completed in the data store
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value('COMPLETED')
    scheduler.update_job_lease_state(lease.id, lease)

    # Make the bot call UpdateBotSession with an ACTIVE lease
    # Expect the BotsInterface to remove the lease as
    # the bot completing an already completed lease isn't valid
    bot.leases[0].state = bots_pb2.LeaseState.Value('ACTIVE')
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name,
                                               bot_session=bot)
    bot = bots_service.UpdateBotSession(request, long_context)

    # The lease should be removed by the server
    assert len(bot.leases) == 0

    mock_timing_record = mock_create_timer_record(
        name=SCHEDULER_UPDATE_LEASE_TIME_METRIC_NAME,
        metadata={'instance-name': ''}
    )
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)
