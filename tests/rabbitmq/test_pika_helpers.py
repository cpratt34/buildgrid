# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from threading import Thread
from time import sleep
import unittest

import pytest

from buildgrid.server.rabbitmq.pika_consumer import PikaConsumer, RetryingPikaConsumer, QueueBinding
from buildgrid.server.rabbitmq.pika_publisher import PikaPublisher

from .utils.fixtures import connection_params, rabbitmq_server
from .utils.utils import check_rabbitmq_server_is_installed

try:
    import pika
except ImportError:
    print("Failed to import pika, RabbitMQ tests will be skipped.")


def test_queue_binding_constructor():
    q1 = QueueBinding(queue='queue1', exchange='exchangeA', routing_key='rk1')
    assert q1.queue == 'queue1'
    assert q1.exchange == 'exchangeA'
    assert q1.routing_key == 'rk1'
    assert q1.auto_delete_queue == False

    q1 = QueueBinding(queue='queue2', exchange='exchangeB', routing_key='rk2',
                      auto_delete_queue=True)
    assert q1.auto_delete_queue == True

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_send_and_receive(connection_params):
    exchange_name = 'test-xchg'
    queue_name = 'test-queue'

    exchanges = {exchange_name: pika.exchange_type.ExchangeType.topic}
    bindings = {QueueBinding(queue=queue_name, exchange='test-xchg',
                             routing_key=None)}

    # Start consumer:
    consumer = PikaConsumer(connection_params, exchanges=exchanges,
                            bindings=bindings)
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()

    # Register callback:
    received_messages = []
    def process_message_callback(body, delivery_tag):
        received_messages.append(body)
        consumer.ack_message(delivery_tag)

    consumer.subscribe(queue_name, process_message_callback)

    publisher = PikaPublisher(connection_params, exchanges=exchanges)

    # Publish a message:
    message_payload = b'hello, queue'
    publisher.send(exchange=exchange_name, routing_key=queue_name,
                   body=message_payload)
    sleep(2)
    # Assert that it reached the consumer:
    assert len(received_messages) == 1
    assert received_messages[0] == message_payload

    consumer.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_consumer_unsubscribe(connection_params):
    exchange_name = 'test-xchg'
    queue_name = 'test-queue'

    exchanges = {exchange_name: pika.exchange_type.ExchangeType.topic}
    bindings = {QueueBinding(queue=queue_name, exchange='test-xchg',
                             routing_key=None)}

    # Start consumer:
    consumer = PikaConsumer(connection_params, exchanges=exchanges,
                            bindings=bindings)
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()

    def unreachable_callback(_body, _delivery_tag):
        pytest.fail("`unreachable_callback()` should not have been invoked, "
                    "it was unsubscribed from consumer")
    # Register callback and unsubscribe it immediately after.
    # (It should never receive any messages.)
    consumer_tag = consumer.subscribe(queue_name, unreachable_callback)
    assert consumer_tag
    assert consumer.unsubscribe(consumer_tag, timeout_seconds=5)

    # Register a new callback instead:
    received_messages = []
    def process_message_callback(body, delivery_tag):
        received_messages.append(body)
        consumer.ack_message(delivery_tag)

    consumer.subscribe(queue_name, process_message_callback)

    publisher = PikaPublisher(connection_params, exchanges=exchanges)

    # Publish a message:
    message_payload = b'hello, queue'
    publisher.send(exchange=exchange_name, routing_key=queue_name,
                   body=message_payload)
    sleep(2)
    # Assert that it reached the right callback:
    assert len(received_messages) == 1
    assert received_messages[0] == message_payload

    consumer.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_consumer_exchange_is_declared_as_durable(connection_params):
    # We want to intercept pika's `exchange_declare()` but have it
    # execute and return a valid result.
    # Otherwise the call to `consumer.declare_exchange()` hangs forever.
    real_exchange_declare_method = pika.channel.Channel.exchange_declare

    exchange_declared = False
    def mock_exchange_declare(self, *args, **kwargs):
        assert kwargs['durable'] == True
        assert kwargs['exchange'] == 'test-xchg'
        assert kwargs['exchange_type'] ==  pika.exchange_type.ExchangeType.topic

        nonlocal exchange_declared
        exchange_declared = True

        return real_exchange_declare_method(self, *args, **kwargs)

    with unittest.mock.patch('pika.channel.Channel.exchange_declare',
                             new=mock_exchange_declare):

        consumer_is_up = False
        def on_consumer_up():
            nonlocal consumer_is_up
            consumer_is_up = True

        consumer = PikaConsumer(connection_parameters=connection_params,
                                exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                                bindings={QueueBinding(queue='queue', exchange='test-xchg',
                                                       routing_key=None)},
                                on_connection_established_callback=on_consumer_up,
                                on_connection_error_callback=on_consumer_up)

        consumer_thread = Thread(target=consumer.start)
        consumer_thread.start()

        while not consumer_is_up:
            continue

    assert exchange_declared
    consumer.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_publisher_exchange_is_declared_as_durable(connection_params):
    # We want to intercept pika's `exchange_declare()` but have it
    # execute and return a valid result.
    # Otherwise the call to `consumer.declare_exchange()` hangs forever.
    real_exchange_declare_method = pika.channel.Channel.exchange_declare

    exchange_declared = False
    def mock_exchange_declare(self, *args, **kwargs):
        assert kwargs['durable'] == True
        assert kwargs['exchange'] == 'test-xchg'
        assert kwargs['exchange_type'] ==  pika.exchange_type.ExchangeType.topic

        nonlocal exchange_declared
        exchange_declared = True

        return real_exchange_declare_method(self, *args, **kwargs)

    with unittest.mock.patch('pika.channel.Channel.exchange_declare',
                             new=mock_exchange_declare):

        PikaPublisher(connection_params,
                      exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic})
        assert exchange_declared

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_consumer_queue_is_declared_as_durable(connection_params):
    # We want to intercept pika's `queue_declare()` but have it
    # execute and return a valid result.
    # Otherwise the call to `consumer.declare_queue()` hangs forever.
    real_queue_declare_method = pika.channel.Channel.queue_declare

    queue_declared = False
    def mock_queue_declare(self, *args, **kwargs):
        assert kwargs['queue'] == 'test-queue'
        assert kwargs['durable'] == True
        assert kwargs['auto_delete'] == False


        nonlocal queue_declared
        queue_declared = True

        return real_queue_declare_method(self, *args, **kwargs)

    with unittest.mock.patch('pika.channel.Channel.queue_declare',
                             new=mock_queue_declare):
        consumer_is_up = False
        def on_consumer_up():
            nonlocal consumer_is_up
            consumer_is_up = True

        consumer = PikaConsumer(connection_parameters=connection_params,
                                exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                                bindings={QueueBinding(queue='test-queue', exchange='test-xchg',
                                                       routing_key=None)},
                                on_connection_established_callback=on_consumer_up,
                                on_connection_error_callback=on_consumer_up)

        consumer_thread = Thread(target=consumer.start)
        consumer_thread.start()

        while not consumer_is_up:
            continue

    assert queue_declared
    consumer.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_queue_is_declared_as_autodelete(connection_params):
    # We want to intercept pika's `queue_declare()` but have it
    # execute and return a valid result.
    # Otherwise the call to `consumer.declare_queue()` hangs forever.
    real_queue_declare_method = pika.channel.Channel.queue_declare

    queue_declared = False
    def mock_queue_declare(self, *args, **kwargs):
        assert kwargs['queue'] == 'test-queue'
        assert kwargs['auto_delete'] == True

        nonlocal queue_declared
        queue_declared = True

        return real_queue_declare_method(self, *args, **kwargs)

    with unittest.mock.patch('pika.channel.Channel.queue_declare',
                             new=mock_queue_declare):
        consumer_is_up = False
        def on_consumer_up():
            nonlocal consumer_is_up
            consumer_is_up = True

        consumer = PikaConsumer(connection_parameters=connection_params,
                                exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                                bindings={QueueBinding(queue='test-queue', exchange='test-xchg',
                                                       routing_key=None, auto_delete_queue=True)},
                                on_connection_established_callback=on_consumer_up,
                                on_connection_error_callback=on_consumer_up)

        consumer_thread = Thread(target=consumer.start)
        consumer_thread.start()

        while not consumer_is_up:
            continue

    assert queue_declared
    consumer.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_negative_qos_values_raise(connection_params):
    with pytest.raises(ValueError):
        PikaConsumer(connection_parameters=connection_params,
                     exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                     prefetch_size=-1,
                     bindings={('test-queue', 'test-xchg', None)})

    with pytest.raises(ValueError):
        PikaConsumer(connection_parameters=connection_params,
                     exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                     prefetch_count=-1,
                     bindings={('test-queue', 'test-xchg', None)})

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_send_and_receive_multiple_queues(connection_params):
    exchange_name = 'test-xchg'
    exchanges = {exchange_name: pika.exchange_type.ExchangeType.topic}
    consumer = PikaConsumer(connection_parameters=connection_params,
                            exchanges=exchanges,
                            bindings={QueueBinding(queue='test-queue-A', exchange='test-xchg',
                                                   routing_key=None),
                                      QueueBinding(queue='test-queue-B', exchange='test-xchg',
                                                   routing_key=None)})

    # Start consumer:
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()

    queue_a_received_messages = []
    def queue_a_callback(body, delivery_tag):
        queue_a_received_messages.append(body)
        consumer.ack_message(delivery_tag)

    consumer.subscribe('test-queue-A', queue_a_callback)

    queue_b_received_messages = []
    def queue_b_callback(body, delivery_tag):
        queue_b_received_messages.append(body)
        consumer.ack_message(delivery_tag)

    consumer.subscribe('test-queue-B', queue_b_callback)


    # Publish messages:
    publisher = PikaPublisher(connection_params, exchanges=exchanges)

    publisher.send(exchange=exchange_name,
                   routing_key='test-queue-A', body=b'Hello, queue A')
    publisher.send(exchange=exchange_name,
                   routing_key='test-queue-B', body=b'Hello, queue B')

    sleep(2)
    # Both make their way into their correct queues and callbacks:
    assert len(queue_a_received_messages) == 1
    assert queue_a_received_messages[0] == b'Hello, queue A'

    assert len(queue_b_received_messages) == 1
    assert queue_b_received_messages[0] == b'Hello, queue B'

    consumer.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_publisher_sends_mandatory(connection_params):
    message_published = False
    def mock_basic_publish(self, *args, **kwargs):
        assert kwargs['mandatory'] == True

        nonlocal message_published
        message_published = True

        # The exchange is not declared, so pika would throw this exception:
        raise pika.exceptions.ChannelClosedByBroker(404, 'Not Found')

    publisher = PikaPublisher(connection_params)
    with unittest.mock.patch("pika.channel.Channel.basic_publish", new=mock_basic_publish):
        # Nobody will ACK the message, so `send()` will throw:
        with pytest.raises(pika.exceptions.ChannelClosedByBroker):
            publisher.send(exchange='e', routing_key='q', body=b'Hi')

    assert message_published

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_send_and_receive_retrying_consumer(connection_params):

    queue_name = 'test-queue'
    exchange_name = 'test-xchg'
    exchange_type = pika.exchange_type.ExchangeType.topic

    exchanges = {exchange_name: exchange_type}
    bindings = [QueueBinding(queue=queue_name, exchange=exchange_name, routing_key=None)]

    consumer = RetryingPikaConsumer(connection_params,
                                    exchanges=exchanges,
                                    bindings=bindings,
                                    max_connection_attempts=10,
                                    retry_delay_base=1)
    received_messages = []
    def process_message_callback(body, delivery_tag):
        received_messages.append(body)
        consumer.ack_message(delivery_tag)

    consumer.subscribe(queue_name, process_message_callback)

    publisher = PikaPublisher(connection_params)

    # Publish a message:
    message_payload = b'hello, queue!'
    publisher.send(exchange=exchange_name, routing_key=queue_name,
                   body=message_payload)
    sleep(2)
    # Assert that it reached the consumer:
    assert len(received_messages) == 1
    assert received_messages[0] == message_payload

    consumer.stop()

def test_retrying_consumer_retries_on_failures():
    real_start_consumer_method = RetryingPikaConsumer._start_consumer

    number_of_connection_attempts = 0
    def fail_connection(self, *args, **kwargs):
        nonlocal number_of_connection_attempts
        number_of_connection_attempts += 1
        real_start_consumer_method(self, *args, **kwargs)

    retries_exceeded = False

    def last_connection_failed_callback():
        nonlocal retries_exceeded
        retries_exceeded = True

    with unittest.mock.patch("buildgrid.server.rabbitmq.pika_consumer.RetryingPikaConsumer._start_consumer",
                             new=fail_connection) as mocked_method:

        consumer = RetryingPikaConsumer(pika.ConnectionParameters(port=1234),
                                        exchanges={'test-xchg': pika.exchange_type.ExchangeType.topic},
                                        bindings={QueueBinding(queue='test-queue-A', exchange='test-xchg',
                                                               routing_key=None)},
                                        max_connection_attempts=3,
                                        retry_delay_base=1,
                                        on_connection_attempts_exceeded_callback=last_connection_failed_callback)

        while not consumer._stopped and not retries_exceeded:
            continue

    assert retries_exceeded
    assert number_of_connection_attempts == 3
