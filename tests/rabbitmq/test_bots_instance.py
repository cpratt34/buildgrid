# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import timedelta
from buildgrid._protos.google.devtools.remoteworkers.v1test2.worker_pb2 import Device, Worker
from threading import Thread
from time import sleep
from unittest import mock

from google.protobuf.any_pb2 import Any
import pytest
import uuid

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Digest
from buildgrid._protos.buildgrid.v2.messaging_pb2 import Job
from buildgrid._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession
from buildgrid._app.settings.rmq_parser import RabbitMqConnection
from buildgrid._enums import OperationStage, LeaseState
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    Action, Digest, ExecuteOperationMetadata)
from buildgrid._protos.buildgrid.v2.messaging_pb2 import UpdateOperations
from buildgrid._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession, Lease
from buildgrid.server.rabbitmq._enums import Exchanges
from buildgrid.server.rabbitmq.bots.instance import BotsInstance
from buildgrid.server.rabbitmq.pika_consumer import PikaConsumer, QueueBinding
from buildgrid.server.rabbitmq.pika_publisher import PikaPublisher
from buildgrid.utils import create_digest

from .utils.fixtures import connection_params, rabbitmq_server
from .utils.utils import check_rabbitmq_server_is_installed


@pytest.fixture(params=['', 'test-instance-name'])
def bots_instance(rabbitmq_server, request):
    conn = rabbitmq_server.dsn()
    bots_instance = BotsInstance(
        RabbitMqConnection('', address=conn['host'], port=conn['port'], virtual_host='/'),
        platform_queues={
            'OSFamily=linux': set(['OSFamily=linux'])
        }
    )
    bots_instance._instance_name = request.param
    yield bots_instance


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
@mock.patch('buildgrid.server.rabbitmq.bots.instance.NETWORK_TIMEOUT', 1)
def test_bots_instance_publishes_status_on_createbotsession(bots_instance, connection_params):
    exchange = Exchanges.BOT_STATUS.value
    exchanges = {exchange.name: exchange.type}
    bindings = {QueueBinding(queue='test-queue', exchange=exchange.name, routing_key='')}

    # Start consumer:
    consumer = PikaConsumer(connection_params, exchanges, bindings)
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()

    # Registering callback:
    received_messages = []

    def process_message_callback(body, ack_message_callback):
        received_messages.append(body)
        ack_message_callback()

    consumer.subscribe('test-queue', process_message_callback)

    bots_instance.start()
    bot_session = BotSession(
        bot_id='test-bot'
    )
    bots_instance.create_bot_session('', bot_session, 1)

    sleep(2)
    assert len(received_messages) == 1

    bots_instance.stop()
    consumer.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_bots_instance_publishes_status_on_updatebotsession(bots_instance, connection_params):
    exchange = Exchanges.BOT_STATUS.value
    exchanges = {exchange.name: exchange.type}
    bindings = {QueueBinding(queue='test-queue', exchange=exchange.name, routing_key='')}

    # Start consumer:
    consumer = PikaConsumer(connection_params, exchanges, bindings)
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()

    # Registering callback:
    received_messages = []

    def process_message_callback(body, ack_message_callback):
        received_messages.append(body)
        ack_message_callback()

    consumer.subscribe('test-queue', process_message_callback)

    bots_instance.start()
    bot_session = BotSession(
        bot_id='test-bot',
        name='/example-bot'
    )
    bots_instance.update_bot_session('', bot_session, 1)

    sleep(2)
    assert len(received_messages) == 1

    bots_instance.stop()
    consumer.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_bots_instance_cancellation_queue_caches_jobids(bots_instance, connection_params):
    bots_instance.start()

    # Publish one cancellation message and assert that the cache received it
    queue_name = f"cancellation-queue-{uuid.uuid4()}"
    publisher = PikaPublisher(connection_params)
    job_id = 'jobid-1234'
    message_payload = job_id.encode()
    publisher.send(exchange=Exchanges.JOB_CANCELLATION.value.name, routing_key=queue_name,
                   body=message_payload)

    sleep(2)
    assert bots_instance._cancellation_cache.size > 0
    assert bots_instance._cancellation_cache.get(job_id) is not None

    bots_instance.stop()


def _publish_test_job(connection_params):
    routing_key = 'OSFamily=linux'
    publisher = PikaPublisher(connection_params)
    exchange = Exchanges.JOBS.value
    action = Action(
        command_digest=Digest(hash="test-command", size_bytes=100),
        input_root_digest=Digest(hash="test-input-root", size_bytes=100),
        do_not_cache=False
    )
    prop = action.platform.properties.add()
    prop.name = 'OSFamily'
    prop.value = 'linux'

    job = Job(job_id='test-job', action=action)

    publisher.send(
        exchange=exchange.name,
        routing_key=routing_key,
        body=job.SerializeToString()
    )
    return job


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
@mock.patch('buildgrid.server.rabbitmq.bots.instance.NETWORK_TIMEOUT', 1)
def test_bots_instance_lease_assignment(bots_instance, connection_params):
    bots_instance.start()

    job = _publish_test_job(connection_params)

    bot_session = BotSession(
        bot_id='test-bot-id',
        worker=Worker(
            devices=[
                Device(
                    handle='test-device',
                    properties=[Device.Property(key='OSFamily', value='linux')]
                )
            ]
        )
    )

    bots_instance.create_bot_session('test', bot_session, 30)

    assert bot_session.name.startswith('test/')
    assert len(bot_session.leases) > 0

    lease = bot_session.leases[0]
    assigned_action = Action()
    lease.payload.Unpack(assigned_action)
    assert lease.id == job.job_id
    assert assigned_action == job.action

    bots_instance.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_bots_instance_lease_assignment_on_update(bots_instance, connection_params):
    bots_instance.start()

    job = _publish_test_job(connection_params)

    bot_session = BotSession(
        bot_id='test-bot-id',
        worker=Worker(
            devices=[
                Device(
                    handle='test-device',
                    properties=[Device.Property(key='OSFamily', value='linux')]
                )
            ]
        )
    )

    bots_instance.update_bot_session('test', bot_session, 30)

    assert len(bot_session.leases) > 0

    lease = bot_session.leases[0]
    assigned_action = Action()
    lease.payload.Unpack(assigned_action)
    assert lease.id == job.job_id
    assert assigned_action == job.action

    bots_instance.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
@mock.patch('buildgrid.server.rabbitmq.bots.instance.NETWORK_TIMEOUT', 1)
def test_assignment_request_expiry_method(bots_instance):
    bots_instance.start()

    bot_session = BotSession(
        bot_id='test-bot-id',
        worker=Worker(
            devices=[
                Device(
                    handle='test-device',
                    properties=[Device.Property(key='OSFamily', value='linux')]
                )
            ]
        )
    )

    bots_instance.create_bot_session('test', bot_session, 3)
    assignment = bots_instance._bot_name_to_assignment_request.get(bot_session.name)
    assert assignment is not None

    # Check that the assignment is expired thanks to its TTL
    assert not assignment._expired
    assert assignment._is_expired()

    # Unexpire the assignment by resetting the TTL to something big
    assignment._ttl = timedelta(seconds=1200)
    assert not assignment._is_expired()

    # Now check that we can explicitly set the expiry status by
    # resetting the TTL
    bots_instance.expire_assignment_for_bot_name(bot_session.name)
    assert assignment._is_expired()
    assert assignment._expired

    bots_instance.stop()


@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_bots_instance_publishes_operation_update_on_updatebotsession(bots_instance, connection_params):
    exchange = Exchanges.OPERATION_UPDATES.value
    exchanges = {exchange.name: exchange.type}

    if bots_instance._instance_name:
        routing_key = '*.*'  # '<stage>.<instance_name>'
    else:
        routing_key = '*'    # '<stage>'

    queue_name = 'operation-updates-queue'
    bindings = {QueueBinding(queue=queue_name, exchange=exchange.name,
                             routing_key=routing_key)}

    consumer = PikaConsumer(connection_params, exchanges=exchanges, bindings=bindings)

    received_messages = []
    def process_message_callback(body, ack_message_callback):
        received_messages.append(body)
        ack_message_callback()

    # Start consumer and subscribe callback:
    consumer_thread = Thread(target=consumer.start)
    consumer_thread.start()
    consumer.subscribe(queue_name, process_message_callback)

    bots_instance.start()

    action = Action(command_digest=Digest(hash='commandDigest', size_bytes=123))
    action_digest = create_digest(action.SerializeToString())

    lease = Lease(id='lease-id',
                  state=LeaseState.ACTIVE.value)
    lease.payload.Pack(action)

    bot_session = BotSession(
        bot_id='test-bot',
        name='/example-bot',
    )
    bot_session.leases.append(lease)

    bots_instance.update_bot_session('', bot_session, 10)

    sleep(2)
    assert len(received_messages) == 1
    bots_instance.stop()

    any_wrapper = Any()
    any_wrapper.ParseFromString(received_messages[0])
    assert any_wrapper.Is(UpdateOperations.DESCRIPTOR)

    # Parsing `UpdateOperations` message:
    update_operations_message = UpdateOperations()
    any_wrapper.Unpack(update_operations_message)
    assert update_operations_message.job_id

    # Checking that the update contains the expected status:
    operation = update_operations_message.operation_state
    operation_metadata = ExecuteOperationMetadata()
    operation.metadata.Unpack(operation_metadata)

    assert operation_metadata.stage == OperationStage.EXECUTING.value
    assert operation_metadata.action_digest == action_digest
