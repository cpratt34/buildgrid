# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from contextlib import contextmanager
import multiprocessing
import signal
import tempfile
import traceback

import pytest_cov

from buildgrid._app.settings import rmq_parser
from buildgrid.server.rabbitmq.server import RMQServer


@contextmanager
def serve(configuration, monitor=False):
    exc = None
    try:
        path = tempfile.NamedTemporaryFile().name
        server = TestServer(configuration, monitor, path)
        yield server, path
    except Exception as e:
        exc = e
    finally:
        try:
            server.quit()
        except UnboundLocalError:
            pass
        if exc is not None:
            raise exc


class TestServer:

    def __init__(self, configuration, monitor=False, endpoint_location=None):

        self.configuration = configuration

        self._queue = multiprocessing.Queue()
        self._process = multiprocessing.Process(
            target=TestServer.serve,
            args=(self._queue, self.configuration, monitor, endpoint_location))
        self._process.start()

        self.port = self._queue.get(timeout=10)
        if self.port == "ERROR":
            raise Exception("Error raised during server startup, check stderr")
        self.remote = f'localhost:{self.port}'

    @classmethod
    def serve(cls, queue, configuration, monitor, endpoint_location):
        pytest_cov.embed.cleanup_on_sigterm()

        try:
            server = RMQServer()

            def _signal_handler(signum, frame):
                server.stop()

            signal.signal(signal.SIGINT, signal.SIG_IGN)
            signal.signal(signal.SIGTERM, _signal_handler)

            instances = rmq_parser.get_parser().safe_load(configuration)['instances']
            for instance in instances:
                instance_name = instance['name']
                services = instance['services']
                for service in services:
                    service.register_instance_with_server(instance_name, server)

            server.add_port('localhost:0', None)

            def _retrieve_port(port_map):
                port = port_map[('localhost:0', None)]
                queue.put(port)

            server.start(port_assigned_callback=_retrieve_port)

        except:
            traceback.print_exc()
            queue.put("ERROR")

    def quit(self):
        if self._process:
            self._process.terminate()
            self._process.join()
