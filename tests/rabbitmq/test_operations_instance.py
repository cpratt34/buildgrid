# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import sleep

import pytest

from buildgrid._enums import ExchangeNames
from buildgrid._exceptions import InvalidArgumentError
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest, RequestMetadata
from buildgrid._protos.buildgrid.v2.messaging_pb2 import CreateOperation, UpdateOperations
from buildgrid._protos.google.longrunning.operations_pb2 import Operation
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.rabbitmq.operations.instance import OperationsInstance
from buildgrid.server.rabbitmq.pika_publisher import PikaPublisher

from .utils.fixtures import connection_params, rabbitmq_server
from .utils.utils import check_rabbitmq_server_is_installed


def create_datastore():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    return MemoryDataStore(storage)


def send_message(message, routing_key, server):
    publisher = PikaPublisher(server)

    publisher.send(exchange=ExchangeNames.OPERATION_UPDATES.value,
                   routing_key=routing_key,
                   body=message)

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
@pytest.mark.parametrize("instance_name", ['', 'test-instance-name'])
def test_operation_update(connection_params, instance_name):
    operations_instance = OperationsInstance(instance_name=instance_name,
                                             rabbitmq_connection_parameters=connection_params,
                                             operations_datastore=create_datastore())

    assert operations_instance.instance_name == instance_name

    operation_name = 'operation0'
    operation = Operation(name=operation_name)
    update_operations = UpdateOperations(job_id=operation_name,
                                         operation_state=operation,
                                         cacheable=True)

    # Send update message:
    update_message = update_operations.SerializeToString()

    send_message(update_message,
                 f'EXECUTING.{instance_name}',
                 connection_params)

    # Call `GetOperation()` to get it back:
    returned_operation, returned_metadata = operations_instance.get_operation(operation_name)
    assert returned_operation == operation
    assert not returned_metadata

    operations_instance.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
@pytest.mark.parametrize("instance_name", ['', 'test-instance-name'])
def test_operation_create(connection_params, instance_name):
    operations_instance = OperationsInstance(instance_name=instance_name,
                                             rabbitmq_connection_parameters=connection_params,
                                             operations_datastore=create_datastore())
    # Create operation request containing request metadata:
    operation_name = 'operation0'

    request_metadata = RequestMetadata()
    request_metadata.tool_details.tool_name = 'bgd-tests'
    request_metadata.tool_details.tool_version = '0.1'
    request_metadata.tool_invocation_id = 'invocation0'

    action_digest = Digest(hash='action-hash', size_bytes=1234)

    create_operation = CreateOperation(job_id=operation_name,
                                       operation_name=operation_name,
                                       action_digest=action_digest,
                                       request_metadata=request_metadata,
                                       cacheable=True)
    send_message(create_operation.SerializeToString(),
                 f'QUEUED.{instance_name}',
                 connection_params)

    # Update message with the Operation itself:
    operation = Operation(name=operation_name)
    update_operations = UpdateOperations(job_id=operation_name,
                                         operation_state=operation,
                                         cacheable=True)
    send_message(update_operations.SerializeToString(),
                 f'EXECUTING.{instance_name}',
                 connection_params)

    # Call `GetOperation()` to get it and the request metadata:
    returned_operation, returned_metadata = operations_instance.get_operation(operation_name)
    assert returned_operation == operation
    assert returned_metadata == request_metadata

    operations_instance.stop()

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_get_non_existing_operation(connection_params):
    operations_instance = OperationsInstance(instance_name='test-instance',
                                             rabbitmq_connection_parameters=connection_params,
                                             operations_datastore=create_datastore())

    with pytest.raises(InvalidArgumentError):
        operations_instance.get_operation('operation-that-was-never-seen')

    operations_instance.stop()
