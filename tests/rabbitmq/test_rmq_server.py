# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import time
import traceback

import grpc
import pytest

from buildgrid._enums import BotStatus
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2_grpc
from buildgrid._protos.buildstream.v2 import buildstream_pb2
from buildgrid._protos.buildstream.v2 import buildstream_pb2_grpc
from buildgrid._protos.google.bytestream import bytestream_pb2
from buildgrid._protos.google.bytestream import bytestream_pb2_grpc
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2_grpc
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid._protos.google.longrunning import operations_pb2_grpc

from tests.utils.utils import run_in_subprocess
from .utils.fixtures import rabbitmq_server
from .utils.server import serve
from .utils.utils import check_rabbitmq_server_is_installed


@pytest.fixture
def configuration(rabbitmq_server):
    conn = rabbitmq_server.dsn()
    return f"""
        description: BuildGrid RabbitMQ-powered Bots service

        server:
            bind:
                !channel
                port: 50055
                insecure-mode: true
            rabbitmq:
                !rabbitmq-connection &rmq
                address: {conn['host']}
                port: {conn['port']}
                virtual-host: "/"

        instances:
          - name: main
            services:
              - !bots-service
                rabbitmq: *rmq
                platform-queues-file: data/config/rabbitmq/platform-queues.yml

        """

@check_rabbitmq_server_is_installed
@pytest.mark.xdist_group("rabbitmq")
def test_create_server(configuration):
    # We need to explicitly set an event loop here, since pytest-aiohttp
    # seems to close the default/implicit loop we rely on in our asyncio
    # code. Without explicitly creating a new loop, this leads to all tests
    # that touch asyncio-related code and run in the same test process as
    # the bgd-browser backend tests failing due to the lack of a running
    # event loop in the current thread.
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    # Actual test function, to be run in a subprocess:
    def _test_create_server(remote):
        # Open a channel to the remote server:
        channel = grpc.insecure_channel(remote)

        try:
            session = bots_pb2.BotSession(leases=[],
                                          bot_id="test-bot",
                                          status=BotStatus.OK.value)
            stub = bots_pb2_grpc.BotsStub(channel)
            request = bots_pb2.CreateBotSessionRequest(parent='main', bot_session=session)
            response = stub.CreateBotSession(request, timeout=1)

            assert response.DESCRIPTOR is bots_pb2.BotSession.DESCRIPTOR
            assert response.name.startswith('main/')

            return True

        except grpc.RpcError as e:
            traceback.print_exc()
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        return False

    with serve(configuration) as (server, _):
        assert run_in_subprocess(_test_create_server, server.remote)
