# Stress testing of BuildGrid

## How to run

Use docker-compose and optionally set the environment variables
to the config options you want to test.

The following invocation includes all the possible options, as an example,
all of which are optional other than the basic
`docker-compose -f my-docker-compose.yml up`

```bash
STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES=yes \
STRESS_TESTING_BGD_CONFIG=bgd-inmem.yml \
STRESS_TESTING_CLIENT_REQUESTS=6 \
 docker-compose -f pges-recc-bbrht.yml up \
  --scale buildgrid=2 --scale bots=4 \
  --scale clients=5 --scale database=3 -d --build
```

## Additional Options and Supported Features
* `STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES` — when this environment variable is set,
 workers and clients will NOT use any platform properties. The default is to generate
 a few random OS/ISA combinations (see `mnt/scripts` for more info).
* `STRESS_TESTING_BGD_CONFIG` — the BuildGrid config file to use from `mnt/scripts`.
* `STRESS_TESTING_CLIENT_REQUESTS` — the number of sequential requests the recc client
 will do.
* `STRESS_TESTING_GRACE_PERIOD` — Each dependee script will sleep for this long after
its dependent container finishes. This can be useful if you notice that services don't
quite finish initializing before their containers are put online,


### Checking status
```bash
export STDC=pges-recc-bbrht.yml

# To see the status of all the containers
docker-compose -f $STDC ps

# To see the status of all the containers that have NOT exited 0
docker-compose -f $STDC ps | grep -v "Exit 0"
# ... Or watch using the provided script
watch -n 0.5 -d ./ps-non0.sh
```

## Pro Tips

### Generating the command
The included shell wizard script can generate the invocation script for you.
Just run: `bash docker-compose-up-gen.sh`, verify the command is what you need
and execute it.

It will also create a file called `start-docker-compose.sh` in your current
working directory with this command.

### Adjusting the database configurations
Bumping the number of allowed connections in the database can be helpful if you
notice timeouts or connections being dropped.

If you want to change the max number of connections for the postgres container, you can
edit `pges-recc-bbrht.yml` and edit the following field:

```
command: postgres -c "max_connections=300"
```

replacing 300 with your desired number.

## Quick scripts
There are several scripts included here to save you some keystrokes.

`docker-compose-up-gen.sh`: A wizard that can create the stress-testing command for you.
`ps.sh`: Shows the ps output of all processes in the stress-testing setup.
`ps-non0.sh`: Like the above, but only processes that existed non-zero.
`stop-docker-compose.sh`: Bring the stress-testing setup down.
`print-logs.sh`: Print all logs.

## Troubleshooting

### "Could not get action from storage?"
If the clients fail to get the action from storage, that probably means you're
running your setup with indexed CAS, and indexed CAS thinks it has some blobs
it doesn't. Run `docker-compose -f pges-recc-bbrht.yml down` to
bring the setup down and try again.

### "Name resolution failure?"
After doing a docker-compose down, you may occasionally encounter name resolution
failures. If this happens, restart your docker daemon.
econds

### "Compose File Use Cases"
## "Cleanup"
To test bgd cleanup, run the following wizard. When asked to choose compose files,
select "docker-compose-cleanup.yml". Then specify the
configurations it asks for (use the defaults if you are unsure).

```
./docker-compose-up-gen.sh
```

The printed docker-compose command will bring up "upload" services
which upload unique files to an S3 CAS (which is also brought up).
It will also bring up "bgd cleanup" daemons which will clean the CAS and Index.
Then a test script will run, for a specified number of seconds, making sure that
the CAS size falls within the user specified ranges (within an error of two batch sizes).
The docker compose setup will only exit zero if the CAS size stays within those ranges.
