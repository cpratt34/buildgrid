#!/bin/bash

filled_up_high=
test_end=$((SECONDS+$TEST_LENGTH))
casupload=/buildbox-e2e-build/buildbox-tools/cpp/build/casupload/casupload
rm -rf /minio/buildgrid-bucket/*

# Setup Minio Client (mc) to query uploaded size.
apt-get -y -qq install wget jq; wget --quiet https://dl.min.io/client/mc/release/linux-amd64/mc; chmod +x mc; mv mc /usr/bin
mc alias set s3 http://minio:9000  minioadmin minioadmin

upload_total=0
while true;
do
    declare -i num=$(mc du s3 --json | jq '.size')

    if [[ -z $NOT_RUN_IN_STAGES ]] && [[ $num -gt $HIGH_WATERMARK ]]
    then
        filled_up_high=true
    fi

    if [[ -z $filled_up_high ]]
    then
        # Generate a random file
        file_name=$(mktemp XXXXXXX)
        file_size=$(shuf -i 0-$MAX_FILE_SIZE -n 1)
        head -c $file_size </dev/urandom >> $file_name

        # Upload to S3
        $casupload --cas-server=$RECC_SERVER $file_name > /dev/null 2>&1
        upload_total=$((upload_total + file_size))
        
        echo Uploaded file_name=$file_name, file_size=$file_size, upload_total=$upload_total, minio_reported=$(mc du s3 --json | jq '.size')
    fi
done
