#!/bin/bash

filled_up_low=
filled_up_high=
LOWER_BOUND=$(expr $LOW_WATERMARK \- $BATCH_SIZE \* 2)
UPPER_BOUND=$(expr $HIGH_WATERMARK \+ $BATCH_SIZE \* 2)

# Setup Minio Client (mc) to query uploaded size.
apt-get -y -qq install wget jq; wget --quiet https://dl.min.io/client/mc/release/linux-amd64/mc; chmod +x mc; mv mc /usr/bin
mc alias set s3 http://minio:9000  minioadmin minioadmin

for (( i=1; i<=$TEST_LENGTH; i++ ));
do
    sleep 1
    declare -i num=$(mc du s3 --json | jq '.size')

    # Prevents extraneous printing until buildgrid is ready
    if [[ $num -gt 80 ]]
    then
        echo CAS size now: $num
    fi

    if [[ $num -ge $LOW_WATERMARK ]]
    then
        filled_up_low=true
    fi

    if [[ $num -ge $HIGH_WATERMARK ]]
    then
        filled_up_high=true
    fi

    if [[ -n $NOT_RUN_IN_STAGES ]] && [[ $filled_up_low ]] && [[ $num -ge $UPPER_BOUND || $num -le $LOWER_BOUND ]]
    then
        echo "CAS size no longer within specified range: $LOWER_BOUND - $UPPER_BOUND."
        exit 1
    fi
done

if [[ -z $NOT_RUN_IN_STAGES ]]
then
    if [[ -z $filled_up_high ]]
    then
        echo "Warning: CAS was not filled up to high watermark: $HIGH_WATERMARK"
    fi

    if [[ $num -lt $LOWER_BOUND ]] || [[ $num -gt $LOW_WATERMARK ]]
    then
        echo "Cleanup under or over deleted. Cas size should remain between $LOWER_BOUND and $LOW_WATERMARK."
    fi
fi

if [[ -n $NOT_RUN_IN_STAGES ]] && [[ $num -le $LOWER_BOUND ]]
then
    echo "CAS size remained less than lower bound: $LOWER_BOUND."
    exit 1
fi

echo Test Passed
