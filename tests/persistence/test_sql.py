# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


from datetime import datetime, timedelta
from typing import Dict, List
from google.protobuf.duration_pb2 import Duration
from grpc import StatusCode
from time import sleep
import tempfile
import hashlib
import json

import pytest
import testing.postgresql
from unittest import mock

from buildgrid._enums import LeaseState, MetricCategories, OperationStage
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.rpc import status_pb2, code_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.job import Job
from buildgrid.server.metrics_names import (
    DATA_STORE_CHECK_FOR_UPDATE_TIME_METRIC_NAME,
    DATA_STORE_CREATE_JOB_TIME_METRIC_NAME,
    DATA_STORE_CREATE_LEASE_TIME_METRIC_NAME,
    DATA_STORE_CREATE_OPERATION_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_DIGEST_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_NAME_TIME_METRIC_NAME,
    DATA_STORE_GET_JOB_BY_OPERATION_TIME_METRIC_NAME,
    DATA_STORE_LIST_OPERATIONS_TIME_METRIC_NAME,
    DATA_STORE_QUEUE_JOB_TIME_METRIC_NAME,
    DATA_STORE_STORE_RESPONSE_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_JOB_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_LEASE_TIME_METRIC_NAME,
    DATA_STORE_UPDATE_OPERATION_TIME_METRIC_NAME
)
from buildgrid.server.monitoring import _MonitoringBus
from buildgrid.server.operations.filtering import FilterParser, DEFAULT_OPERATION_FILTERS
from buildgrid.server.persistence.sql import models
from buildgrid.server.persistence.sql.impl import (SQLDataStore, PruningOptions)
from buildgrid.server.persistence.sql.utils import DATETIME_FORMAT
from buildgrid.settings import MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES
from buildgrid.utils import hash_from_dict, JobState
from buildgrid._exceptions import DatabaseError, InvalidArgumentError, RetriableDatabaseError
from buildgrid._protos.google.rpc import status_pb2
from tests.utils.metrics import mock_create_timer_record

from google.protobuf import any_pb2


try:
    Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True)
except RuntimeError:
    print(f"skipping all POSTGRES tests")
    POSTGRES_INSTALLED = False
else:
    POSTGRES_INSTALLED = True

postgres_skip_message = "Skipped test due to Postgres not being installed"
check_for_postgres = pytest.mark.skipif(not POSTGRES_INSTALLED, reason=postgres_skip_message)

TEST_JOB_PRUNING_PERIOD = timedelta(seconds=1)


def generate_dummy_action(do_not_cache=False):
    action_proto = remote_execution_pb2.Action()
    action_proto.command_digest.hash = 'commandHash'
    action_proto.command_digest.size_bytes = 123
    action_proto.do_not_cache = do_not_cache
    return action_proto


def populated_operation_request_metadata_fields():
    """ These are `Operation()` kwargs related to information that a
    client attaches in a RequestMetadata message.
    """
    return {'tool_name': 'bgd',
            'tool_version': 'v1.2.3',
            'invocation_id': 'inv-id-0',
            'correlated_invocations_id': 'corr-inv-id-1'}


@pytest.fixture(params=['sqlite', 'postgresql'])
def database(request):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    data_store = None
    try:
        if request.param == 'sqlite':
            with tempfile.NamedTemporaryFile() as db:
                data_store = SQLDataStore(
                    storage,
                    connection_string=f"sqlite:///{db.name}",
                    automigrate=True
                )
                data_store.set_instance_name('test_instance')
                data_store.set_action_browser_url('https://localhost/')
                data_store.start()
                yield data_store
        elif request.param == 'postgresql':
            with Postgresql() as postgresql:
                data_store = SQLDataStore(
                    storage,
                    connection_string=postgresql.url(),
                    automigrate=True
                )
                data_store.set_instance_name('test_instance')
                data_store.set_action_browser_url('https://localhost/')
                data_store.start()
                yield data_store

    finally:
        if data_store is not None:
            data_store.stop()


@pytest.fixture()
def database_pair():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    try:
        db = tempfile.NamedTemporaryFile().name
        data_store_1 = SQLDataStore(storage,
                                    connection_string=f"sqlite:///{db}",
                                    automigrate=True)
        data_store_2 = SQLDataStore(storage,
                                    connection_string=f"sqlite:///{db}",
                                    automigrate=False)
        data_store_1.start()
        data_store_2.start()
        yield data_store_1, data_store_2

    finally:
        data_store_1.stop()
        data_store_2.stop()


@pytest.fixture()
def database_pruning_enabled():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    data_store = None
    try:
        pruning_options = PruningOptions(pruner_job_max_age=timedelta(seconds=1),
                                         pruner_period=TEST_JOB_PRUNING_PERIOD, pruner_max_delete_window=1000)
        with Postgresql() as postgresql:
            data_store = SQLDataStore(
                storage,
                connection_string=postgresql.url(),
                automigrate=True,
                connection_timeout=5,
                poll_interval=1,
                pruning_options=pruning_options
            )
            data_store.set_instance_name('test_instance')
            data_store.set_action_browser_url('https://localhost/')
            data_store.start()
            yield data_store

    finally:
        if data_store is not None:
            data_store.stop()
            data_store.pruner_keep_running = False


def add_test_job(job_name, database):
    with database.session(reraise=True) as session:
        session.add(models.Job(
            name=job_name,
            action=generate_dummy_action().SerializeToString(),
            action_digest="test-action-digest/144",
            priority=10,
            stage=OperationStage.CACHE_CHECK.value
        ))


def add_test_blob(digest, data, storage):
    write_session = storage.begin_write(digest)
    write_session.write(data)
    storage.commit_write(digest, write_session)


def add_test_execute_response(database):
    test_execute_response = remote_execution_pb2.ExecuteResponse(result=remote_execution_pb2.ActionResult(exit_code=12))
    test_execute_response_digest = remote_execution_pb2.Digest(hash="finished-action-execute-response", size_bytes=123)
    add_test_blob(test_execute_response_digest, test_execute_response.SerializeToString(), database.storage)
    return test_execute_response_digest

def populate_database(database):
    test_execute_response_digest = add_test_execute_response(database)

    with database.session(reraise=True) as session:
        session.add_all([
            models.Job(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name="test-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            ),
            models.Job(
                name="other-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="other-action/10",
                priority=5,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="other-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="linux-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="linux-action/10",
                priority=7,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="linux-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="extra-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="extra-action/50",
                priority=20,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="extra-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"], "generic": ["requirement"]})
            ),
            models.Job(
                name="cancelled-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="cancelled-action/35",
                priority=20,
                stage=OperationStage.COMPLETED.value,
                cancelled=True,
                queued_timestamp=datetime(2019, 6, 1),
                queued_time_duration=60,
                worker_start_timestamp=datetime(2019, 6, 1, minute=1),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.CANCELLED.value
                )],
                operations=[
                    models.Operation(
                        name="cancelled-operation",
                        cancelled=True,
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="finished-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="finished-action/35",
                result=f"{test_execute_response_digest.hash}/{test_execute_response_digest.size_bytes}",
                priority=20,
                stage=OperationStage.COMPLETED.value,
                queued_timestamp=datetime(2019, 6, 1),
                queued_time_duration=10,
                worker_start_timestamp=datetime(2019, 6, 1, second=10),
                worker_completed_timestamp=datetime(2019, 6, 1, minute=1),
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.COMPLETED.value
                )],
                operations=[
                    models.Operation(
                        name="finished-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            ),
            models.Job(
                name="platform-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="platform-action/10",
                priority=5,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.PENDING.value
                )],
                operations=[
                    models.Operation(
                        name="platform-operation",
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict(
                    {"OSFamily": ["aix"], "generic": ["requirement", "requirement2"]}),
            )
        ])


def completed_job(prefix, index, timestamp):
    return models.Job(
        name=f"{prefix}-job/{index}",
        action=generate_dummy_action().SerializeToString(),
        action_digest=f"{prefix}-action/{index}",
        priority=20,
        stage=OperationStage.COMPLETED.value,
        queued_timestamp=timestamp - timedelta(seconds=10),
        queued_time_duration=10,
        worker_start_timestamp=timestamp,
        worker_completed_timestamp=timestamp + timedelta(seconds=10),
        leases=[models.Lease(
            status=0,
            state=LeaseState.COMPLETED.value
        )],
        operations=[
            models.Operation(
                name=f"{prefix}-operation/{index}"
            )
        ],
        platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
    )


@check_for_postgres
@pytest.mark.parametrize("conn_str", ["sqlite:///file:memdb1?option=value&cache=shared&mode=memory",
                                      "sqlite:///file:memdb1?mode=memory&cache=shared",
                                      "sqlite:///file:memdb1?cache=shared&mode=memory",
                                      "sqlite:///file::memory:?cache=shared",
                                      "sqlite:///file::memory:",
                                      "sqlite:///:memory:",
                                      "sqlite:///",
                                      "sqlite://"])
def test_is_sqlite_inmemory_connection_string(conn_str):
    with pytest.raises(ValueError):
        # Should raise ValueError when trying to instantiate
        database = SQLDataStore(None, connection_string=conn_str)


@check_for_postgres
@pytest.mark.parametrize("conn_str", ["sqlite:///../../myfile.db",
                                      "sqlite:///./myfile.db",
                                      "sqlite:////myfile.db"])
def test_file_based_sqlite_db(conn_str):
    # Those should be OK and not raise anything during instantiation
    with mock.patch('buildgrid.server.persistence.sql.impl.create_engine') as create_engine:
        database = SQLDataStore(None, connection_string=conn_str)
        database.watcher_keep_running = False
        assert create_engine.call_count == 1
        call_args, call_kwargs = create_engine.call_args


@check_for_postgres
def test_rollback(database):
    job_name = "test-job"
    add_test_job(job_name, database)
    try:
        with database.session(reraise=True) as session:
            job = session.query(models.Job).filter_by(name=job_name).first()
            assert job is not None
            job.name = "other-job"
            raise Exception("Forced exception")
    except Exception:
        pass

    with database.session(reraise=True) as session:
        # This query will only return a result if the rollback was successful and
        # the job name wasn't changed
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None


def _get_disposal_exceptions(db):
    dialect = db.engine.dialect.name
    assert dialect != ''

    exceptions = db._sql_pool_dispose_helper._dispose_pool_on_exceptions
    if dialect == 'postgresql':
        assert len(exceptions) > 0

    return exceptions or ()


@check_for_postgres
@pytest.mark.parametrize('reraise', [True, False])
def test_pool_disposal_exceptions(database, reraise):
    disposal_exceptions = _get_disposal_exceptions(database)
    database.engine.dispose = mock.MagicMock()
    database._sql_pool_dispose_helper._cooldown_time_in_secs = 1

    # Throw some random exceptions and ensure we don't dispose the pool
    for e in (ValueError, KeyError):
        try:
            with database.session(reraise=reraise) as session:
                raise Exception("Forced exception") from e()
        except Exception:
            assert reraise
        else:
            assert not reraise
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        database.engine.dispose.assert_not_called()

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        try:
            with database.session(reraise=reraise) as session:
                raise Exception("Forced exception") from e()
        except Exception as e:
            assert reraise
        else:
            assert not reraise
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        database.engine.dispose.assert_called_once()

    # Test retriable
    retry_delay = timedelta(seconds=10)
    retry_message = "Database connection was temporarily interrupted, please retry"
    for e in disposal_exceptions:
        try:
            with database.session(reraise=reraise) as session:
                raise RetriableDatabaseError(retry_message, retry_delay)
        except Exception as e:
            assert reraise
            assert e.retry_info is not None
            d = Duration()
            d.FromTimedelta(retry_delay)
            assert e.retry_info.retry_delay == d

            assert e.error_status.code == StatusCode.UNAVAILABLE
            assert e.error_status.details == retry_message

            assert e.error_status.trailing_metadata is not None

            # Construct the expected Status message
            expected_metadata = status_pb2.Status(code=14, message=retry_message)
            error_detail = expected_metadata.details.add()
            error_detail.Pack(e.retry_info)
            expected_metadata_str = expected_metadata.SerializeToString()
            found_metadata = False
            # Go through and find all the 'grpc-status-details-bin' metadata keys and
            # verify the Status message is one of them and has the expected contents
            for trailing_metadata in e.error_status.trailing_metadata:
                if trailing_metadata[0] == 'grpc-status-details-bin' and trailing_metadata[1] == expected_metadata_str:
                    found_metadata = True
            if not found_metadata:
                pytest.fail("Status message was not sent as part of trailing_metadata")
        else:
            assert not reraise
        database.engine.dispose.assert_called_once()


@check_for_postgres
@pytest.mark.parametrize('reraise', [True, False])
def test_pool_disposal_limit(database, reraise):
    disposal_exceptions = _get_disposal_exceptions(database)
    database.engine.dispose = mock.MagicMock()
    database._sql_pool_dispose_helper._cooldown_time_in_secs = 1

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        try:
            with database.session(reraise=reraise) as session:
                raise Exception("Forced exception") from e()
        except Exception:
            assert reraise
        else:
            assert not reraise
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        database.engine.dispose.assert_called_once()

    # Now manually override the last disposal time and see if that triggers another disposal (once more)
    timestamp_disposed_a_while_back = datetime.utcnow() - 2 * timedelta(minutes=MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES)
    database._sql_pool_dispose_helper._last_pool_dispose_time = timestamp_disposed_a_while_back

    # Throw the exceptions we want to dispose the pool on
    # Ensure we dispose the pool again, but only once more
    for e in disposal_exceptions:
        try:
            with database.session(reraise=reraise) as session:
                raise Exception("Forced exception") from e()
        except Exception:
            assert reraise
        else:
            assert not reraise
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        assert database.engine.dispose.call_count == 2


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_get_job_by_action(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)
    job = database.get_job_by_action(models.string_to_digest("notarealjob/123"))
    assert job is None

    # Ensure that get_job_by_action doesn't get completed jobs.
    # Actions aren't unique in the job history, so we only care
    # about the one that is currently incomplete (if any).
    job = database.get_job_by_action(models.string_to_digest("finished-action/35"))
    assert job is None

    job = database.get_job_by_action(models.string_to_digest("extra-action/50"))
    digest = job.action_digest.hash
    size = job.action_digest.size_bytes
    assert job.name == "extra-job"
    assert job.priority == 20
    assert job.execute_response.message == f'https://localhost/action/test_instance/{digest}/{size}/'

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_GET_JOB_BY_DIGEST_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_get_job_by_name(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)
    job = database.get_job_by_name("notarealjob")
    assert job is None

    job = database.get_job_by_name("extra-job")
    digest = job.action_digest.hash
    size = job.action_digest.size_bytes
    assert job.name == "extra-job"
    assert job.priority == 20
    assert job.execute_response.message == f'https://localhost/action/test_instance/{digest}/{size}/'

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_GET_JOB_BY_NAME_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_get_operation_request_metadata_by_name(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)

    # Non-existing operation:
    assert database.get_operation_request_metadata_by_name("notarealoperation") is None

    # Existing operation:
    metadata = database.get_operation_request_metadata_by_name("extra-operation")
    assert metadata is not None

    expected_keys = {'tool-name', 'tool-version', 'invocation-id', 'correlated-invocations-id'}
    assert expected_keys <= metadata.keys()

    # We get back the values that were pre-populated by this method:
    expected_values = populated_operation_request_metadata_fields()
    assert metadata['tool-name'] == expected_values['tool_name']
    assert metadata['tool-version'] == expected_values['tool_version']
    assert metadata['invocation-id'] == expected_values['invocation_id']
    assert metadata['correlated-invocations-id'] == expected_values['correlated_invocations_id']


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_get_job_by_operation(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)
    job = database.get_job_by_operation("notarealjob")
    assert job is None

    job = database.get_job_by_operation("extra-operation")
    digest = job.action_digest.hash
    size = job.action_digest.size_bytes
    assert job.name == "extra-job"
    assert job.priority == 20
    assert job.execute_response.message == f'https://localhost/action/test_instance/{digest}/{size}/'

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_GET_JOB_BY_OPERATION_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_get_all_jobs(database):
    populate_database(database)
    jobs = database.get_all_jobs()
    assert len(jobs) == 5
    for job in jobs:
        digest = job.action_digest.hash
        size = job.action_digest.size_bytes
        assert job.execute_response.message == f'https://localhost/action/test_instance/{digest}/{size}/'


def test_hash_from_dict():
    config = {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    assert hash_from_dict(config) == '2844e4e6d221f4205cdb70c344f51db79dc1bd80'


def compare_lists_of_dicts(list1, list2):
    for dictionary in list1:
        assert dictionary in list2
    for dictionary in list2:
        assert dictionary in list1


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_create_job(database):
    _MonitoringBus._instance = mock.Mock()
    job_name = "test-job"
    action = generate_dummy_action(do_not_cache=False)

    job = Job(do_not_cache=False,
              action=action,
              action_digest=models.string_to_digest("test-action-digest/144"),
              priority=10,
              name=job_name)
    database.create_job(job)

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None
        assert job.priority == 10
        assert job.action_digest == "test-action-digest/144"

        assert job.action is not None

        assert not job.do_not_cache
        assert job.queued_timestamp is None
        assert job.worker_start_timestamp is None
        assert job.worker_completed_timestamp is None

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_CREATE_JOB_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_update_job(database):
    _MonitoringBus._instance = mock.Mock()
    job_name = "test-job"
    add_test_job(job_name, database)

    database.update_job(job_name, {"priority": 1})

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None
        assert job.priority == 1

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_UPDATE_JOB_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_delete_job(database):
    populate_database(database)
    job = database.get_job_by_name("test-job")
    database.store_response(job)
    assert "test-job" in database.response_cache

    database.delete_job("test-job")
    assert "test-job" not in database.response_cache


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_store_response(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)
    job = database.get_job_by_name("test-job")
    database.store_response(job)

    updated = database.get_job_by_name("test-job")
    assert updated.execute_response is not None
    assert "test-job" in database.response_cache
    assert database.response_cache["test-job"] is not None

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_STORE_RESPONSE_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_get_operations_by_stage(database):
    populate_database(database)
    operations = database.get_operations_by_stage(OperationStage(4))
    assert len(operations) == 2


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_list_operations(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)
    operations, next_page_token = database.list_operations()
    # Only incomplete operations are returned
    assert len(operations) == 7
    assert next_page_token == ""

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_LIST_OPERATIONS_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_list_operations_filtered(database):
    populate_database(database)
    operation_filters = FilterParser.parse_listoperations_filters("stage != completed")
    operations, next_page_token = database.list_operations(
        operation_filters=operation_filters
    )
    assert len(operations) == 5
    assert next_page_token == ""


@check_for_postgres
def test_list_operations_sort_only(database):
    populate_database(database)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name(asc)")
    operations, next_page_token = database.list_operations(operation_filters)
    # Only incomplete operations are returned
    assert len(operations) == 7
    assert next_page_token == ""


def check_operation_order(operations, sort_keys, database):
    with database.session() as session:
        results = session.query(models.Operation).join(models.Job, models.Operation.job_name == models.Job.name)
        results = results.order_by(*sort_keys)
        for expected_operation, actual_operation in zip(results, operations):
            if expected_operation.name != actual_operation.name:
                return False

    return True


@check_for_postgres
def test_list_operations_sorted(database):
    def check_sort_filters(filter_string, sort_columns):
        operation_filters = FilterParser.parse_listoperations_filters(filter_string)
        operations, _ = database.list_operations(
            operation_filters=operation_filters
        )
        assert check_operation_order(operations, sort_columns, database)

    populate_database(database)
    # By default, operations are primarily sorted by queued_timestamp,
    check_sort_filters("", [models.Job.queued_timestamp, models.Operation.name])

    # When "name" is given as the "sort_order", the operations should
    # be ordered primarily by name in ascending order
    check_sort_filters("sort_order = name", [models.Operation.name])

    # This is equivalent to putting "(asc)" at the end of the sort order value
    check_sort_filters("sort_order = name(asc)", [models.Operation.name])

    # "(desc)" at the end of the sort_order value should produce descending order
    check_sort_filters("sort_order = name(desc)", [models.Operation.name.desc()])

    # The parser requires that (desc) be attached to the sort key name.
    # Whitespace isn't supported.
    with pytest.raises(InvalidArgumentError):
        check_sort_filters("sort_order = name (desc)", [models.Operation.name.desc()])


@check_for_postgres
def test_large_list_operations_multi_job(database):
    """ With a large number of jobs and corresponding operations in the database,
    ensure that the results are properly returned in page form.

    We populate the database with 2500 operations and ask for 1000-operation pages.
    We should get back two 1000-operation pages and one 500-operation page. """
    queued_timestamp = datetime.utcnow()
    total_jobs = 2500
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:04}"
                    )
                ],
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(total_jobs)
        ]
        session.add_all(jobs)

    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    token_timestamp = (queued_timestamp + timedelta(seconds=999)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0999"

    # Another 1000 are returned the second time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 1000
    token_timestamp = (queued_timestamp + timedelta(seconds=1999)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-1999"

    # The last 500 are returned the last time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 500
    assert next_token == ""


@check_for_postgres
def test_large_list_operations_same_job(database):
    """ With a large number of operations in the database, ensure that the results
    are properly returned in page form. All of the operations are for a single job.

    We populate the database with 2500 operations and ask for 1000-operation pages.
    We should get back two 1000-operation pages and one 500-operation page. """
    queued_timestamp = datetime.utcnow()
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:04}"
                    ) for i in range(2500)
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    token_timestamp = queued_timestamp.strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0999"

    # Another 1000 are returned the second time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 1000
    assert next_token == f"{token_timestamp}|test-operation-1999"

    # The last 500 are returned the last time
    operations, next_token = database.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 500
    assert next_token == ""


@check_for_postgres
def test_large_list_operations_filtered(database):
    """  We populate the database with 1000 operations and filter for the operations
    [250, 750). We should get back those 500 operations. """
    queued_timestamp = datetime.utcnow()
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i:03}"
                    ) for i in range(1000)
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    op_filters = FilterParser().parse_listoperations_filters(
        "name >= test-operation-250 & name < test-operation-750")
    # Only the first 1000 operations are returned the first time
    operations, next_token = database.list_operations(operation_filters=op_filters)
    assert len(operations) == 500
    for operation in operations:
        assert operation.name >= "test-operation-250" and operation.name < "test-operation-750"
    assert next_token == ""


@check_for_postgres
def test_paginated_custom_sort(database):
    """ Demonstrate that custom sort orders work on a paginated result set. """
    queued_timestamp = datetime.utcnow()
    total_jobs = 100
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{total_jobs-i-1:04}"
                    )
                ],
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(total_jobs)
        ]
        session.add_all(jobs)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name")
    page_size = 10
    next_page_token = ""
    for _ in range(total_jobs // page_size):
        operations, next_page_token = database.list_operations(
            operation_filters=operation_filters,
            page_size=page_size,
            page_token=next_page_token
        )
        assert len(operations) == page_size
        prev_operation = None
        for operation in operations:
            if prev_operation:
                assert prev_operation.name < operation.name
            prev_operation = operation


@check_for_postgres
def test_large_list_operations_exact(database):
    with database.session(reraise=True) as session:
        session.add(
            models.Job(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-{i}"
                    ) for i in range(1000)
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]})
            )
        )
    # All operations are returned
    operations, next_token = database.list_operations(page_size=1000)
    assert len(operations) == 1000
    # There is no next_token since we've gotten all of the results
    assert next_token == ""


@check_for_postgres
def test_lazy_expiry_with_list_operations(database):
    max_execution_timeout_seconds = timedelta(seconds=7200)
    queued_timestamp = datetime.utcnow() - 3 * max_execution_timeout_seconds
    old_worker_start_timestamp = datetime.utcnow() - 2 * max_execution_timeout_seconds
    new_worker_start_timestamp = datetime.utcnow()

    num_jobs_queued = 7
    # Add a bunch of jobs in queued state
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-queued-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-queued-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-queued-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_queued)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a long time
    num_jobs_old_executing = 11
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-executing-old-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-executing-old-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-old-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=old_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_old_executing)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a short time
    num_jobs_new_executing = 13
    with database.session(reraise=True) as session:
        jobs = [
            models.Job(
                name=f"test-executing-new-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-executing-new-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.Lease(
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=f"test-operation-new-{i}"
                    )
                ],
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=new_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"})
            ) for i in range(num_jobs_new_executing)
        ]
        session.add_all(jobs)

    # List all the operations
    # and make sure all the operations that exceed the
    # execution timeout are marked as cancelled

    # Mock the `notify_job_updated` method to inspect the call
    with mock.patch.object(database, '_notify_job_updated',
                           autospec=True) as mock_notify_fn:
        # Call list operations with the default filters (i.e. ignore COMPLETED operations)
        operations, _ = database.list_operations(
            page_size=1000,
            operation_filters=DEFAULT_OPERATION_FILTERS,
            max_execution_timeout=max_execution_timeout_seconds.seconds)

        # Make sure we did a notify for each of the cancelled jobs
        call_args, _ = mock_notify_fn.call_args
        assert mock_notify_fn.call_count == 1
        jobs_cancelled_notifications = call_args[0]

        count_cancelled_old = sum(('test-executing-old-job-' in job)
                                  for job in jobs_cancelled_notifications)

        # Make sure all the jobs that were old were cancelled
        # and that all the cancelled jobs were the old ones
        assert len(jobs_cancelled_notifications) == num_jobs_old_executing
        assert count_cancelled_old == num_jobs_old_executing

    # The queued and newly executing jobs should be in the list
    assert sum('test-operation-queued-' in op.name
               for op in operations) == num_jobs_queued
    assert sum('test-operation-new-' in op.name
               for op in operations) == num_jobs_new_executing

    # The jobs that have been executing for a while should be cancelled
    assert sum('test-operation-old-' in op.name for op in operations) == 0


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_create_operation(database):
    _MonitoringBus._instance = mock.Mock()
    job_name = "test-job"
    add_test_job(job_name, database)

    op_name = "test-operation"

    database.create_operation(op_name, job_name)

    with database.session(reraise=True) as session:
        op = session.query(models.Operation).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_CREATE_OPERATION_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_update_operation(database):
    _MonitoringBus._instance = mock.Mock()
    job_name = "test-job"
    add_test_job(job_name, database)

    op_name = "test-operation"
    with database.session(reraise=True) as session:
        session.add(models.Operation(
            name=op_name,
            job_name=job_name,
            cancelled=False
        ))

    database.update_operation(op_name, {"cancelled": True})

    with database.session(reraise=True) as session:
        op = session.query(models.Operation).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name
        assert op.cancelled

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_UPDATE_OPERATION_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_get_leases_by_state(database):
    populate_database(database)
    leases = database.get_leases_by_state(LeaseState(1))
    assert len(leases) == 4


@check_for_postgres
def test_create_lease(database):
    # There's no metrics check in this method because the duration metric for
    # Lease creation is published when assigning a Lease in the SQL data store.
    job_name = "test-job"
    add_test_job(job_name, database)

    state = 0
    lease = bots_pb2.Lease()
    lease.id = job_name
    lease.state = state

    database.create_lease(lease)

    with database.session(reraise=True) as session:
        lease = session.query(models.Lease).filter_by(job_name=job_name).first()
        assert lease is not None
        assert lease.job.name == job_name
        assert lease.state == state


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_update_lease(database):
    _MonitoringBus._instance = mock.Mock()
    job_name = "test-job"
    add_test_job(job_name, database)

    state = 0
    with database.session(reraise=True) as session:
        session.add(models.Lease(
            job_name=job_name,
            state=state
        ))

    database.update_lease(job_name, {"state": 1})
    with database.session(reraise=True) as session:
        lease = session.query(models.Lease).filter_by(job_name=job_name).first()
        assert lease is not None
        assert lease.job.name == job_name
        assert lease.state == 1

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_UPDATE_LEASE_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_load_unfinished_jobs(database):
    populate_database(database)

    jobs = database.load_unfinished_jobs()
    assert jobs
    assert jobs[0].name == "test-job"


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_assign_n_leases(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)

    assigned_jobs = {}
    def assignment_callback(jobs: List[Job]) -> Dict[str, Job]:
        job_map = {}
        for job in jobs:
            lease = job.lease
            if not lease:
                lease = job.create_lease("test-suite", data_store=database)
            else:
                job.worker_name = "test-suite"
            if lease:
                job.mark_worker_started()
                job_map[job.name] = job
        assigned_jobs.update(job_map)
        return job_map

    # Request 2 Leases from the data store. The example data contains
    # 2 queued jobs which have `OSFamily: linux`, so we should get two
    # jobs passed to the assignment callback as we requested.
    capability = hash_from_dict({"OSFamily": ["linux"]})
    database.assign_n_leases(
        lease_count=2,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 2

    # Clear the assigned jobs dict so we can test a bit more
    assigned_jobs.clear()
    assert len(assigned_jobs) == 0

    # Check that those jobs actually got assigned in the data store too.
    # There should be no matching jobs left when we ask for more leases.
    database.assign_n_leases(
        lease_count=10,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 0


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_assign_n_leases_limits(database):
    _MonitoringBus._instance = mock.Mock()
    populate_database(database)

    assigned_jobs = {}
    def assignment_callback(jobs: List[Job]) -> Dict[str, Job]:
        job_map = {}
        for job in jobs:
            lease = job.lease
            if not lease:
                lease = job.create_lease("test-suite", data_store=database)
            else:
                job.worker_name = "test-suite"
            if lease:
                job.mark_worker_started()
                job_map[job.name] = job
        assigned_jobs.update(job_map)
        return job_map

    # Request only 1 lease from the data store. The example data contains
    # 2 queued jobs matching `OSFamily: linux`, so we should get the job
    # with the highest priority (`other-job`).
    capability = hash_from_dict({"OSFamily": ["linux"]})
    database.assign_n_leases(
        lease_count=1,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 1
    assert "other-job" in assigned_jobs

    # Clear the assigned jobs dict so we can test further easily
    assigned_jobs.clear()
    assert len(assigned_jobs) == 0

    # Request many leases from the data store. There's only one job left
    # in the queue, so we should get just that job (`linux-job`).
    database.assign_n_leases(
        lease_count=10,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 1
    assert "linux-job" in assigned_jobs


@check_for_postgres
def test_to_internal_job(database):
    populate_database(database)

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name="finished-job").first()
        internal_job = job.to_internal_job(database)

        assert isinstance(internal_job.queued_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_start_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_completed_timestamp_as_datetime, datetime)

        assert internal_job.operation_stage.value == 4

    with database.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name="cancelled-job").first()
        internal_job = job.to_internal_job(database)

        assert isinstance(internal_job.queued_timestamp_as_datetime, datetime)
        assert isinstance(internal_job.worker_start_timestamp_as_datetime, datetime)
        assert internal_job.worker_completed_timestamp_as_datetime is None

        assert internal_job.cancelled
        assert internal_job.operation_stage.value == 4


# Check that the metrics returned match what is
# populated in the test database
@check_for_postgres
def test_get_metrics(database):
    populate_database(database)

    # Setup expected counts for each category
    expected_metrics = {}
    expected_metrics[MetricCategories.LEASES.value] = {
        LeaseState.UNSPECIFIED.value: 0,
        LeaseState.PENDING.value: 4,
        LeaseState.COMPLETED.value: 1,
        LeaseState.CANCELLED.value: 1,
        LeaseState.ACTIVE.value: 1
    }
    expected_metrics[MetricCategories.JOBS.value] = {
        OperationStage.UNKNOWN.value: 0,
        OperationStage.CACHE_CHECK.value: 0,
        OperationStage.QUEUED.value: 5,
        OperationStage.EXECUTING.value: 0,
        OperationStage.COMPLETED.value: 2
    }

    returned_metrics = database.get_metrics()
    assert returned_metrics != {}
    for category in MetricCategories:
        assert category.value in returned_metrics
        assert expected_metrics[category.value] == returned_metrics[category.value]


# Make it so connection to the database throws a
# DatabaseError, and ensure it's non-fatal
@check_for_postgres
def test_get_metrics_error(database):
    populate_database(database)

    with mock.patch.object(database, 'session', autospec=True,
                           side_effect=DatabaseError("TestException")) as session_fn:
        returned_metrics = database.get_metrics()
        assert session_fn.called
        assert returned_metrics == {}


@check_for_postgres
def test_watcher_exits_after_flag_update(database):
    poll_interval = 0.02
    delta = 0.5

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Thread should be running in a loop already
    for i in range(3):
        assert database.watcher.is_alive()
        sleep(poll_interval + delta)

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False


@check_for_postgres
@mock.patch('buildgrid.server.metrics_utils.create_timer_record', new=mock_create_timer_record)
def test_watcher_detects_changes(database):
    _MonitoringBus._instance = mock.Mock()
    poll_interval = 0.02
    delta = 0.5

    populate_database(database)
    job_name = 'other-job'
    job = database.get_job_by_name(job_name)

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Ensure the watcher is running
    assert database.watcher.is_alive()

    # Start watching a job
    database.watch_job(job, 'other-operation', 'test')

    spec = database.watched_jobs[job_name]
    with mock.patch.object(spec, 'event') as patched_event:
        # Update the job
        database.update_job(
            job_name,
            {'stage': OperationStage.EXECUTING.value}
        )
        sleep(poll_interval + delta + delta)
        assert spec.last_state.operation_stage == OperationStage.EXECUTING
        spec.event.notify_change.assert_called_once()

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False

    metadata = {'instance-name': database._instance_name}
    mock_timing_record = mock_create_timer_record(
        name=DATA_STORE_CHECK_FOR_UPDATE_TIME_METRIC_NAME, metadata=metadata)
    call_list = [mock.call(mock_timing_record)]
    _MonitoringBus._instance.send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_for_postgres
def test_watcher_polling_handles_exceptions(database):
    def _failing_get_watched_jobs():
        raise Exception("db-fail")

    def notify(job_name):
        with database.session() as session:
            database._notify_job_updated(job_name, session)

    poll_interval = 0.02
    delta = 0.5

    populate_database(database)
    job_name = 'other-job'
    job = database.get_job_by_name(job_name)

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Thread should be running already
    assert database.watcher.is_alive()

    # Simulate database failure by patching this method to raise
    database._get_watched_jobs = _failing_get_watched_jobs
    database.get_job_by_name = _failing_get_watched_jobs

    # Start watching a job
    database.watch_job(job, 'other-operation', 'test')

    # Send a notification
    notify(job_name)

    # Make sure the watcher thread is still running even when
    # the database keeps raising exceptions
    for i in range(3):
        assert database.watcher.is_alive()
        notify(job_name)
        sleep(poll_interval + delta)

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False


@check_for_postgres
def test_watcher_polling_handles_none_job(database):
    def _failing_get_watched_jobs():
        return None

    def _failing_get_job_by_name():
        return None

    def notify(job_name):
        with database.session() as session:
            database._notify_job_updated(job_name, session)

    poll_interval = 0.02
    delta = 0.5

    populate_database(database)
    job_name = 'other-job'
    job = database.get_job_by_name(job_name)

    # Make the poll interval short
    database.poll_interval = poll_interval

    # Thread should be running already
    assert database.watcher.is_alive()

    # Simulate database failure and suppressed exception by patching
    # this method to return None
    database._get_watched_jobs = _failing_get_watched_jobs
    database.get_job_by_name = _failing_get_job_by_name

    # Start watching a job
    database.watch_job(job, 'other-operation', 'test')

    # Send a notification
    notify(job_name)

    # Make sure the watcher thread is still running even when
    # getting jobs returns None
    for i in range(3):
        assert database.watcher.is_alive()
        notify(job_name)
        sleep(poll_interval + delta)

    # Update flag to ask the watcher to stop after this iteration
    database.watcher_keep_running = False

    # Wait a bit
    sleep(poll_interval + delta)

    # Assert that the watcher thread has exited
    assert database.watcher.is_alive() is False


@check_for_postgres
def test_external_job_completion_loads_result(database_pair):
    database, database_external = database_pair

    # Create job in-memory
    job_name = "test-job-external-completion"
    platform_reqs = {"OSFamily": set(["solaris"])}
    inmem_job = Job(do_not_cache=True,
                    action=generate_dummy_action(do_not_cache=True),
                    action_digest=models.string_to_digest("test-action/100"),
                    name=job_name,
                    priority=1,
                    stage=OperationStage.CACHE_CHECK,
                    operation_names=["test-operation"],
                    platform_requirements=platform_reqs)
    # Add job to db
    database.create_job(inmem_job)

    # Create a dummy ActionResult
    output_file_1 = remote_execution_pb2.OutputFile(
        path="/path/to/file1.txt",
        digest=models.string_to_digest("output_file_1_digest/123"))
    output_directory_1 = remote_execution_pb2.OutputDirectory(
        path="/path/to/dir1",
        tree_digest=models.string_to_digest("output_dir_1_digest/456"))

    action_result = remote_execution_pb2.ActionResult()
    action_result.output_files.extend([output_file_1])
    action_result.output_directories.extend([output_directory_1])
    action_result.exit_code = 0
    action_result.stdout_raw = str.encode("some-stdout")

    action_result_in_any_proto = any_pb2.Any()
    action_result_in_any_proto.Pack(action_result)

    # Update job to completed with a result "externally"
    inmem_job_db2 = database_external.get_job_by_name(job_name)

    # Simulate lease assignment/job completion
    state = 0
    lease = bots_pb2.Lease()
    lease.id = job_name
    lease.state = state

    database_external.create_lease(lease)

    # Reload job with lease
    inmem_job_db2 = database_external.get_job_by_name(job_name)
    inmem_job_db2.update_lease_state(LeaseState.ACTIVE, data_store=database_external)
    # Reload job (has a lease now)
    inmem_job_db2.update_lease_state(LeaseState.COMPLETED,
                                     status=status_pb2.Status(code=0),
                                     result=action_result_in_any_proto,
                                     action_cache=None,
                                     data_store=database_external,
                                     skip_notify=True)
    inmem_job_db2.update_operation_stage(OperationStage.COMPLETED,
                                         data_store=database_external)

    # Reload job from db1 and check for result being present
    inmem_job = database.get_job_by_name(job_name)
    inmem_job_serialized = inmem_job.action_result.SerializeToString()
    action_result_serialized = action_result.SerializeToString()

    # Compare fields that were set from the "worker"
    # as ActionResult may contain additional metadata, like timings etc
    assert inmem_job.action_result.output_files == action_result.output_files
    assert inmem_job.action_result.output_directories == action_result.output_directories
    assert inmem_job.action_result.exit_code == action_result.exit_code
    assert inmem_job.action_result.stdout_raw == action_result.stdout_raw


@check_for_postgres
def test_pruner_delete_function(database):

    # populate database
    NUM_ROWS_ADDED = 1000
    now = datetime.utcnow()
    prefix = 'completed'
    with database.session(reraise=True) as session:
        session.add_all([
            completed_job(prefix, x, now - timedelta(minutes=1))
            for x in range(NUM_ROWS_ADDED)
        ])

    # confirm existence
    with database.session(reraise=True) as session:
        jobs = session.query(models.Job).filter(models.Job.name.like(f"{prefix}-job/%"))
        assert jobs is not None
        assert jobs.count() == NUM_ROWS_ADDED

    # invoke the deleter function confirming we only deleted TEST_LIMIT rows
    TEST_LIMIT = 100
    assert database._delete_jobs_prior_to(now, TEST_LIMIT) == TEST_LIMIT

    # confirm remaining
    with database.session(reraise=True) as session:
        jobs = session.query(models.Job).filter(models.Job.name.like(f"{prefix}-job/%"))
        assert jobs.count() == (NUM_ROWS_ADDED - TEST_LIMIT)


@check_for_postgres
def test_pruner_thread(database_pruning_enabled):

    prefix = "completed"
    now = datetime.utcnow()
    with database_pruning_enabled.session(reraise=True) as session:
        session.add(completed_job(prefix, 1, now - timedelta(minutes=1)))

    # confirm existence
    job_name = f"{prefix}-job/1"
    with database_pruning_enabled.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is not None
        assert job.name == job_name

    # sleep for 2 cycles, allowing the background pruning thread time to delete the record
    sleep(TEST_JOB_PRUNING_PERIOD.total_seconds() * 2)

    # confirm non-existence
    with database_pruning_enabled.session(reraise=True) as session:
        job = session.query(models.Job).filter_by(name=job_name).first()
        assert job is None


@check_for_postgres
def test_operation_to_proto(database):
    action_result = remote_execution_pb2.ActionResult(exit_code=12, stdout_raw=b'hello!')
    action_result_digest = remote_execution_pb2.Digest(hash="finished-action-result", size_bytes=200)
    add_test_blob(action_result_digest, action_result.SerializeToString(), database.storage)

    execute_response = remote_execution_pb2.ExecuteResponse(result=action_result)
    execute_response_digest = remote_execution_pb2.Digest(hash="execute-response-digest", size_bytes=123)
    add_test_blob(execute_response_digest, execute_response.SerializeToString(), database.storage)

    # Adding a completed Job and associated Operation:
    operation_name = "finished-operation"
    with database.session(reraise=True) as session:
        job = models.Job(
            name="a-finished-job",
            action=generate_dummy_action().SerializeToString(),
            action_digest="a-finished-action/35",
            stage=OperationStage.COMPLETED.value,
            result=f"{execute_response_digest.hash}/{execute_response_digest.size_bytes}",
            operations=[models.Operation(name=operation_name, job_name="a-finished-job")])
        session.add(job)

    # Fetching back the Operation and converting it to protobuf:
    with database.session(reraise=True) as session:
        operation = session.query(models.Operation).filter_by(name=operation_name).first()
        assert operation

        operation_proto = operation.to_protobuf(data_store=database)
        assert operation_proto and operation_proto.done
        assert operation_proto.response.Is(remote_execution_pb2.ExecuteResponse().DESCRIPTOR)

        returned_execute_response = remote_execution_pb2.ExecuteResponse()
        operation_proto.response.Unpack(returned_execute_response)
        assert returned_execute_response.result == action_result

@check_for_postgres
def test_operation_to_proto_with_missing_execute_response(database):
    # Adding a completed Job and associated Operation:
    operation_name = "finished-operation2"
    execute_response_digest = remote_execution_pb2.Digest(hash="execute-response-digest-not-in-cas",
                                                          size_bytes=1234)
    with database.session(reraise=True) as session:
        job = models.Job(
            name="a-finished-job",
            action=generate_dummy_action().SerializeToString(),
            action_digest="a-finished-action/35",
            stage=OperationStage.COMPLETED.value,
            result=f"{execute_response_digest.hash}/{execute_response_digest.size_bytes}",
            operations=[models.Operation(name=operation_name, job_name="a-finished-job")])
        session.add(job)

    assert not database.storage.has_blob(execute_response_digest)

    # Fetching back the Operation and converting it to protobuf:
    with database.session(reraise=True) as session:
        operation = session.query(models.Operation).filter_by(name=operation_name).first()
        assert operation

        operation_proto = operation.to_protobuf(data_store=database)
        assert operation_proto and operation_proto.done
        assert operation_proto.error.code == code_pb2.DATA_LOSS


@check_for_postgres
def test_job_leases_order_by_id(database):
    # GIVEN
    operation_name = "test_operation_lease_id_order"
    job_name = "test_job_lease_id_order"
    with database.session(reraise=True) as session:
        job = models.Job(
                name=job_name,
                action=generate_dummy_action().SerializeToString(),
                action_digest="some-action/35",
                priority=20,
                stage=OperationStage.COMPLETED.value,
                cancelled=True,
                queued_timestamp=datetime(2019, 6, 1),
                queued_time_duration=60,
                worker_start_timestamp=datetime(2019, 6, 1, minute=1),
                leases=[models.Lease(
                    id=0,
                    status=0,
                    state=LeaseState.CANCELLED.value
                ), models.Lease(
                    id=1,
                    status=0,
                    state=LeaseState.ACTIVE.value
                )],
                operations=[
                    models.Operation(
                        name=operation_name,
                        cancelled=True,
                        **populated_operation_request_metadata_fields()
                    )
                ],
                platform_requirements=hash_from_dict({"OSFamily": ["linux"]})
            )
        session.add(job)


    with database.session(reraise=True) as session:
        # WHEN
        read_job = session.query(models.Job).filter_by(name=job_name).first()
        job_internal = read_job.to_internal_job(database)

        # THEN
        assert len(read_job.active_leases) == 2
        assert read_job.active_leases[0].id == 1
        assert read_job.active_leases[1].id == 0
        assert job_internal.lease.state == LeaseState.ACTIVE.value
