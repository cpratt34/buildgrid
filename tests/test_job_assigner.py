# Copyright (C) 2022 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import hashlib
import json
import tempfile
import time
from typing import Iterator, Optional
from unittest.mock import MagicMock
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Digest
from buildgrid.server.job import Job

from grpc._server import _Context
import pytest
from _pytest.fixtures import SubRequest

from buildgrid._enums import BotStatus
from buildgrid._exceptions import DuplicateBotSessionError
from buildgrid._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession
from buildgrid.server.bots.job_assigner import JobAssigner, Worker
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.persistence.interface import DataStoreInterface
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore


@pytest.fixture
def context() -> Iterator[MagicMock]:
    context_mock = MagicMock(spec=_Context)
    context_mock.time_remaining.return_value = 5
    yield context_mock

@pytest.fixture
def expired_context() -> Iterator[MagicMock]:
    context_mock = MagicMock(spec=_Context)
    context_mock.is_active.return_value = False
    context_mock.time_remaining.return_value = 5
    yield context_mock


@pytest.fixture
def bot_session() -> Iterator[BotSession]:
    bot = BotSession()
    bot.bot_id = 'ana'
    bot.name = 'test-worker'
    bot.status = BotStatus.OK.value
    yield bot


@pytest.fixture(params=["mem", "sql"])
def data_store(request: SubRequest) -> Iterator[DataStoreInterface]:
    data_store: Optional[DataStoreInterface] = None
    try:
        storage = LRUMemoryCache(1024 * 1024)
        if request.param == "sql":
            db = tempfile.NamedTemporaryFile().name
            data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
        elif request.param == "mem":
            data_store = MemoryDataStore(storage)
    finally:
        if data_store is not None:
            data_store.stop()


@pytest.fixture
def job_assigner(data_store: DataStoreInterface) -> Iterator[JobAssigner]:
    job_assigner: Optional[JobAssigner] = None
    try:
        job_assigner = JobAssigner(data_store)
        job_assigner.start()
        yield job_assigner
    finally:
        if job_assigner is not None:
            job_assigner.stop()


def test_get_partial_capabilities_hashes(job_assigner: JobAssigner) -> None:
    capabilities = {}
    assert job_assigner._get_partial_capabilities_hashes(capabilities) == [
        hashlib.sha1(json.dumps(capabilities, sort_keys=True).encode()).hexdigest()]

    expected_partial_capabilities = [
        {},
        {'OSFamily': ['Linux']},
        {'ISA': ['x86-32']},
        {'ISA': ['x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-64']},
        {'ISA': ['x86-32', 'x86-64']},
        {'OSFamily': ['Linux'], 'ISA': ['x86-32', 'x86-64']}
    ]
    expected_partial_capabilities_hashes = sorted(list(map(
        lambda cap: hashlib.sha1(json.dumps(cap, sort_keys=True).encode()).hexdigest(),
        expected_partial_capabilities)))

    capabilities = {'OSFamily': 'Linux', 'ISA': {'x86-32', 'x86-64'}}
    assert sorted(job_assigner._get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes

    # Should be the same if the string is passed in as a singleton set
    capabilities = {'OSFamily': {'Linux'}, 'ISA': {'x86-32', 'x86-64'}}
    assert sorted(job_assigner._get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes

    # Changing the order of the ISA values should produce the same hashes
    capabilities = {'OSFamily': 'Linux', 'ISA': {'x86-64', 'x86-32'}}
    assert sorted(job_assigner._get_partial_capabilities_hashes(capabilities)) == expected_partial_capabilities_hashes


def test_register_worker(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 10, context)
    assert isinstance(worker, Worker)

    # The worker should be stored in the worker map
    assert job_assigner._worker_map[bot_session.name] == worker

    # There should be one bucket, for the empty capability set
    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    assert len(job_assigner._buckets) == 1
    assert capability_hash in job_assigner._buckets
    assert len(job_assigner._buckets[capability_hash]) == 1
    assert job_assigner._buckets[capability_hash][0] == bot_session.name


def test_remove_worker(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 10, context)
    job_assigner.remove_worker(bot_session.name)

    assert worker not in job_assigner._worker_map.values()

    # The bucket may or may not have been cleaned up already.
    # If not, make sure it is empty and then double-check cleanup.
    cleaned = False
    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    with job_assigner._buckets_lock:
        if capability_hash in job_assigner._buckets:
            assert len(job_assigner._buckets[capability_hash]) == 0
        else:
            cleaned = True
    if not cleaned:
        # NOTE: The JobAssigner currently waits for a hardcoded 1s between
        # checking buckets. If that changes, this sleep will also need to
        # change to match.
        time.sleep(1.1)
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets) == 0


# NOTE: This test verifies the code path where a worker connects but there is
# no work available. The case where there is work available is covered by the
# Bots service's integration tests.
def test_worker_expiry(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 1, context)
    lease = worker.wait_for_work()
    assert worker.expired
    assert lease is None

    # Check that we can't assign a lease to an expired worker
    #
    # This job won't even exist in the data store, but the check for worker expiry
    # should happen before we issue any database queries to minimize wasted time.
    job = Job(
        do_not_cache=False,
        action=Action(),
        action_digest=Digest()
    )
    assigned = worker.maybe_assign_lease(job, data_store=job_assigner._data_store)
    assert not assigned

# Validate that if a new request comes in for the same BotSession that the old one is evicted
# and marked as cancelled
def test_multiple_workers_same_name(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 5, context)
    worker2 = job_assigner.register_worker(bot_session, 5, context)
    
    assert worker not in job_assigner._worker_map.values()
    assert worker.cancelled
    assert worker2 in job_assigner._worker_map.values()

    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets[capability_hash]) == 1
    
    job_assigner.remove_worker(bot_session.name)

    assert worker2 not in job_assigner._worker_map.values()
    
    cleaned = False
    with job_assigner._buckets_lock:
        if capability_hash in job_assigner._buckets:
            assert len(job_assigner._buckets[capability_hash]) == 0
        else:
            cleaned = True
    if not cleaned:
        # NOTE: The JobAssigner currently waits for a hardcoded 1s between
        # checking buckets. If that changes, this sleep will also need to
        # change to match.
        time.sleep(1.1)
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets) == 0

# This test validates that if a second registration for a bot name whose context is no longer active
# that worker is replaced and the original marked as cancelled
def test_multiple_workers_same_name_inactive_context(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock, expired_context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 5, expired_context)
    worker2 = job_assigner.register_worker(bot_session, 5, context)

    assert worker not in job_assigner._worker_map.values()
    assert worker.cancelled
    assert worker2 in job_assigner._worker_map.values()
    
    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets[capability_hash]) == 1
    
    job_assigner.remove_worker(bot_session.name)

    assert worker2 not in job_assigner._worker_map.values()
    
    cleaned = False
    with job_assigner._buckets_lock:
        if capability_hash in job_assigner._buckets:
            assert len(job_assigner._buckets[capability_hash]) == 0
        else:
            cleaned = True
    if not cleaned:
        # NOTE: The JobAssigner currently waits for a hardcoded 1s between
        # checking buckets. If that changes, this sleep will also need to
        # change to match.
        time.sleep(1.1)
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets) == 0

# Test that calling remove_worker with a worker which isn't the currently registered worker
# doesn't remove the currently registered worker
def test_remove_worker_given_worker_argument(job_assigner: JobAssigner, bot_session: BotSession, context: MagicMock) -> None:
    worker = job_assigner.register_worker(bot_session, 5, context)
    job_assigner.remove_worker(bot_session.name, worker)

    worker2 = job_assigner.register_worker(bot_session, 5, context)
    job_assigner.remove_worker(bot_session.name, worker)

    assert worker2 in job_assigner._worker_map.values()
    capability_hash = hashlib.sha1(json.dumps({}).encode()).hexdigest()
    with job_assigner._buckets_lock:
        assert len(job_assigner._buckets[capability_hash]) == 1

    job_assigner.remove_worker(bot_session.name, worker2)
    assert worker2 not in job_assigner._worker_map.values()
