# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


import copy
import os
import tempfile
import time
from unittest import mock
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import LogStream

import grpc
from grpc._server import _Context
import pytest

from buildgrid._enums import LeaseState, OperationStage
from buildgrid._exceptions import StorageFullError
from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.rpc import code_pb2

from buildgrid.utils import create_digest, hash_from_dict
from buildgrid.server.controller import ExecutionController
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.execution import service
from buildgrid.server.execution.service import ExecutionService
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore

from .utils.logstream import serve_logstream
from .utils.utils import run_in_subprocess


server = mock.create_autospec(grpc.server)

command = remote_execution_pb2.Command()
command_digest = create_digest(command.SerializeToString())

action = remote_execution_pb2.Action(command_digest=command_digest,
                                     do_not_cache=False)
action_digest = create_digest(action.SerializeToString())


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


PARAMS = [(impl, use_cache) for impl in ["sql", "mem"]
          for use_cache in ["action-cache", "no-action-cache"]]


# Return informative test ids for tests using the controller fixture
def idfn(params_value):
    return f"{params_value[0]}-{params_value[1]}"


@pytest.fixture(params=PARAMS, ids=idfn)
def controller(request):
    impl, use_cache = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    write_session = storage.begin_write(command_digest)
    write_session.write(command.SerializeToString())
    storage.commit_write(command_digest, write_session)

    write_session = storage.begin_write(action_digest)
    write_session.write(action.SerializeToString())
    storage.commit_write(action_digest, write_session)

    try:
        if impl == "sql":
            db = tempfile.NamedTemporaryFile().name
            data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
        elif impl == "mem":
            data_store = MemoryDataStore(storage)

        keys = ["OSFamily", "ISA"]
        if use_cache == "action-cache":
            cache = LruActionCache(storage, 50)
            yield ExecutionController(
                data_store, storage=storage, action_cache=cache,
                property_keys=keys, match_properties=keys)
        else:
            yield ExecutionController(
                data_store, storage=storage, property_keys=keys,
                match_properties=keys)
    finally:
        data_store.watcher_keep_running = False


PARAMS_MAX_EXECUTION = [(impl, max_execution_timeout) for impl in ["sql", "mem"]
                        for max_execution_timeout in [None, 0.1, 2]]


@pytest.fixture(params=PARAMS_MAX_EXECUTION, ids=idfn)
def controller_max_execution_timeout(request):
    impl, max_execution_timeout = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    write_session = storage.begin_write(command_digest)
    write_session.write(command.SerializeToString())
    storage.commit_write(command_digest, write_session)

    write_session = storage.begin_write(action_digest)
    write_session.write(action.SerializeToString())
    storage.commit_write(action_digest, write_session)

    try:
        if impl == "sql":
            db = tempfile.NamedTemporaryFile().name
            data_store = SQLDataStore(storage, connection_string=f"sqlite:///{db}", automigrate=True)
        elif impl == "mem":
            data_store = MemoryDataStore(storage)

        keys = ['OSFamily', 'ISA']
        yield ExecutionController(data_store, storage=storage,
                                  property_keys=keys, match_properties=keys,
                                  max_execution_timeout=max_execution_timeout)
    finally:
        data_store.watcher_keep_running = False


def test_unregister_operation_peer(controller, context):
    scheduler = controller.execution_instance._scheduler
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())

    watch_spec = scheduler.data_store.watched_jobs.get(job_name)
    assert watch_spec is not None
    assert operation_name in watch_spec.operations
    assert context.peer() in watch_spec.peers_for_operation(operation_name)

    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())
    watch_spec = scheduler.data_store.watched_jobs.get(job_name)
    assert watch_spec is None

    operation_name = controller.execution_instance.register_job_peer(job_name,
                                                                     context.peer())
    scheduler._update_job_operation_stage(job_name, OperationStage.COMPLETED)
    controller.execution_instance.unregister_operation_peer(operation_name, context.peer())
    if isinstance(scheduler.data_store, MemoryDataStore):
        assert scheduler.data_store.get_job_by_name(job_name) is None
    elif isinstance(scheduler.data_store, SQLDataStore):
        assert job_name not in scheduler.data_store.response_cache


@pytest.mark.parametrize("monitoring", [True, False])
def test_update_lease_state(controller, context, monitoring):
    scheduler = controller.execution_instance._scheduler
    with mock.patch.object(scheduler.data_store, 'activate_monitoring', autospec=True) as activate_monitoring_fn:
        if monitoring:
            scheduler.activate_monitoring()
            assert activate_monitoring_fn.call_count == 1
        else:
            assert activate_monitoring_fn.call_count == 0

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    job = scheduler.data_store.get_job_by_name(job_name)
    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    lease = copy.deepcopy(job_lease)
    scheduler.update_job_lease_state(job_name, lease)

    lease.state = LeaseState.ACTIVE.value
    scheduler.update_job_lease_state(job_name, lease)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert lease.state == job._lease.state

    lease.state = LeaseState.COMPLETED.value
    scheduler.update_job_lease_state(job_name, lease)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert job._lease is None

    with mock.patch.object(scheduler.data_store, 'deactivate_monitoring', autospec=True) as deactivate_monitoring_fn:
        if monitoring:
            # TODO: Actually test that monitoring functioned as expected
            scheduler.deactivate_monitoring()
            assert deactivate_monitoring_fn.call_count == 1
        else:
            assert deactivate_monitoring_fn.call_count == 0


def test_retry_job_lease(controller, context):
    scheduler = controller.execution_instance._scheduler
    scheduler.MAX_N_TRIES = 2

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    scheduler._update_job_operation_stage(job_name, OperationStage.EXECUTING)

    job = scheduler.data_store.get_job_by_name(job_name)

    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    scheduler.retry_job_lease(job_name)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.n_tries == 2

    scheduler.retry_job_lease(job_name)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.n_tries == 2
    assert job.status_code == code_pb2.ABORTED
    assert job.operation_stage == OperationStage.COMPLETED


def test_requeue_queued_job(controller, context):
    scheduler = controller.execution_instance._scheduler

    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    job = scheduler.data_store.get_job_by_name(job_name)

    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    assigned_jobs = []

    def _assignment_cb(jobs):
        result = {}
        for job in jobs:
            assigned_jobs.append(job.name)
            result[job.name] = job
        return result

    scheduler.data_store.assign_n_leases(
        capability_hash=hash_from_dict({}),
        lease_count=1,
        assignment_callback=_assignment_cb
    )
    for name in assigned_jobs:
        scheduler._update_job_operation_stage(name, OperationStage.EXECUTING)
    assert len(assigned_jobs) == 1

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.name in assigned_jobs
    assert job.operation_stage == OperationStage.EXECUTING

    # Make sure that retrying a job that was assigned but
    # not marked as in progress properly re-queues

    scheduler.retry_job_lease(job_name)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.operation_stage == OperationStage.QUEUED

    assigned_jobs.clear()
    scheduler.data_store.assign_n_leases(
        capability_hash=hash_from_dict({}),
        lease_count=1,
        assignment_callback=_assignment_cb
    )
    for name in assigned_jobs:
        scheduler._update_job_operation_stage(name, OperationStage.EXECUTING)
    assert len(assigned_jobs) == 1

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job.name in assigned_jobs
    assert job.operation_stage == OperationStage.EXECUTING


# Test that jobs can be created/completed with no action-cache, a working action cache,
# and an action-cache that throws exceptions when used
@pytest.mark.parametrize("cache_errors", [True, False])
def test_complete_lease_action_cache_error(controller, context, cache_errors):
    scheduler = controller.execution_instance._scheduler
    if scheduler._action_cache and cache_errors:
        with mock.patch.object(scheduler._action_cache, 'get_action_result', autospec=True) as ac_mock:
            ac_mock.side_effect = ConnectionError("Fake Connection Error")
            job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=False)
            assert ac_mock.call_count == 1
    else:
        job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=False)

    job = scheduler.data_store.get_job_by_name(job_name)
    job_lease = job.create_lease("test-suite", data_store=scheduler.data_store)
    if isinstance(scheduler.data_store, SQLDataStore):
        scheduler.data_store.create_lease(job_lease)

    lease = copy.deepcopy(job_lease)
    scheduler.update_job_lease_state(job_name, lease)

    lease.state = LeaseState.ACTIVE.value
    scheduler.update_job_lease_state(job_name, lease)
    job = scheduler.data_store.get_job_by_name(job_name)
    assert lease.state == job._lease.state

    lease.state = LeaseState.COMPLETED.value
    if scheduler._action_cache and cache_errors:
        with mock.patch.object(scheduler._action_cache, 'update_action_result', autospec=True) as ac_mock:
            ac_mock.side_effect = StorageFullError("TestActionCache is full")
            scheduler.update_job_lease_state(job_name, lease)
            assert ac_mock.call_count == 1
    else:
        scheduler.update_job_lease_state(job_name, lease)

    job = scheduler.data_store.get_job_by_name(job_name)
    assert job._lease is None


def test_get_metadata_for_leases(controller, context):

    def _test_get_metadata_for_leases():
        with serve_logstream(['testing']) as server:
            channel = grpc.insecure_channel(server.remote)
            scheduler = controller.execution_instance._scheduler

            scheduler._logstream_channel = channel
            scheduler._logstream_instance_name = 'testing'

            job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
            job = scheduler.data_store.get_job_by_name(job_name)
            operation_name = job.register_new_operation(data_store=scheduler.data_store)

            lease = job.create_lease('test-suite', data_store=scheduler.data_store)
            stdout_stream = LogStream(name="stdout-stream", write_resource_name="stdout-stream/write")
            stderr_stream = LogStream(name="stderr-stream", write_resource_name="stderr-stream/write")
            job.set_stdout_stream(stdout_stream, data_store=scheduler.data_store)
            job.set_stderr_stream(stderr_stream, data_store=scheduler.data_store)

            metadata = scheduler.get_metadata_for_leases([lease])
            assert len(metadata) == 1

            job = scheduler.data_store.get_job_by_name(job_name)
            job_metadata = job.get_metadata(writeable_streams=True)
            assert len(metadata[0]) == 2
            assert metadata[0][0] == 'executeoperationmetadata-bin'
            assert metadata[0][1] == job_metadata.SerializeToString()
            assert job_metadata.stdout_stream_name == job._stdout_stream_write_name
            assert job_metadata.stderr_stream_name == job._stderr_stream_write_name
        return True

    assert run_in_subprocess(_test_get_metadata_for_leases)


def test_list_operations(controller, context):
    """ Test that the scheduler reports the correct
    number of operations when calling list_operations() """
    scheduler = controller.execution_instance._scheduler
    operations, _ = scheduler.list_operations()
    assert len(operations) == 0
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    operations, _ = scheduler.list_operations()
    assert len(operations) == 0
    scheduler.register_job_peer(job_name, "bot1")
    scheduler.register_job_peer(job_name, "bot2")
    scheduler.register_job_peer(job_name, "bot3")
    operations, _ = scheduler.list_operations()
    assert len(operations) == 3


def test_query_operation_metadata(controller):
    """ Test that the scheduler returns the expected `RequestMetadata`
    information for an operation.
    """
    scheduler = controller.execution_instance._scheduler
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)

    request_metadata = remote_execution_pb2.RequestMetadata()
    request_metadata.tool_details.tool_name = "my-tool"
    request_metadata.tool_details.tool_version = "1.0"
    request_metadata.tool_invocation_id = "invId123"
    request_metadata.correlated_invocations_id = "corId456"

    operation_name = scheduler.register_job_peer(job_name, "bot1",
                                                 request_metadata=request_metadata)

    metadata = scheduler.get_operation_request_metadata(operation_name)

    if isinstance(scheduler.data_store, SQLDataStore):
        assert metadata is not None
        expected_keys = {'tool-name', 'tool-version', 'invocation-id',
                         'correlated-invocations-id'}
        assert expected_keys <= metadata.keys()
        assert metadata['tool-name'] == request_metadata.tool_details.tool_name
        assert metadata['tool-version'] == request_metadata.tool_details.tool_version
        assert metadata['invocation-id'] == request_metadata.tool_invocation_id
        assert metadata['correlated-invocations-id'] == request_metadata.correlated_invocations_id
    elif isinstance(scheduler.data_store, MemoryDataStore):
        # The in-memory datastore does not keep metadata entries.
        assert metadata is None
    else:
        assert False

@pytest.mark.parametrize("n_operations", [1, 2])
@pytest.mark.parametrize("sleep_time", [0.2])
@pytest.mark.parametrize("trigger_request",
                         [None, 'get_job_operation', 'register_job_peer',
                          'register_job_operation_peer', 'queue_job'])
def test_max_execution_timeout(controller_max_execution_timeout, context,
                               sleep_time, n_operations, trigger_request):
    scheduler = controller_max_execution_timeout.execution_instance._scheduler

    data_store = scheduler.data_store
    max_execution_timeout = scheduler._max_execution_timeout

    # Queue Job
    job_name = scheduler.queue_job_action(action, action_digest, skip_cache_lookup=True)
    job = data_store.get_job_by_name(job_name)

    # Create n operations
    operation_names = []
    for i in range(n_operations):
        operation_names.append(job.register_new_operation(data_store=data_store))

    for i in range(n_operations):
        operation_initially = scheduler.get_job_operation(operation_names[i])
        assert operation_initially.done is False

    def _assign_lease_cb(jobs):
        for job in jobs:
            # Mark this job as "executing"
            job.create_lease('test-worker', 'bot-id', data_store=data_store)
            job.mark_worker_started()
        return {job.name: job for job in jobs}

    data_store.assign_n_leases(
        lease_count=1,
        capability_hash=hash_from_dict({}),
        assignment_callback=_assign_lease_cb
    )  # Sets the start time

    job_lease = data_store.get_job_by_name(job_name).lease

    job_lease.state = LeaseState.ACTIVE.value
    scheduler.update_job_lease_state(job_name, job_lease)

    # Make sure it was marked as "Executing"
    job_after_assignment = data_store.get_job_by_name(job_name)
    assert job_after_assignment.operation_stage is OperationStage.EXECUTING

    # Wait and see it marked as cancelled when exceeding execution timeout...
    time.sleep(sleep_time)

    # Simulate a request (or not) to see if the timeout has kicked in
    if trigger_request:
        if trigger_request == 'get_job_operation':
            _ = scheduler.get_job_operation(operation_names[0])
        elif trigger_request == 'register_job_peer':
            scheduler.register_job_peer(job_name, 'test-peer')
        elif trigger_request == 'register_job_operation_peer':
            _ = scheduler.register_job_operation_peer(operation_names[0], 'test-peer-2')
        elif trigger_request == 'queue_job':
            _ = scheduler.queue_job_action(action, action_digest)

    # Retrieve job from data store after the request that could've triggered expiry
    job_after_sleep = data_store.get_job_by_name(job_name)

    if trigger_request and max_execution_timeout and sleep_time >= max_execution_timeout:
        for operation in job_after_sleep.get_all_operations():
            assert operation.done is True
        assert job_after_sleep.cancelled is True
    else:
        for operation in job_after_sleep.get_all_operations():
            assert operation.done is False
        assert job_after_sleep.cancelled is False
