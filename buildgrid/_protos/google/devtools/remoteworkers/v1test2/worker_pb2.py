# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: google/devtools/remoteworkers/v1test2/worker.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n2google/devtools/remoteworkers/v1test2/worker.proto\x12%google.devtools.remoteworkers.v1test2\"\xa9\x02\n\x06Worker\x12>\n\x07\x64\x65vices\x18\x01 \x03(\x0b\x32-.google.devtools.remoteworkers.v1test2.Device\x12J\n\nproperties\x18\x02 \x03(\x0b\x32\x36.google.devtools.remoteworkers.v1test2.Worker.Property\x12\x45\n\x07\x63onfigs\x18\x03 \x03(\x0b\x32\x34.google.devtools.remoteworkers.v1test2.Worker.Config\x1a&\n\x08Property\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\x1a$\n\x06\x43onfig\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\"\x8c\x01\n\x06\x44\x65vice\x12\x0e\n\x06handle\x18\x01 \x01(\t\x12J\n\nproperties\x18\x02 \x03(\x0b\x32\x36.google.devtools.remoteworkers.v1test2.Device.Property\x1a&\n\x08Property\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\tB\xc3\x01\n)com.google.devtools.remoteworkers.v1test2B\x13RemoteWorkersWorkerP\x01ZRgoogle.golang.org/genproto/googleapis/devtools/remoteworkers/v1test2;remoteworkers\xa2\x02\x02RW\xaa\x02%Google.DevTools.RemoteWorkers.V1Test2b\x06proto3')



_WORKER = DESCRIPTOR.message_types_by_name['Worker']
_WORKER_PROPERTY = _WORKER.nested_types_by_name['Property']
_WORKER_CONFIG = _WORKER.nested_types_by_name['Config']
_DEVICE = DESCRIPTOR.message_types_by_name['Device']
_DEVICE_PROPERTY = _DEVICE.nested_types_by_name['Property']
Worker = _reflection.GeneratedProtocolMessageType('Worker', (_message.Message,), {

  'Property' : _reflection.GeneratedProtocolMessageType('Property', (_message.Message,), {
    'DESCRIPTOR' : _WORKER_PROPERTY,
    '__module__' : 'google.devtools.remoteworkers.v1test2.worker_pb2'
    # @@protoc_insertion_point(class_scope:google.devtools.remoteworkers.v1test2.Worker.Property)
    })
  ,

  'Config' : _reflection.GeneratedProtocolMessageType('Config', (_message.Message,), {
    'DESCRIPTOR' : _WORKER_CONFIG,
    '__module__' : 'google.devtools.remoteworkers.v1test2.worker_pb2'
    # @@protoc_insertion_point(class_scope:google.devtools.remoteworkers.v1test2.Worker.Config)
    })
  ,
  'DESCRIPTOR' : _WORKER,
  '__module__' : 'google.devtools.remoteworkers.v1test2.worker_pb2'
  # @@protoc_insertion_point(class_scope:google.devtools.remoteworkers.v1test2.Worker)
  })
_sym_db.RegisterMessage(Worker)
_sym_db.RegisterMessage(Worker.Property)
_sym_db.RegisterMessage(Worker.Config)

Device = _reflection.GeneratedProtocolMessageType('Device', (_message.Message,), {

  'Property' : _reflection.GeneratedProtocolMessageType('Property', (_message.Message,), {
    'DESCRIPTOR' : _DEVICE_PROPERTY,
    '__module__' : 'google.devtools.remoteworkers.v1test2.worker_pb2'
    # @@protoc_insertion_point(class_scope:google.devtools.remoteworkers.v1test2.Device.Property)
    })
  ,
  'DESCRIPTOR' : _DEVICE,
  '__module__' : 'google.devtools.remoteworkers.v1test2.worker_pb2'
  # @@protoc_insertion_point(class_scope:google.devtools.remoteworkers.v1test2.Device)
  })
_sym_db.RegisterMessage(Device)
_sym_db.RegisterMessage(Device.Property)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'\n)com.google.devtools.remoteworkers.v1test2B\023RemoteWorkersWorkerP\001ZRgoogle.golang.org/genproto/googleapis/devtools/remoteworkers/v1test2;remoteworkers\242\002\002RW\252\002%Google.DevTools.RemoteWorkers.V1Test2'
  _WORKER._serialized_start=94
  _WORKER._serialized_end=391
  _WORKER_PROPERTY._serialized_start=315
  _WORKER_PROPERTY._serialized_end=353
  _WORKER_CONFIG._serialized_start=355
  _WORKER_CONFIG._serialized_end=391
  _DEVICE._serialized_start=394
  _DEVICE._serialized_end=534
  _DEVICE_PROPERTY._serialized_start=315
  _DEVICE_PROPERTY._serialized_end=353
# @@protoc_insertion_point(module_scope)
