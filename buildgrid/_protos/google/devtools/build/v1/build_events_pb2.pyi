"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
Copyright 2020 Google LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import builtins
import buildgrid._protos.google.devtools.build.v1.build_status_pb2
import google.protobuf.any_pb2
import google.protobuf.descriptor
import google.protobuf.internal.enum_type_wrapper
import google.protobuf.message
import google.protobuf.timestamp_pb2
import sys
import typing

if sys.version_info >= (3, 10):
    import typing as typing_extensions
else:
    import typing_extensions

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor

class _ConsoleOutputStream:
    ValueType = typing.NewType("ValueType", builtins.int)
    V: typing_extensions.TypeAlias = ValueType

class _ConsoleOutputStreamEnumTypeWrapper(google.protobuf.internal.enum_type_wrapper._EnumTypeWrapper[_ConsoleOutputStream.ValueType], builtins.type):
    DESCRIPTOR: google.protobuf.descriptor.EnumDescriptor
    UNKNOWN: _ConsoleOutputStream.ValueType  # 0
    """Unspecified or unknown."""
    STDOUT: _ConsoleOutputStream.ValueType  # 1
    """Normal output stream."""
    STDERR: _ConsoleOutputStream.ValueType  # 2
    """Error output stream."""

class ConsoleOutputStream(_ConsoleOutputStream, metaclass=_ConsoleOutputStreamEnumTypeWrapper):
    """The type of console output stream."""

UNKNOWN: ConsoleOutputStream.ValueType  # 0
"""Unspecified or unknown."""
STDOUT: ConsoleOutputStream.ValueType  # 1
"""Normal output stream."""
STDERR: ConsoleOutputStream.ValueType  # 2
"""Error output stream."""
global___ConsoleOutputStream = ConsoleOutputStream

@typing_extensions.final
class BuildEvent(google.protobuf.message.Message):
    """An event representing some state change that occurred in the build. This
    message does not include field for uniquely identifying an event.
    """

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    @typing_extensions.final
    class InvocationAttemptStarted(google.protobuf.message.Message):
        """Notification that the build system has attempted to run the build tool."""

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        ATTEMPT_NUMBER_FIELD_NUMBER: builtins.int
        DETAILS_FIELD_NUMBER: builtins.int
        attempt_number: builtins.int
        """The number of the invocation attempt, starting at 1 and increasing by 1
        for each new attempt. Can be used to determine if there is a later
        invocation attempt replacing the current one a client is processing.
        """
        @property
        def details(self) -> google.protobuf.any_pb2.Any:
            """Arbitrary details about the invocation attempt."""
        def __init__(
            self,
            *,
            attempt_number: builtins.int = ...,
            details: google.protobuf.any_pb2.Any | None = ...,
        ) -> None: ...
        def HasField(self, field_name: typing_extensions.Literal["details", b"details"]) -> builtins.bool: ...
        def ClearField(self, field_name: typing_extensions.Literal["attempt_number", b"attempt_number", "details", b"details"]) -> None: ...

    @typing_extensions.final
    class InvocationAttemptFinished(google.protobuf.message.Message):
        """Notification that an invocation attempt has finished."""

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        INVOCATION_STATUS_FIELD_NUMBER: builtins.int
        DETAILS_FIELD_NUMBER: builtins.int
        @property
        def invocation_status(self) -> buildgrid._protos.google.devtools.build.v1.build_status_pb2.BuildStatus:
            """Final status of the invocation."""
        @property
        def details(self) -> google.protobuf.any_pb2.Any:
            """Arbitrary details about the invocation attempt."""
        def __init__(
            self,
            *,
            invocation_status: buildgrid._protos.google.devtools.build.v1.build_status_pb2.BuildStatus | None = ...,
            details: google.protobuf.any_pb2.Any | None = ...,
        ) -> None: ...
        def HasField(self, field_name: typing_extensions.Literal["details", b"details", "invocation_status", b"invocation_status"]) -> builtins.bool: ...
        def ClearField(self, field_name: typing_extensions.Literal["details", b"details", "invocation_status", b"invocation_status"]) -> None: ...

    @typing_extensions.final
    class BuildEnqueued(google.protobuf.message.Message):
        """Notification that the build request is enqueued."""

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        DETAILS_FIELD_NUMBER: builtins.int
        @property
        def details(self) -> google.protobuf.any_pb2.Any:
            """Additional details about the Build."""
        def __init__(
            self,
            *,
            details: google.protobuf.any_pb2.Any | None = ...,
        ) -> None: ...
        def HasField(self, field_name: typing_extensions.Literal["details", b"details"]) -> builtins.bool: ...
        def ClearField(self, field_name: typing_extensions.Literal["details", b"details"]) -> None: ...

    @typing_extensions.final
    class BuildFinished(google.protobuf.message.Message):
        """Notification that the build request has finished, and no further
        invocations will occur.  Note that this applies to the entire Build.
        Individual invocations trigger InvocationFinished when they finish.
        """

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        STATUS_FIELD_NUMBER: builtins.int
        DETAILS_FIELD_NUMBER: builtins.int
        @property
        def status(self) -> buildgrid._protos.google.devtools.build.v1.build_status_pb2.BuildStatus:
            """Final status of the build."""
        @property
        def details(self) -> google.protobuf.any_pb2.Any:
            """Additional details about the Build."""
        def __init__(
            self,
            *,
            status: buildgrid._protos.google.devtools.build.v1.build_status_pb2.BuildStatus | None = ...,
            details: google.protobuf.any_pb2.Any | None = ...,
        ) -> None: ...
        def HasField(self, field_name: typing_extensions.Literal["details", b"details", "status", b"status"]) -> builtins.bool: ...
        def ClearField(self, field_name: typing_extensions.Literal["details", b"details", "status", b"status"]) -> None: ...

    @typing_extensions.final
    class ConsoleOutput(google.protobuf.message.Message):
        """Textual output written to standard output or standard error."""

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        TYPE_FIELD_NUMBER: builtins.int
        TEXT_OUTPUT_FIELD_NUMBER: builtins.int
        BINARY_OUTPUT_FIELD_NUMBER: builtins.int
        type: global___ConsoleOutputStream.ValueType
        """The output stream type."""
        text_output: builtins.str
        """Regular UTF-8 output; normal text."""
        binary_output: builtins.bytes
        """Used if the output is not UTF-8 text (for example, a binary proto)."""
        def __init__(
            self,
            *,
            type: global___ConsoleOutputStream.ValueType = ...,
            text_output: builtins.str = ...,
            binary_output: builtins.bytes = ...,
        ) -> None: ...
        def HasField(self, field_name: typing_extensions.Literal["binary_output", b"binary_output", "output", b"output", "text_output", b"text_output"]) -> builtins.bool: ...
        def ClearField(self, field_name: typing_extensions.Literal["binary_output", b"binary_output", "output", b"output", "text_output", b"text_output", "type", b"type"]) -> None: ...
        def WhichOneof(self, oneof_group: typing_extensions.Literal["output", b"output"]) -> typing_extensions.Literal["text_output", "binary_output"] | None: ...

    @typing_extensions.final
    class BuildComponentStreamFinished(google.protobuf.message.Message):
        """Notification of the end of a build event stream published by a build
        component other than CONTROLLER (See StreamId.BuildComponents).
        """

        DESCRIPTOR: google.protobuf.descriptor.Descriptor

        class _FinishType:
            ValueType = typing.NewType("ValueType", builtins.int)
            V: typing_extensions.TypeAlias = ValueType

        class _FinishTypeEnumTypeWrapper(google.protobuf.internal.enum_type_wrapper._EnumTypeWrapper[BuildEvent.BuildComponentStreamFinished._FinishType.ValueType], builtins.type):  # noqa: F821
            DESCRIPTOR: google.protobuf.descriptor.EnumDescriptor
            FINISH_TYPE_UNSPECIFIED: BuildEvent.BuildComponentStreamFinished._FinishType.ValueType  # 0
            """Unknown or unspecified; callers should never set this value."""
            FINISHED: BuildEvent.BuildComponentStreamFinished._FinishType.ValueType  # 1
            """Set by the event publisher to indicate a build event stream is
            finished.
            """
            EXPIRED: BuildEvent.BuildComponentStreamFinished._FinishType.ValueType  # 2
            """Set by the WatchBuild RPC server when the publisher of a build event
            stream stops publishing events without publishing a
            BuildComponentStreamFinished event whose type equals FINISHED.
            """

        class FinishType(_FinishType, metaclass=_FinishTypeEnumTypeWrapper):
            """How did the event stream finish."""

        FINISH_TYPE_UNSPECIFIED: BuildEvent.BuildComponentStreamFinished.FinishType.ValueType  # 0
        """Unknown or unspecified; callers should never set this value."""
        FINISHED: BuildEvent.BuildComponentStreamFinished.FinishType.ValueType  # 1
        """Set by the event publisher to indicate a build event stream is
        finished.
        """
        EXPIRED: BuildEvent.BuildComponentStreamFinished.FinishType.ValueType  # 2
        """Set by the WatchBuild RPC server when the publisher of a build event
        stream stops publishing events without publishing a
        BuildComponentStreamFinished event whose type equals FINISHED.
        """

        TYPE_FIELD_NUMBER: builtins.int
        type: global___BuildEvent.BuildComponentStreamFinished.FinishType.ValueType
        """How the event stream finished."""
        def __init__(
            self,
            *,
            type: global___BuildEvent.BuildComponentStreamFinished.FinishType.ValueType = ...,
        ) -> None: ...
        def ClearField(self, field_name: typing_extensions.Literal["type", b"type"]) -> None: ...

    EVENT_TIME_FIELD_NUMBER: builtins.int
    INVOCATION_ATTEMPT_STARTED_FIELD_NUMBER: builtins.int
    INVOCATION_ATTEMPT_FINISHED_FIELD_NUMBER: builtins.int
    BUILD_ENQUEUED_FIELD_NUMBER: builtins.int
    BUILD_FINISHED_FIELD_NUMBER: builtins.int
    CONSOLE_OUTPUT_FIELD_NUMBER: builtins.int
    COMPONENT_STREAM_FINISHED_FIELD_NUMBER: builtins.int
    BAZEL_EVENT_FIELD_NUMBER: builtins.int
    BUILD_EXECUTION_EVENT_FIELD_NUMBER: builtins.int
    SOURCE_FETCH_EVENT_FIELD_NUMBER: builtins.int
    @property
    def event_time(self) -> google.protobuf.timestamp_pb2.Timestamp:
        """The timestamp of this event."""
    @property
    def invocation_attempt_started(self) -> global___BuildEvent.InvocationAttemptStarted:
        """An invocation attempt has started."""
    @property
    def invocation_attempt_finished(self) -> global___BuildEvent.InvocationAttemptFinished:
        """An invocation attempt has finished."""
    @property
    def build_enqueued(self) -> global___BuildEvent.BuildEnqueued:
        """The build is enqueued."""
    @property
    def build_finished(self) -> global___BuildEvent.BuildFinished:
        """The build has finished. Set when the build is terminated."""
    @property
    def console_output(self) -> global___BuildEvent.ConsoleOutput:
        """An event containing printed text."""
    @property
    def component_stream_finished(self) -> global___BuildEvent.BuildComponentStreamFinished:
        """Indicates the end of a build event stream (with the same StreamId) from
        a build component executing the requested build task.
        *** This field does not indicate the WatchBuild RPC is finished. ***
        """
    @property
    def bazel_event(self) -> google.protobuf.any_pb2.Any:
        """Structured build event generated by Bazel about its execution progress."""
    @property
    def build_execution_event(self) -> google.protobuf.any_pb2.Any:
        """An event that contains supplemental tool-specific information about
        build execution.
        """
    @property
    def source_fetch_event(self) -> google.protobuf.any_pb2.Any:
        """An event that contains supplemental tool-specific information about
        source fetching.
        """
    def __init__(
        self,
        *,
        event_time: google.protobuf.timestamp_pb2.Timestamp | None = ...,
        invocation_attempt_started: global___BuildEvent.InvocationAttemptStarted | None = ...,
        invocation_attempt_finished: global___BuildEvent.InvocationAttemptFinished | None = ...,
        build_enqueued: global___BuildEvent.BuildEnqueued | None = ...,
        build_finished: global___BuildEvent.BuildFinished | None = ...,
        console_output: global___BuildEvent.ConsoleOutput | None = ...,
        component_stream_finished: global___BuildEvent.BuildComponentStreamFinished | None = ...,
        bazel_event: google.protobuf.any_pb2.Any | None = ...,
        build_execution_event: google.protobuf.any_pb2.Any | None = ...,
        source_fetch_event: google.protobuf.any_pb2.Any | None = ...,
    ) -> None: ...
    def HasField(self, field_name: typing_extensions.Literal["bazel_event", b"bazel_event", "build_enqueued", b"build_enqueued", "build_execution_event", b"build_execution_event", "build_finished", b"build_finished", "component_stream_finished", b"component_stream_finished", "console_output", b"console_output", "event", b"event", "event_time", b"event_time", "invocation_attempt_finished", b"invocation_attempt_finished", "invocation_attempt_started", b"invocation_attempt_started", "source_fetch_event", b"source_fetch_event"]) -> builtins.bool: ...
    def ClearField(self, field_name: typing_extensions.Literal["bazel_event", b"bazel_event", "build_enqueued", b"build_enqueued", "build_execution_event", b"build_execution_event", "build_finished", b"build_finished", "component_stream_finished", b"component_stream_finished", "console_output", b"console_output", "event", b"event", "event_time", b"event_time", "invocation_attempt_finished", b"invocation_attempt_finished", "invocation_attempt_started", b"invocation_attempt_started", "source_fetch_event", b"source_fetch_event"]) -> None: ...
    def WhichOneof(self, oneof_group: typing_extensions.Literal["event", b"event"]) -> typing_extensions.Literal["invocation_attempt_started", "invocation_attempt_finished", "build_enqueued", "build_finished", "console_output", "component_stream_finished", "bazel_event", "build_execution_event", "source_fetch_event"] | None: ...

global___BuildEvent = BuildEvent

@typing_extensions.final
class StreamId(google.protobuf.message.Message):
    """Unique identifier for a build event stream."""

    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    class _BuildComponent:
        ValueType = typing.NewType("ValueType", builtins.int)
        V: typing_extensions.TypeAlias = ValueType

    class _BuildComponentEnumTypeWrapper(google.protobuf.internal.enum_type_wrapper._EnumTypeWrapper[StreamId._BuildComponent.ValueType], builtins.type):  # noqa: F821
        DESCRIPTOR: google.protobuf.descriptor.EnumDescriptor
        UNKNOWN_COMPONENT: StreamId._BuildComponent.ValueType  # 0
        """Unknown or unspecified; callers should never set this value."""
        CONTROLLER: StreamId._BuildComponent.ValueType  # 1
        """A component that coordinates builds."""
        WORKER: StreamId._BuildComponent.ValueType  # 2
        """A component that runs executables needed to complete a build."""
        TOOL: StreamId._BuildComponent.ValueType  # 3
        """A component that builds something."""

    class BuildComponent(_BuildComponent, metaclass=_BuildComponentEnumTypeWrapper):
        """Which build component generates this event stream. Each build component
        may generate one event stream.
        """

    UNKNOWN_COMPONENT: StreamId.BuildComponent.ValueType  # 0
    """Unknown or unspecified; callers should never set this value."""
    CONTROLLER: StreamId.BuildComponent.ValueType  # 1
    """A component that coordinates builds."""
    WORKER: StreamId.BuildComponent.ValueType  # 2
    """A component that runs executables needed to complete a build."""
    TOOL: StreamId.BuildComponent.ValueType  # 3
    """A component that builds something."""

    BUILD_ID_FIELD_NUMBER: builtins.int
    INVOCATION_ID_FIELD_NUMBER: builtins.int
    COMPONENT_FIELD_NUMBER: builtins.int
    build_id: builtins.str
    """The id of a Build message."""
    invocation_id: builtins.str
    """The unique invocation ID within this build.
    It should be the same as {invocation} (below) during the migration.
    """
    component: global___StreamId.BuildComponent.ValueType
    """The component that emitted this event."""
    def __init__(
        self,
        *,
        build_id: builtins.str = ...,
        invocation_id: builtins.str = ...,
        component: global___StreamId.BuildComponent.ValueType = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing_extensions.Literal["build_id", b"build_id", "component", b"component", "invocation_id", b"invocation_id"]) -> None: ...

global___StreamId = StreamId
