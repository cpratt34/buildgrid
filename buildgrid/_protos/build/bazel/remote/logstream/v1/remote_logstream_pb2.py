# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: build/bazel/remote/logstream/v1/remote_logstream.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n6build/bazel/remote/logstream/v1/remote_logstream.proto\x12\x1f\x62uild.bazel.remote.logstream.v1\"(\n\x16\x43reateLogStreamRequest\x12\x0e\n\x06parent\x18\x01 \x01(\t\"6\n\tLogStream\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x1b\n\x13write_resource_name\x18\x02 \x01(\t2\x8c\x01\n\x10LogStreamService\x12x\n\x0f\x43reateLogStream\x12\x37.build.bazel.remote.logstream.v1.CreateLogStreamRequest\x1a*.build.bazel.remote.logstream.v1.LogStream\"\x00\x42q\n\x1f\x62uild.bazel.remote.logstream.v1B\x14RemoteLogStreamProtoP\x01Z\x0fremotelogstream\xa2\x02\x02RL\xaa\x02\x1f\x42uild.Bazel.Remote.LogStream.v1b\x06proto3')



_CREATELOGSTREAMREQUEST = DESCRIPTOR.message_types_by_name['CreateLogStreamRequest']
_LOGSTREAM = DESCRIPTOR.message_types_by_name['LogStream']
CreateLogStreamRequest = _reflection.GeneratedProtocolMessageType('CreateLogStreamRequest', (_message.Message,), {
  'DESCRIPTOR' : _CREATELOGSTREAMREQUEST,
  '__module__' : 'build.bazel.remote.logstream.v1.remote_logstream_pb2'
  # @@protoc_insertion_point(class_scope:build.bazel.remote.logstream.v1.CreateLogStreamRequest)
  })
_sym_db.RegisterMessage(CreateLogStreamRequest)

LogStream = _reflection.GeneratedProtocolMessageType('LogStream', (_message.Message,), {
  'DESCRIPTOR' : _LOGSTREAM,
  '__module__' : 'build.bazel.remote.logstream.v1.remote_logstream_pb2'
  # @@protoc_insertion_point(class_scope:build.bazel.remote.logstream.v1.LogStream)
  })
_sym_db.RegisterMessage(LogStream)

_LOGSTREAMSERVICE = DESCRIPTOR.services_by_name['LogStreamService']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'\n\037build.bazel.remote.logstream.v1B\024RemoteLogStreamProtoP\001Z\017remotelogstream\242\002\002RL\252\002\037Build.Bazel.Remote.LogStream.v1'
  _CREATELOGSTREAMREQUEST._serialized_start=91
  _CREATELOGSTREAMREQUEST._serialized_end=131
  _LOGSTREAM._serialized_start=133
  _LOGSTREAM._serialized_end=187
  _LOGSTREAMSERVICE._serialized_start=190
  _LOGSTREAMSERVICE._serialized_end=330
# @@protoc_insertion_point(module_scope)
