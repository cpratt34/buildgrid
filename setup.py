#!/usr/bin/env python3
#
# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import re
import sys
import pathlib

if sys.version_info[0] != 3 or sys.version_info[1] < 8:
    print("BuildGrid requires Python >= 3.8")
    sys.exit(1)

try:
    from setuptools import setup, find_packages, Command
except ImportError:
    print("BuildGrid requires setuptools in order to build. Install it using"
          " your package manager (usually python3-setuptools) or via pip (pip3"
          " install setuptools).")
    sys.exit(1)


class BuildGRPC(Command):
    """Command to generate project *_pb2.py modules from proto files."""

    description = 'build gRPC protobuf modules'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            import grpc_tools.command
        except ImportError:
            print("BuildGrid requires grpc_tools in order to build gRPC modules.\n"
                  "Install it via pip (pip3 install grpcio-tools).")
            exit(1)

        protos_root = 'buildgrid/_protos'

        grpc_tools.command.build_package_protos(protos_root)

        # Postprocess imports in generated code
        for root, _, files in os.walk(protos_root):
            for filename in files:
                if filename.endswith('.py'):
                    path = os.path.join(root, filename)
                    with open(path, 'r') as f:
                        code = f.read()

                    # All protos are in buildgrid._protos
                    code = re.sub(r'^from ', r'from buildgrid._protos.',
                                  code, flags=re.MULTILINE)
                    # Except for the core google.protobuf protos
                    code = re.sub(r'^from buildgrid._protos.google.protobuf', r'from google.protobuf',
                                  code, flags=re.MULTILINE)

                    with open(path, 'w') as f:
                        f.write(code)

REQ_DIR = os.path.join(pathlib.Path(__file__).parent.absolute(), 'requirements')

# Load main requirements from file:
with open(os.path.join(REQ_DIR, 'requirements.base.in')) as requirements_file:
    install_requirements = requirements_file.read().splitlines()

auth_requirements, auth_file = [], os.path.join(REQ_DIR, 'requirements.auth.in')
# Load 'auth' requirements from dedicated file:
if os.path.isfile(auth_file):
    with open(auth_file) as requirements_file:
        auth_requirements = requirements_file.read().splitlines()

browser_requirements, browser_file = [], os.path.join(REQ_DIR, 'requirements.browser.in')
# Load 'auth' requirements from dedicated file:
if os.path.isfile(browser_file):
    with open(browser_file) as requirements_file:
        browser_requirements = requirements_file.read().splitlines()

docs_requirements, docs_file = [], os.path.join(REQ_DIR, 'requirements.docs.in')
# Load 'docs' requirements from dedicated file:
if os.path.isfile(docs_file):
    with open(docs_file) as requirements_file:
        docs_requirements = requirements_file.read().splitlines()

tests_requirements, test_file = [], os.path.join(REQ_DIR, 'requirements.tests.in')
# Load 'tests' requirements from dedicated file:
if os.path.isfile(test_file):
    with open(test_file) as requirements_file:
        tests_requirements = requirements_file.read().splitlines()

db_requirements, db_file = [], os.path.join(REQ_DIR, 'requirements.db.in')
# Load 'db' requirements from dedicated file:
if os.path.isfile(db_file):
    with open(db_file) as requirements_file:
        db_requirements = requirements_file.read().splitlines()

redis_requirements, redis_file = [], os.path.join(REQ_DIR, 'requirements.redis.in')
# Load 'redis' requirements from dedicated file:
if os.path.isfile(redis_file):
    with open(redis_file) as requirements_file:
        redis_requirements = requirements_file.read().splitlines()

dev_requirements, dev_file = [], os.path.join(REQ_DIR, 'requirements.dev.in')
# Load 'dev' requirements from dedicated file:
if os.path.isfile(dev_file):
    with open(dev_file) as requirements_file:
        dev_requirements = requirements_file.read().splitlines()

rabbitmq_requirements, rabbitmq_file = [], os.path.join(REQ_DIR, 'requirements.rabbitmq.in')
# Load 'rabbitmq' requirements from dedicated file:
if os.path.isfile(rabbitmq_file):
    with open(rabbitmq_file) as requirements_file:
        rabbitmq_requirements = requirements_file.read().splitlines()

# List containing all requirements that are necessary and optional.
all_requirements, all_file = [], os.path.join(REQ_DIR, 'requirements.in')
if os.path.isfile(all_file):
    with open(all_file) as requirements_file:
        all_requirements = requirements_file.read().splitlines()

about = {}
with open("buildgrid/_version.py") as version_file:
    exec(version_file.read(), about)

setup(
    name="BuildGrid",
    version=about["__version__"],
    license="Apache License, Version 2.0",
    description="A remote execution service",
    cmdclass={
        'build_grpc': BuildGRPC, },
    packages=find_packages(),
    package_data={
        'buildgrid.server.persistence.sql': ['alembic/*', 'alembic/**/*'],
        'buildgrid._app.settings': ['schemas/*', 'schemas/**/*'],
        'buildgrid.server.operations.filtering': ['filter_grammar.lark']
    },
    python_requires='>= 3.8',
    install_requires=install_requirements,
    setup_requires=['pytest-runner'],
    tests_require=tests_requirements,
    extras_require={
        'auth': auth_requirements,
        'browser': browser_requirements,
        'database': db_requirements,
        'rabbitmq': rabbitmq_requirements,
        'redis': redis_requirements,
        'docs': docs_requirements,
        'tests': tests_requirements,
        'dev': dev_requirements,
        'all': all_requirements},
    entry_points={
        'console_scripts': [
            'bgd = buildgrid._app:cli',
        ]
    }
)
