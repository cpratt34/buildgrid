# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from enum import Enum
import time
from typing import Callable, Dict

import gipc
import grpc
from locust import events


class RequestType(Enum):
    UNARY_UNARY = 0
    UNARY_STREAM = 1
    STREAM_UNARY = 2
    STREAM_STREAM = 3


class GrpcClient:
    def __init__(self, host, stub_class):
        self._host = host
        self._stub_class = stub_class

    def _simple_request(
        self,
        pipe,
        name: str,
        request_meta: Dict,
        *args,
        **kwargs
    ) -> None:
        try:
            channel = grpc.insecure_channel(self._host)
            stub = self._stub_class(channel)
            func = self._stub_class.__getattribute__(stub, name)

            start_perf_counter = time.perf_counter()

            success = False
            try:
                response = func(*args, **kwargs)
                request_meta["response"] = response.SerializeToString()
                success = True
            except grpc.RpcError as e:
                request_meta["exception"] = str(e)

            request_meta["response_time"] = (time.perf_counter() - start_perf_counter) * 1000
            if success:
                request_meta["response_length"] = len(request_meta["response"])
        except Exception as e:
            request_meta["exception"] = str(e)
            request_meta["response_time"] = 0
        try:
            pipe.put(request_meta)
        except BrokenPipeError:
            # This normally means that the greenlet which made this request
            # has gone away, usually due to the load test being stopped.
            pass

    def __getattr__(self, name: str) -> Callable:

        def wrapper(request_type: RequestType, *args, **kwargs):
            request_meta = {
                "request_type": "grpc",
                "name": name,
                "start_time": time.time(),
                "response_length": 0,
                "exception": None,
                "context": None,
                "response": None,
            }

            if request_type in (RequestType.UNARY_UNARY, RequestType.STREAM_UNARY):
                with gipc.pipe() as (read, write):
                    request_process = gipc.start_process(
                        target=self._simple_request,
                        args=(write, name, request_meta, *args),
                        kwargs=kwargs
                    )
                    try:
                        request_meta = read.get()
                    except (EOFError, OSError):
                        # Sometimes we might lose communication with the subprocess.
                        # Just return here, and write this attempted request off.
                        return
                    request_process.join()

            events.request.fire(**request_meta)
            return request_meta["response"]

        return wrapper
