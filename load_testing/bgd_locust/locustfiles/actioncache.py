# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import random

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    ActionResult,
    Digest,
    ExecutedActionMetadata,
    GetActionResultRequest,
    UpdateActionResultRequest
)
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2_grpc import ActionCacheStub
from buildgrid.settings import HASH
from locust import between, task

from common.grpc.client import RequestType
from common.grpc.user import GrpcUser


class ActionCacheUser(GrpcUser):
    abstract = False
    host = "localhost:50053"
    stub_classes = {
        "action_cache": ActionCacheStub
    }
    wait_time = between(1, 5)

    def __init__(self, environment):
        super().__init__(environment)
        self._action_digests = []

    def _generate_blob(self, size: int) -> bytes:
        return os.urandom(size)

    def _generate_action_digest(self) -> Digest:
        fake_action = self._generate_blob(120)
        digest = Digest(
            hash=HASH(fake_action).hexdigest(),
            size_bytes=len(fake_action)
        )
        self._action_digests.append(digest)
        return digest

    def _update_action_result(self, digest: Digest) -> None:
        executed_action_metadata = ExecutedActionMetadata(worker="test-worker")
        result = ActionResult(
            execution_metadata=executed_action_metadata,
            exit_code=0,
            stdout_raw=self._generate_blob(1024 * 1024)
        )
        request = UpdateActionResultRequest(
            instance_name="",
            action_digest=digest,
            action_result=result
        )

        self.action_cache_client.UpdateActionResult(RequestType.UNARY_UNARY, request)

    def _pick_digest(self) -> Digest:
        if not self._action_digests:
            digest = self._generate_action_digest()
            self._update_action_result(digest)
            return digest
        index = random.randint(0, len(self._action_digests) - 1)
        return self._action_digests[index]

    @task
    def update_action_result(self) -> None:
        digest = self._generate_action_digest()
        self._update_action_result(digest)

    @task(10)
    def get_action_result(self) -> None:
        digest = self._pick_digest()
        request = GetActionResultRequest(instance_name="", action_digest=digest)
        self.action_cache_client.GetActionResult(RequestType.UNARY_UNARY, request)
