.. _installation:

Installation
============

.. _install-on-host:

Installation onto host machine
------------------------------

How to install BuildGrid directly onto your machine.

.. note::

   BuildGrid server currently only supports *Linux*. *macOS* and *Windows*
   platforms are **not** supported.


.. _install-host-prerequisites:

Prerequisites
~~~~~~~~~~~~~

BuildGrid only supports ``python3 >= 3.8`` but has no system requirements.
Main Python dependencies, automatically handled during installation, include:

- `boto3`_: the Amazon Web Services (AWS) SDK for Python.
- `click`_: a Python composable command line library.
- `grpcio`_: Google's `gRPC`_ Python interface.
- `janus`_: a mixed sync-async Python queue.
- `protobuf`_: Google's `protocol-buffers`_ Python interface.
- `pycurl`_: a Python interface to the cURL library (depends on libcurl).
- `PyYAML`_: a YAML parser and emitter for Python.

.. _boto3: https://pypi.org/project/boto3
.. _click: https://pypi.org/project/click
.. _grpcio: https://pypi.org/project/grpcio
.. _gRPC: https://grpc.io
.. _janus: https://pypi.org/project/janus
.. _protobuf: https://pypi.org/project/protobuf
.. _protocol-buffers: https://developers.google.com/protocol-buffers
.. _pycurl: https://pypi.org/project/pycurl
.. _PyYAML: https://pypi.org/project/PyYAML
.. _install-host-source-install:

Install from sources
~~~~~~~~~~~~~~~~~~~~

BuildGrid has ``setuptools`` support. We recommend installing it in a dedicated
`virtual environment`_. In order to do so in an environment named ``env``
placed in the source tree, run:

.. code-block:: sh

   git clone https://gitlab.com/BuildGrid/buildgrid.git
   cd buildgrid
   python3 -m venv env
   env/bin/python -m pip install --upgrade setuptools pip wheel
   env/bin/python -m pip install --editable .

.. hint::

   Once created, the virtual environment can be *activated* by sourcing the
   ``env/bin/activate`` script. In an activated terminal session, simply run
   ``deactivate`` to later *deactivate* it.

Once completed, you can check that installation succeed by locally starting the
BuildGrid server with default configuration. Simply run:

.. code-block:: sh

   env/bin/bgd server start data/config/default.yml -vvv

.. note::

   The ``setup.py`` script defines extra targets, ``auth``, ``docs``,
   ``tests``, ``db`` and ``redis``. They declare required dependency for, respectively,
   authentication and authorization management, generating documentation, running
   unit-tests and different storage implementations. They can be use as helpers for
   setting up a development environment. To use them run:

   .. code-block:: sh

      env/bin/python -m pip install --editable ".[auth,docs,tests]"

.. TODO: split up other storage backends into their own requirements files

   Similarly, if using a specific storage type, install the required dependencies. For
   example, if using a redis storage backend, install the redis client by:

    .. code-block:: sh

      env/bin/python -m pip install --editable ".[redis]"

.. _virtual environment: https://docs.python.org/3/library/venv.html


.. _install-kubernetes:

Installation in Kubernetes
--------------------------

BuildGrid also supports deployment into a Kubernetes cluster. Docker images
for use in this kind of deployment are provided for `the BuildGrid server
and tooling`_ and also `BuildBox for running workers`_.

An example Kubernetes deployment of BuildGrid is provided in
`the buildgrid-deployments repository`_. This example is intended as a
demo deployment, providing a starting point for writing a production
deployment configuration.

Some things to consider which aren't addressed in the example:

- Handling Bot connections across a scaled Bots service.
   - This will need the "Permissive BotSession Mode" described in
     :ref:`persisting-state`
- Scaling the CAS service
   - The example uses disk storage, a real deployment probably needs
     to use some kind of shareable storage like S3, perhaps with a
     cache layer for speed.

.. _the BuildGrid server and tooling: https://gitlab.com/BuildGrid/buildgrid/container_registry/eyJuYW1lIjoiYnVpbGRncmlkL2J1aWxkZ3JpZC9idWlsZGdyaWQiLCJ0YWdzX3BhdGgiOiIvQnVpbGRHcmlkL2J1aWxkZ3JpZC9yZWdpc3RyeS9yZXBvc2l0b3J5LzEyODgxMzEvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxMjg4MTMxLCJjbGVhbnVwX3BvbGljeV9zdGFydGVkX2F0IjpudWxsfQ==
.. _BuildBox for running workers: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/container_registry/eyJuYW1lIjoiYnVpbGRncmlkL2J1aWxkZ3JpZC5odWIuZG9ja2VyLmNvbS9idWlsZGJveCIsInRhZ3NfcGF0aCI6Ii9CdWlsZEdyaWQvYnVpbGRncmlkLmh1Yi5kb2NrZXIuY29tL3JlZ2lzdHJ5L3JlcG9zaXRvcnkvNDM5OTEwL3RhZ3M%2FZm9ybWF0PWpzb24iLCJpZCI6NDM5OTEwLCJjbGVhbnVwX3BvbGljeV9zdGFydGVkX2F0IjpudWxsfQ==
.. _the buildgrid-deployments repository: https://gitlab.com/BuildGrid/buildgrid-deployments/-/tree/master/kubernetes
